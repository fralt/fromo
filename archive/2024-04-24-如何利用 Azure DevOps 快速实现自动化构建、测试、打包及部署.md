> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [www.cnblogs.com](https://www.cnblogs.com/BrainDeveloper/p/12322251.html)

前两天有朋友问我，微软的 Azure 好用吗，适不适合国人的使用习惯，我就跟他讲了下，Azue 很好用，这也是为什么微软云营收一直涨涨涨的原因，基本可以再 1 个小时内实现自动化构建、打包以及部署到 Azure 服务器上。利用周末的时间，写了这篇文章，分享给大家，希望能帮助一些人快速入手如何使用 Azure DevOps 自动化构建、测试以及部署自己的服务。

今天，我给大家一步一步详细介绍，如何在 1 个小时内，创建一个 Web API 项目，实现服务的自动化构建、打包，并自动化部署到 Azure 上。

**1. 创建一个 Azure 托管存储库 （Organization）**
======================================

第一步，需要在 Azure DevOps (https://dev.azure.com/) 上创建一个组织团体（Organization)。

Organization 可以理解为是一个公司（或者一个事业群）或某个机构的所有数据的统一存储库，微软称之为托管存储库。

Host 项目的地区选择一个最近的区域最好，减少网络延迟。

![](https://img2018.cnblogs.com/common/280018/202002/280018-20200217161010624-1229566985.png)

**2. 创建一个新的 Azure DevOps 项目（Project）**
======================================

　　Azure DevOps Project 一般是一个大组或者一个大的周期内共同使用的一个数据及代码集合。你在创建项目时，可以设置项目名称以及项目具体的介绍；也可以选择项目的可见性，可以设置为公开的或者私有的。版本控制可以选择默认的 Git。

　　其他选项（例如，工作项处理的模板等），有兴趣的可以自己研究，这里不做重点介绍。

　　![](https://img2018.cnblogs.com/common/280018/202002/280018-20200217161637387-1410519850.png)

**3. 创建一个新的代码仓库 (Repository）**
==============================

 Repository 顾名思义就是存放代码的地方， 一个 Repo 可以有很多个分支，一般默认为 master 分支。 

![](https://img2018.cnblogs.com/common/280018/202002/280018-20200217162246116-1196385207.png)

在这里，我们创建一个名叫 AzureWebApps 的 Repository，并且假设我们以 VisualStudio 为 IDE，选择 VisualStudio .ignore 文件模板。.ignore 文件里配置了那些我们纳入 Git 管理的文件。

　![](https://img2018.cnblogs.com/common/280018/202002/280018-20200217162647580-1413907538.png)

**4.  创建我们的 Web API 项目，并进行修改。**
===============================

 **4.1 克隆代码到本地**

　　我们先把上面创建的 AzureWebApps Repo 克隆到本地，方便在本地对代码进行增删改。Azure DevOps 本身也提供了在线编辑 Git Repo，一般一些微小的改动可以直接在线修改。

![](https://img2018.cnblogs.com/common/280018/202002/280018-20200217163031570-305110804.png)

![](https://img2018.cnblogs.com/common/280018/202002/280018-20200217163306714-776886137.png)

打开 Visual Studio，连接到我们的托管存储库，并定位到我们所创建的代码仓库（AzureWebApps)，点击 “克隆” 即可。

（你本机如未安装 Git，请自行到 [Git 官网](https://git-scm.com/downloads)安装 Git，后面会用到。）

![](https://img2018.cnblogs.com/common/280018/202002/280018-20200217163815946-962358073.png)

**4.2 创建 HelloAzure API 网站。**

接下来，我们在 VS 里来创建一个 Web API project。

新建项目模板时，我们选择 “Asp.Net Core Web 应用程序” 模板，如下：

![](https://img2018.cnblogs.com/common/280018/202002/280018-20200217172751431-583433770.png)

然后下一步，我们选择 API 的 ASP.NET Core Web API: 

![](https://img2018.cnblogs.com/common/280018/202002/280018-20200217172954632-1601343516.png)

当我们创建完项目后，默认的 API Project 是一个随机返回天气预报信息的 API。 我们可以任意修好一些配置，如端口，打包输出位置，对象类型及属性等。我在这里简单加了一个 Source 属性给 WeatherForecase.cs. 

![](https://img2018.cnblogs.com/common/280018/202002/280018-20200217173213692-1330627363.png)

**5. 创建一个构建管道（Build Pipeline）**
===============================

　　此构建管道（Build Pipeline）的作用就是：每当我们有代码更新（Push）到远程 master 分支时，它会自动用来自动构建，（自动测试，这里略过），自动打包生成 Artifacts 供后面自动部署管道使用。

　　**5.1 创建构建管道 （BuildAndPublishHelloAzure)**

　　我们在 Pipelines 页面，新建一个 Pipeline， 并选择连接到 “Azure Repos Git” 作为代码仓库位置，如下图：

![](https://img2018.cnblogs.com/common/280018/202002/280018-20200217173652351-659869572.png)

接着，选择我们上面存放代码的代码仓库 (Repository) - AzureWebApps:

![](https://img2018.cnblogs.com/common/280018/202002/280018-20200217174622208-1912273488.png)

 接下来，我们来进行初始化配置我们的构建管道（Build Pipeline）。我们给他配置上一个默认的任务（Task）- ASP.Net Core (.NET Framework) ， 此 Task 会利用 VS Build 来自动编译. sln 及 .csproj 的项目。

![](https://img2018.cnblogs.com/common/280018/202002/280018-20200217174816255-908952631.png)

这里我简单介绍下，Azure 的一个 Pipeline 一般是包含多个任务（Task）， 每个任务（Task）是一个最小的运行单元。Azure 市场（Market place）上有很多现成的 task 模板可以供咱们直接使用，只需简单的配置一些参数即可。

因为我们需要把编译构建 HelloAzure 的结果包发布到 Azure 上的某个地方，因此我们需要给我们的 Build Pipeline 加一个任务 Publish build artifacts （直接在搜索框里搜‘publish build’）：

![](https://img2018.cnblogs.com/common/280018/202002/280018-20200217175212565-326671001.png)

Publish build artifacts 任务有三个参数，我们保持默认就可以。 请注意，其中 Artifact name （drop） 在后面配置部署管道时会用到。

![](https://img2018.cnblogs.com/common/280018/202002/280018-20200217175732653-1620525121.png)

点击添加完后，咱们就会在左边的 YAML Settings 里看到我们新加的这个任务的设置了，如果需要的话，可以进行修改。最后我们保存我们配置好的构建管道。

![](https://img2018.cnblogs.com/common/280018/202002/280018-20200217175953003-972615650.png)

保存后，我们可以把我们的管道重命名成一个更有意义的名字，如 BuildAndPublishHelloAzure :

![](https://img2018.cnblogs.com/common/280018/202002/280018-20200217180212623-1044025784.png)

**5.2 配置自动化（持续性）构建**

构建管道创建好了，接下来我们需要给我们的 Repository 配置如何自动化构建。

我们的需求是，如果 master 分支有代码更新（包括新建 Pull Request， Complete Code/Push），那么就自动运行我们 BuildAndPublishHelloAzure Pipeline。

首先我们在分支页面，找到 Master 分支的分支策略管理页面，添加一个构建策略：

![](https://img2018.cnblogs.com/common/280018/202002/280018-20200217180327802-1644440864.png)

新建的构建策略的配置页面，触发一项我们选”Automatic“，这样每当有新的 PullRequest 创建时，就会自动绑定此 BuildAndPublishHelloAzure Pipeline 进行编译，构建，跑单元测试等。 

![](https://img2018.cnblogs.com/common/280018/202002/280018-20200217180751637-685313752.png)

 最后，我们需要在配置，当有代码 check in （PullRequest Complete）后，也自动运行这个 build Pipeline。

在 BuildAndPublishHelloAzure 编辑页面，跳到 Triggers（触发器）这个配置 tab 页面，我们勾上并选中 “Enable continuous integration” 即可，一般我们只需要对特定的一些分支设置持续性集成构建测试，所以我这里也只设置了 master 分支。

![](https://img2018.cnblogs.com/common/280018/202002/280018-20200217182124597-1606338989.png)

到此，自动化的持续性集成构建 （测试）及打包已经完成了。

**6. 在 Azure 上创建一个 Web APP （Web API） 网站**
=========================================

**6.1 创建 Azure 订阅（Subscription）**

在创建 Web 网站之前，我们需要创建一个 Azure 订阅（Subscription，Azure 用来收费的账户，如果你已经有了，可跳过）。登录 www.azure.com， 用微软账户登录，在门户页面创建一个 subscription，如下：

![](https://img2018.cnblogs.com/common/280018/202002/280018-20200217182721090-1054845624.png)

**6.2 创建 HelloAzure Web API Application**

在 Azure Portal （门户）的搜索框里搜”Api app“， 就回出来 API App 的一个创建模板，点击它开始创建：

![](https://img2018.cnblogs.com/common/280018/202002/280018-20200217182928907-1020468408.png)

配置好你的网站名字 - JasonHelloAzure，并选择上一步创建的订阅（Azure subsciption - Jason Test) ，其他默认即可。

![](https://img2018.cnblogs.com/common/280018/202002/280018-20200217183125305-589426393.png)

接下来我们将介绍如果将自动化构建生成好的包部署到我们创建的这个 API 网站（JasonHelloAzure) 上。

**7. 创建一个发布管道（Release Pipeline）**
=================================

　　此发布管道（Release Pipeline）的作用就是：每当我们有代码更新（Push）后并已经打包好后，此管道会自动将构建管道生成的 Artifacts 自动部署到 Azure Web App (JasonHelloAzure)。

**7.1 创建发布管道 HelloAzureReleasePipeline**

我们在 Releases 频道，新建一个 Release Pipeline， 并选择连接到 “Azure Repos Git” 作为代码仓库位置，如下

![](https://img2018.cnblogs.com/common/280018/202002/280018-20200217181155699-1968938486.png)

新建是，会弹出来让你选择一个模板（如下图），我们这可以选择”Azure App Service deployment“， 这个模板适用于所有 Azure Web app 及其他一些 app （如 containers 部署，Azure Function apps 等）：

![](https://img2018.cnblogs.com/common/280018/202002/280018-20200217183638673-43716783.png)

现在我们来给这个部署管道设置部署的来源，点击 Artifacs 这个模块，在右边会弹出来配置的页面：

Project 就是我们第二不创建的项目，也是存放我们创建的构建管道的地方。

Source （build pipeline），选择我们创建的 BuildAndPublishHelloAzure 管道。

默认版本（Default Vesion），选择 Latest 即可，意思是每当上面的 BuildAndPublishHelloAzure 管道的最新发布的包。

Source alias， 就是包名的意思，在配置 BuildAndPublishHelloAzure 构建管道时，有一步配置 Artifacts name 配置的就是 "drop”， 这里只需前后配置一致即可，任意字符串都可以。

![](https://img2018.cnblogs.com/common/280018/202002/280018-20200217183839968-178267887.png)

**7.2 配置自动化持续性部署**

现在我们来给部署管道配置持续性部署触发（Continuous deployment trigger)， 这个意味着，每当有新的 artifacts 包生成时，就自动触发这个部署管道进行部署。

点击 Artifacts 模块里的那个小闪电 button， 右边就会出来持续性部署触发器的配置页面。

选择一个 master 分支，启用 Continue deployment trigger， 如下图：

![](https://img2018.cnblogs.com/common/280018/202002/280018-20200217185740648-1292412484.png)

最后一步，我们来配置部署管道要部署的目的地，也就是配置到我们上面创建好的 Azure API App （JasonHelloAzure)。

一个部署管道也跟构建管道类似，区别是他包含多个阶段（Stage），一个阶段又包含任务（Task）。

点击任务选项组（Tasks），在右边的配置页里，填好阶段名称，选择订阅名称（Azure subscription - Jason Test)。

App type, 由于我们创建的是 API App，自然选择 API App， 选了 API App 后，最后的 App service name 下拉框就会出现所有该订阅下面的 API App， 我们选择 JasonHelloAzure 及可。

![](https://img2018.cnblogs.com/common/280018/202002/280018-20200217190014107-488325637.png)

 Deploy Azure App Service 任务的配置，我们保持默认即可。

 ![](https://img2018.cnblogs.com/common/280018/202002/280018-20200217190919534-1377789835.png)

**8. 测试结果与总结**
==============

**8.1 效果展示**

我们直接先手动运行下创建好的 “HelloAzureReleasePipeline” 部署管道，然后访问 JasonHelloAzure API 网站，如下：

![](https://img2018.cnblogs.com/common/280018/202002/280018-20200217191113011-753079877.png)

最后，我们来试试自动化部署，看看效果（成果）哈 ：）

我们创建一个 Pull Request， 那么自动跑我们配置好的 CodeBuild Policy （其实就是跑 BuildAndPublishHelloAzure Pipeline)

![](https://img2018.cnblogs.com/common/280018/202002/280018-20200217191535458-1248072027.png)

当 Pull Request Complete 后， 会自动跑持续性构建管道，以及部署管道：

![](https://img2018.cnblogs.com/common/280018/202002/280018-20200217191802596-1431162515.png)

约 3 分钟后，部署完成，再次访问 JasonHelloAzure API: [https://jasonhelloazure.azurewebsites.net/weatherforecast](https://jasonhelloazure.azurewebsites.net/weatherforecast) , 结果已经更新：

![](https://img2018.cnblogs.com/common/280018/202002/280018-20200217192034355-1891080951.png)

希望对想用 Azure DevOps 对自己的服务做自动化 CI/CD 的人有帮助。

本文没有重点介绍测试部分，可以直接给 HelloAzure 创建一个 UnitTest Project，BuildAndPublishPipelline 可以增加一个跑单元测试的任务即可以实现自动化构建 + 测试了。

**8.2 总结**

Azure DevOps 整体还是很人性好的，在易用性和可扩展性方面确实做的不错。对于一些中小企业，还是一个不错的选择，可以让研发人员专注于业务逻辑，省去了一些 CI/CD 的繁杂琐事。微软 Azure 部门可以说是最具有互联网基因的事业群了，Azure 的产品同时也有了互联网的敏捷性和易用性，这也是微软股价持续新高，被华尔街看好的原因。