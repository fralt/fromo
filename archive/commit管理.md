## commit提交记录

开发过程中，本地通常会有无数次 commit ，可以合并 “相同功能” 的多个 commit，以保持历史的简洁。

### **git rebase**

```
# 从HEAD版本开始往过去数3个版本
$ git rebase -i HEAD~3

# 合并指定版本号（不包含此版本）
$ git rebase -i [commitid]

```

说明：

*   `-i（--interactive）`：弹出交互式的界面进行编辑合并
*   `[commitid]`：要合并多个版本之前的版本号，注意：`[commitid]` 本身不参与合并

指令解释（交互编辑时使用）：

*   p, pick = use commit
*   r, reword = use commit, but edit the commit message
*   e, edit = use commit, but stop for amending
*   s, squash = use commit, but meld into previous commit
*   f, fixup = like "squash", but discard this commit's log message
*   x, exec = run command (the rest of the line) using shell
*   d, drop = remove commit

### **合并步骤**

1.  查看 log 记录，使用`git rebase -i`选择要合并的 commit
2.  编辑要合并的版本信息，保存提交，多条合并会出现多次（可能会出现冲突）
3.  修改注释信息后，保存提交，多条合并会出现多次
4.  推送远程仓库或合并到主干分支

#### **查看 log**

```
$ git log --oneline
291e427 update website
8c8f3f4 update website
1693a6f update clear-logs.sh version
3759b84 update clear-logs.sh
fc36a2a add links
1d795e6 fix && update clear-logs.sh 0.0.2
9536dab add dingtalk script
3a51aaa fix shellcheck problem
2db6ad3 add clear logs scripts
e57b0e6 fix && add batch del
17cb931 fix && add batch del
cf7e875 add redis script
fe4bbcb Initial commit

```

#### **编辑要合并版本**

```
# 指定要合并版本号，cf7e875 不参与合并，进入 vi 编辑器
$ git rebase -i cf7e875 
pick 17cb931 fix && add batch del
pick e57b0e6 fix && add batch del
pick 2db6ad3 add clear logs scripts
pick 3a51aaa fix shellcheck problem
pick 9536dab add dingtalk script
pick 1d795e6 fix && update clear-logs.sh 0.0.2
pick fc36a2a add links
pick 3759b84 update clear-logs.sh
pick 1693a6f update clear-logs.sh version
pick 8c8f3f4 update website

# Rebase cf7e875..291e427 onto cf7e875 (10 commands)
#
# Commands:
# p, pick = use commit
# r, reword = use commit, but edit the commit message
# e, edit = use commit, but stop for amending
# s, squash = use commit, but meld into previous commit
# f, fixup = like "squash", but discard this commit's log message
# x, exec = run command (the rest of the line) using shell
# d, drop = remove commit
#
# These lines can be re-ordered; they are executed from top to bottom.
#
# If you remove a line here THAT COMMIT WILL BE LOST.
#
# However, if you remove everything, the rebase will be aborted.
#
# Note that empty commits are commented out

```

将 commit 内容编辑如下：

```
# 把 e57b0e6 合并到 17cb931，不保留注释；把 1693a6f 合并到 3759b84
pick 17cb931 fix && add batch del
f e57b0e6 fix && add batch del
pick 2db6ad3 add clear logs scripts
pick 3a51aaa fix shellcheck problem
pick 9536dab add dingtalk script
pick 1d795e6 fix && update clear-logs.sh 0.0.2
pick fc36a2a add links
pick 3759b84 update clear-logs.sh
s 1693a6f update clear-logs.sh version
pick 8c8f3f4 update website

```

然后`:wq`保存退出后是注释界面：

```
# This is a combination of 2 commits.
# This is the 1st commit message:

update clear-logs.sh

# This is the commit message #2:

update clear-logs.sh version

# Please enter the commit message for your changes. Lines starting
# with '#' will be ignored, and an empty message aborts the commit.
#
# Date:      Tue Jul 28 18:25:57 2020 +0800
#
# interactive rebase in progress; onto cf7e875
# Last commands done (9 commands done):
#    pick 3759b84 update clear-logs.sh
#    s 1693a6f update clear-logs.sh version
# Next command to do (1 remaining command):
#    pick 8c8f3f4 update website
# You are currently editing a commit while rebasing branch 'master' on 'cf7e875'.
#
# Changes to be committed:
# modified:   logs/README.md
# modified:   logs/clear-logs.sh
#

```

编辑注释信息，保存退出`:wq`即可完成 commit 的合并：

#### **查看合并后的 log**

```
$ git log --oneline
47e7751 update website
4c2316c update clear-logs.sh
73f082e add links
56adcf2 fix && update clear-logs.sh 0.0.2
ebf3786 add dingtalk script
6e81ea7 fix shellcheck problem
64ca58f add clear logs scripts
9327def fix && add batch del
cf7e875 add redis script
fe4bbcb Initial commit

```

#### **推送远程**

```
# 由于我是一个人开发，故强制推送到远程仓库
$ git push --force origin master

```

### **冲突解决**

在 `git rebase` 过程中，可能会存在冲突，此时就需要解决冲突。

错误提示信息：`git rebase -i resumeerror: could not apply ...`。

```
# 查看冲突
$ git status

# 解决冲突之后，本地提交
$ git add .

# rebase 继续
$ git rebase --continue

```

## commit提交说明

Git 每次提交代码，都要写 Commit message（提交说明），否则就不允许提交。但是，一般来说，commit message 应该清晰明了，说明本次提交的目的。

目前，社区有多种 Commit message 的写法规范。本文介绍 [Angular 规范](https://link.segmentfault.com/?enc=dIUZ9boIq1WiZd3iSS9Rlg%3D%3D.wVnfIpcoe3ccpJ%2FtUPvZ5bCiy7RCJWKw9SSnfMfKFgEJa5BI3PABvhqa%2FW2vM6DMjigt2FfZoRejDdsdNf%2BX3hxS%2BSy927J1v0gvtu3mMthR2yAsEGXqsQyhGobsdt6N9dNJbZn99vC53Z8KTKo7WA%3D%3D)是目前使用最广的写法，比较合理和系统化，并且有配套的工具。前前端框架 [Angular.js](https://link.segmentfault.com/?enc=cBjZqo6hXcrAa0vtDIyTIg%3D%3D.xGAMfYEkZmPll30P9%2FMw4OlkDUV9qbBxIt6B%2FGET8e42HduCcZcWIZeiBmVW3A6blYqCkrv5BSNGOqzS3eeLEikTVfXI8NmoUH3%2FfxVENSQ%3D) 采用的就是该规范。如下图：

![](https://segmentfault.com/img/remote/1460000009048914?w=700&h=678)

### Commit message 的作用

#### 提供更多的历史信息，方便快速浏览。

比如，下面的命令显示上次发布后的变动，每个 commit 占据一行。你只看行首，就知道某次 commit 的目的。

```
$ git log <last tag> HEAD --pretty=format:%s

```

![](https://segmentfault.com/img/remote/1460000009048915?w=546&h=357)

#### 可以过滤某些 commit（比如文档改动），便于快速查找信息

```
$ git log <last release> HEAD --grep feature

```

#### 可以直接从 commit 生成 Change log。

Change Log 是发布新版本时，用来说明与上一个版本差异的文档，详见后文。  
![](https://segmentfault.com/img/remote/1460000009048916?w=700&h=353)

#### 其他优点

*   可读性好，清晰，不必深入看代码即可了解当前 commit 的作用。
    
*   为 Code Reviewing 做准备
    
*   方便跟踪工程历史
    
*   让其他的开发者在运行 git blame 的时候想跪谢
    
*   提高项目的整体质量，提高个人工程素质
    

### Commit message 的格式


每次提交，Commit message 都包括三个部分：header，body 和 footer。

```
<type>(<scope>): <subject>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>

```

其中，header 是必需的，body 和 footer 可以省略。  
不管是哪一个部分，任何一行都不得超过 72 个字符（或 100 个字符）。这是为了避免自动换行影响美观。

#### Header

Header 部分只有一行，包括三个字段：`type`（必需）、`scope`（可选）和`subject`（必需）。

##### type

用于说明 commit 的类别，只允许使用下面 7 个标识。

*   feat：新功能（feature）
    
*   fix：修补 bug
    
*   docs：文档（documentation）
    
*   style： 格式（不影响代码运行的变动）
    
*   refactor：重构（即不是新增功能，也不是修改 bug 的代码变动）
    
*   test：增加测试
    
*   chore：构建过程或辅助工具的变动
    

如果 type 为`feat`和`fix`，则该 commit 将肯定出现在 Change log 之中。其他情况（`docs`、`chore`、`style`、`refactor`、`test`）由你决定，要不要放入 Change log，建议是不要。

##### scope

scope 用于说明 commit 影响的范围，比如数据层、控制层、视图层等等，视项目不同而不同。

例如在`Angular`，可以是`$location`, `$browser`, `$compile`, `$rootScope`, `ngHref`, `ngClick`, `ngView`等。

如果你的修改影响了不止一个`scope`，你可以使用`*`代替。

##### subject

`subject`是 commit 目的的简短描述，不超过 50 个字符。

其他注意事项：

*   以动词开头，使用第一人称现在时，比如 change，而不是 changed 或 changes
    
*   第一个字母小写
    
*   结尾不加句号（.）
    

#### Body

Body 部分是对本次 commit 的详细描述，可以分成多行。下面是一个范例。

```
More detailed explanatory text, if necessary.  Wrap it to 
about 72 characters or so. 

Further paragraphs come after blank lines.

- Bullet points are okay, too
- Use a hanging indent

```

有两个注意点:

*   使用第一人称现在时，比如使用 change 而不是 changed 或 changes。
    
*   永远别忘了第 2 行是空行
    
*   应该说明代码变动的动机，以及与以前行为的对比。
    

#### Footer

Footer 部分只用于以下两种情况：

##### 不兼容变动

如果当前代码与上一个版本不兼容，则 Footer 部分以 BREAKING CHANGE 开头，后面是对变动的描述、以及变动理由和迁移方法。

```
BREAKING CHANGE: isolate scope bindings definition has changed.

    To migrate the code follow the example below:

    Before:

    scope: {
      myAttr: 'attribute',
    }

    After:

    scope: {
      myAttr: '@',
    }

    The removed `inject` wasn't generaly useful for directives so there should be no code using it.

```

##### 关闭 Issue

如果当前 commit 针对某个 issue，那么可以在 Footer 部分关闭这个 issue 。

```
Closes #234

```

#### Revert

还有一种特殊情况，如果当前 commit 用于撤销以前的 commit，则必须以 revert: 开头，后面跟着被撤销 Commit 的 Header。

```
revert: feat(pencil): add 'graphiteWidth' option

This reverts commit 667ecc1654a317a13331b17617d973392f415f02.

```

Body 部分的格式是固定的，必须写成`This reverts commit &lt;hash>`.，其中的 hash 是被撤销 commit 的 SHA 标识符。

如果当前 commit 与被撤销的 commit，在同一个发布（release）里面，那么它们都不会出现在 Change log 里面。如果两者在不同的发布，那么当前 commit，会出现在 Change log 的 Reverts 小标题下面。

### Commitizen


可以使用典型的 git 工作流程或通过使用 CLI 向导 [Commitizen](https://link.segmentfault.com/?enc=ARBP41T0jcn1D%2B43l0mlrw%3D%3D.2l8GsONUTKUB%2BA7hkcVvIZjVR4ABKncTLOQ2QiM0YhtyWW1ri9KLiP5XvuQkh%2Fod) 来添加提交消息格式。

#### 安装

```
 npm install -g commitizen

```

然后，在项目目录里，运行下面的命令，使其支持 Angular 的 Commit message 格式。

```
commitizen init cz-conventional-changelog --save --save-exact

```

以后，凡是用到`git commit`命令，一律改为使用`git cz`。这时，就会出现选项，用来生成符合格式的 Commit message。  
![](https://segmentfault.com/img/remote/1460000009048917?w=557&h=300)

### validate-commit-msg


[validate-commit-msg](https://link.segmentfault.com/?enc=LM8CxOQpCRTuHS77YdkhQg%3D%3D.04Exb2GiQgTOeyup3c7Dl3wqq%2Bkb4NdFLhyZBLAY4L2UrkZq2XcP%2BLGNXNVlj%2FR92wv0ubPsusJ2nvR9KiWR9A%3D%3D) 用于检查项目的 Commit message 是否符合 Angular 规范。

该包提供了使用 githooks 来校验`commit message`的一些二进制文件。在这里，我推荐使用 [husky](https://link.segmentfault.com/?enc=yiO0FwRdIESoDAcMBG82LA%3D%3D.ANEPBQCHbixpG%2F5vGcbj8K8R9K1CJl4vxYGuWqj2q0g%3D)，只需要添加 `"commitmsg": "validate-commit-msg"` 到你的`package.json`中的`nam scripts`即可.

当然，你还可以通过定义配置文件`.vcmrc`来自定义校验格式。详细使用请见文档 [validate-commit-msg](https://link.segmentfault.com/?enc=Wx3iMlF4r6TBhRxMZUSt2Q%3D%3D.cY9SIh4GILtZ%2FESeZeHtqSWrn%2BUnEoUKex8g0Z8H5%2B8%2BJakcNBJxi5MhLfRuVcGbdKhIZbIIFnWObdcwVUlqnQ%3D%3D)

### 生成 Change log

如果你的所有 Commit 都符合 Angular 格式，那么发布新版本时， Change log 就可以用脚本自动生成。生成的文档包括以下三个部分：

*   New features
    
*   Bug fixes
    
*   Breaking changes.
    

每个部分都会罗列相关的 commit ，并且有指向这些 commit 的链接。当然，生成的文档允许手动修改，所以发布前，你还可以添加其他内容。

[conventional-changelog](https://link.segmentfault.com/?enc=rTqlbUs1vZ85YfQU79YK8Q%3D%3D.%2FjLec6jGmf7foZbLRqtwFqUA8SbE1TdRq%2BkYNXc2KHxiXqxKm2yJ4S7ELjl9RJpJFGfZ4tLvYuWZ%2F%2FR%2FpgoR8g%3D%3D) 就是生成 Change log 的工具，运行下面的命令即可。

```
$ npm install -g conventional-changelog
$ cd my-project
$ conventional-changelog -p angular -i CHANGELOG.md -w

```

## 参考文章


[Commit message 和 Change log 编写指南](https://link.segmentfault.com/?enc=GLb5Tot0hyq76CML33g4gQ%3D%3D.dC%2FByT9XhhAfPnlgETdMAUxFDf8AdrKvd4m2NBrLJmLwowCVG8cewuGdNTzh9MJ1m3buSn8m3YZGU86N6HrPp6DYxt96K5X3f1rfypMZlS0%3D)  
[Angular.js Git Commit Guidelines](https://link.segmentfault.com/?enc=RiVMI9Hqes%2Fma9JetEW8Gg%3D%3D.TAeBitbr9gJgjwYT5pz9lzN2hS4sulCS9XOun2W4%2BiXUjVPOrr6Tfd1O6700ba3YA9vQ8laHqOQffa8ZMG0mnGTWFpCPwY20r9Kxl7a%2B%2BHA%3D)

[git commit 规范指南 - 不挑食的程序员 - SegmentFault 思否](/archive/git%20commit%20规范指南%20-%20不挑食的程序员%20-%20SegmentFault%20思否)

[Git 合并多个 commit，保持历史简洁 - 腾讯云开发者社区 - 腾讯云](/archive/Git%20合并多个%20commit，保持历史简洁%20-%20腾讯云开发者社区%20-%20腾讯云)