> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [www.cnblogs.com](https://www.cnblogs.com/onelikeone/p/12072558.html)

Q-dir 是轻量的文件管理器，特点鲜明，各种布局视图切换灵活，默认四个小窗口组成一个大窗口，操作快捷。软件虽小，却非常好用

下载地址：

[https://www.softwareok.com/?Download=Q-Dir](https://www.softwareok.com/?Download=Q-Dir)

64bit

[https://www.softwareok.com/Download/Q-Dir_Portable_x64.zip](https://www.softwareok.com/Download/Q-Dir_Portable_x64.zip)

软件版本：Q-Dir Version 7.99  x64  /  Dec 18 2019 14:25:13

**个人的定制项**

一、双击空白，返回上一级

![](https://img2018.cnblogs.com/blog/1164733/201912/1164733-20191220160137862-420732008.png)

二、通过快捷键快速启动 Q-dir 方法

a. 先固定到任务栏 (放置到第 1 个位置)

![](https://img2018.cnblogs.com/i-beta/1164733/202001/1164733-20200120141147613-1850757756.png)

b. win 按键 + 1  (即可快速启动)

三、打开文件夹时，默认使用 Q-dir 打开

![](https://img2020.cnblogs.com/i-beta/1164733/202003/1164733-20200313105020860-2079475731.png)

![](https://img2020.cnblogs.com/i-beta/1164733/202003/1164733-20200313105142102-854288553.png)

另一款利器：**Directory Opus**

[Directory Opus --- 布局灵活的文件管理，支持文件预览，强烈推荐](https://www.cnblogs.com/onelikeone/p/14610742.html)