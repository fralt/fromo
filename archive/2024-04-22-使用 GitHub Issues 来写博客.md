> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [zhuanlan.zhihu.com](https://zhuanlan.zhihu.com/p/400962805)

本文首发于「[效率工具指南](https://link.zhihu.com/?target=https%3A//mp.weixin.qq.com/s%3F__biz%3DMzAxMjY0NTY5OA%3D%3D%26mid%3D2649917076%26idx%3D1%26sn%3Dd20090e8fc66cd31cd93e0b978d071a6%26chksm%3D83a882b9b4df0bafae78793b2e0b541e6fb4c5c122d8f0b315c8a24e06ad18c6ea755bf96139%26token%3D1700052107%26lang%3Dzh_CN%23rd)」
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

文 / 彭宏豪，笔名 / 安哥拉

Hello 大家好，我是安哥。

去年用腾讯云搭建了一个**[可在国内正常访问的博客](https://link.zhihu.com/?target=http%3A//mp.weixin.qq.com/s%3F__biz%3DMzAxMjY0NTY5OA%3D%3D%26mid%3D2649893670%26idx%3D1%26sn%3D1f3f91248ade0d5f1bbe3c2046990d63%26chksm%3D83a82f0bb4dfa61d463dfe7af7ecafa21ac0cf1bee21b1b147990b83f657f687d3857db40bf8%26scene%3D21%23wechat_redirect)**，时间一晃而过，租用一年的服务器就快到期了，由于去年我是腾讯云的**新用户**，可以不到 100 块的价格租到为期一年的服务器。

但从新客户变成老客户，如果想要继续租用服务器的话，并**没有任何续费优惠**，反而是之后每年的价格都变高了，有点像是其他平台「**大数据杀熟**」的感觉。

以我现在租用的一台服务器为例，续费一年的价格是 692 元，一次性续费至少 3 年，才有所谓的 5 折折扣，总价为 1251 元，均摊下来一年也要 417 元。

![](https://pic4.zhimg.com/v2-1c3265be75944061e30e15aa72f3bd5b_b.png)

想着每年没写多少篇博客，也没多少人看，在这上面也赚不到钱，每年却要支出一笔费用，这让我有点犹豫要不要续费下去。

基于此，我又重新寻找**简单、不需要过多折腾、最好还能是免费**的建立个人博客的方法，正好这两天看到一个**基于 GitHub 的 Issues 功能来写博客**的开源项目。

![](https://pic3.zhimg.com/v2-ef679222c2b8eacd79b9e608b3ad1b2e_b.png)

**gitblog 项目地址：**

_[https://github.com/yihong0618/gitblog](https://link.zhihu.com/?target=https%3A//github.com/yihong0618/gitblog)_

对比我目前正在用的部署在腾讯云的 Hexo 博客，使用 GitHub 自带的 Issues 来写博客，我目前能感知到的**优点**有：

*   完全免费，不需要租用域名和服务器
*   不需要过多折腾，对不懂技术的人来说非常友好
*   Issues 自带评论功能，不需要后期为博客安装评论插件
*   GitHub 有网页版和手机 App，只要有网，我们可以随时随地发布博客，而不仅仅局限于电脑
*   发布到 GitHub 的内容，在 Google 的搜索结果中有非常高的权重，如果你写的内容比较优质，或者是许多人都会有的需求，也能有非常不错的曝光

除了这 4 个优点，这个开源博客项目的作者 @yihong 还提到了另外一些**使用 GitHub 仓库来写博客的优点**：

![](https://pic4.zhimg.com/v2-001b46979db6be9010cbc3a34d99eb2f_b.png)

看完使用这个方法来搭建博客的好处，接下来就简单介绍一下搭建的整个过程：

**01. 下载整个项目文件**
----------------

点击项目首页的绿色按钮 Code，在弹出的面板中，选择 **Download ZIP**，以压缩包的形式下载整个项目文件。

![](https://pic1.zhimg.com/v2-99c5945c039bf1540700db3097f4fffc_b.png)

下载解压得到的文件，对解压得到的文件进行修改。BACKUP 文件夹存放的是项目作者之前发布的博客的备份文件，你可以将这个文件夹删除。

![](https://pic3.zhimg.com/v2-f372412193b87179ae49c694449d1ec2_b.png)

打开 `.github` 文件夹，里面有一个 workflows 子文件夹，继续打开，可以看到一个名为 `generate_readme.yml` 的文件，在**记事本**或**代码编辑器**中打开这个文件。

![](https://pic1.zhimg.com/v2-db9845e26f7106beebb86aa75c648958_b.png)

这里我使用代码编辑器 **VS Code** 打开这个文件，需要修改两个地方，一个是 **branches** 的值，将其由原来的 master 更改为 **main**。

另一个是分别将下面的 **GITHUB_NAME** 和 **GITHUB_EMAIL** 替换成自己的 **GitHub 账号的 ID** 和**邮箱**。

![](https://pic4.zhimg.com/v2-9a48a4231ebf3456fba2f835255b525b_b.png)

还没有 GitHub 账号的朋友，请出门右转先注册一个 GitHub 账号：

_[https://github.com/signup](https://link.zhihu.com/?target=https%3A//github.com/signup)_

修改好上面两处地方之后，记得在关闭文件之前保存一下文件。

**02. 在 GitHub 创建一个新的仓库**
-------------------------

回到浏览器中的 GitHub 个人主页，点击右上角的加号，选择 **New repository** 创建一个新的仓库。

![](https://pic2.zhimg.com/v2-eee609347c8cc7ccafa8ce44a8ef29ad_b.png)

为你的新仓库起一个名字，名字可以是英文，也可以是英文与数字的组成，暂不支持中文仓库名。

![](https://pic2.zhimg.com/v2-5d015807eeba7bf42c8b840af5fdd9c5_b.png)

接着勾选下方的 **Choose a license**，从内置的许可证中选择一个**协议**，因为我们使用的是别人写好的代码，因此这里最好使用与原来相同的 **MIT 开源协议**。

最后点击底部的绿色按钮 **Create repository**，完成新仓库的创建。

![](https://pic1.zhimg.com/v2-ca978ed290a3318b87b1596865ea6e2c_b.png)

在新仓库中，点击右上角的 **Add file**，选择 **Upload files**，将之前下载到本地的文件上传到仓库中。

![](https://pic2.zhimg.com/v2-b95a9692d66d99c13093b5a9a89a8261_b.png)

上传本地文件，和平常上传文件到其他网站是一样的，这里需要上传的文件有 `main.py` 和 `requirements.txt`。

![](https://pic3.zhimg.com/v2-f372412193b87179ae49c694449d1ec2_b.png)

本地的文件夹 `.github` 中有一个名为 `generate_readme.yml` 的文件，由于 GitHub 网页版不支持直接上传文件夹，我们要使用另外一个选项 **Create new file**。

![](https://pic2.zhimg.com/v2-5ebffc707eabf7f465cf0bcad10afae1_b.png)

在左上角的文件名输入 `.github/` 才能创建一个文件夹路径，按照本地的文件夹路径，后面继续输入 `workflows/generate_readme.yml`。

![](https://pic1.zhimg.com/v2-b7da6044efe8d91995dd075828a54d74_b.png)

最终得到的**文件路径**和**文件名**如下图，接着将本地的 `generate_readme.yml` 文件的内容复制到下方的编辑窗口中。

![](https://pic2.zhimg.com/v2-a44f28d7701a1dc408fbcc30e87d9f7d_b.png)

复制过来之后，滑动到页面底部的绿色按钮 **Commit new file**，点击确认创建文件。

![](https://pic2.zhimg.com/v2-6137668b2c41d650d654709d20184f29_b.png)

**03. 获取 Token 并配置参数**
----------------------

为了让我们前面创建的 `generate_readme.yml` 可以顺利运行，我们还需要获取一个 **Token** 参数，并将其配置到仓库的 **Secrets** 中。

在浏览器打开网页 `https://github.com/settings/tokens` ，点击右上角的 **Generate new token**。

![](https://pic2.zhimg.com/v2-ba7eeb87fb9e4ec66cda9d27ffc3ee65_b.png)

Note 这里需要我们为即将生成的 Token **添加一个备注信息**，你可以随意填，也可以填入一点比较有意义的信息，譬如下图的 blog_token。

接着下方还要**开启权限**，为了避免后面程序运行时出错，这里建议**勾选所有复选框**，最后点击底部的绿色按钮 **Generate token**，生成一个 Token。

![](https://pic3.zhimg.com/v2-de612e5d5b087a3f4be1e1332705a3b2_b.png)

生成的 Token 是一长串数字和字母的组合，我们不需要记住它，只需要点击 Token 右侧的**复制**按钮，将其复制到剪贴板。

![](https://pic3.zhimg.com/v2-ad48e0bb293805441f0528434696ff46_b.png)

接着回到我们前面创建的博客仓库，点击 **Settings** >> **Secrets** >> **New repository secret**。

![](https://pic2.zhimg.com/v2-f84631d6c2e0f9d45d6b9f59a25e8fcd_b.png)

这里需要填入两个值，**Name** 填入 **G_T**，这个值是固定的，它与之前的 `generate_readme.yml` 文件中定义的**变量名**有关，变量名没有改变的话，值就是这一个。

下方的 **Value** 就填入刚刚我们复制到剪贴板的 Token 值，最后点击下方的 **Add secret** 即可。

![](https://pic4.zhimg.com/v2-9b3f0371dafb7d4906ca94a8e06289b3_b.png)

**04. 使用 Issues 发布第一篇博客**
-------------------------

完成上面的操作，就可以说是完成了写博客之前的所有配置，点击仓库顶部的 **Issues** 选项卡，接着点击右侧的 **New issue**。

![](https://pic1.zhimg.com/v2-1585d6eef7521a15004dd70f977af7d8_b.png)

在打开的新页面中，可以看两个文本框，一个是用来**添加博客文章标题的 Title 区域**，一个**用来输入或粘贴内容的编辑区**，Issues 这种设计也很符合我们平时写文章的习惯。

编辑好之后，点击右下角的 **Submit new issue**，就完成了文章的发布。

![](https://pic2.zhimg.com/v2-2447b36485d6ab6353640affc59dca45_b.png)

对于已发布的文章，如果想在发布后进行修改，可以点击右侧的 … 按钮，选择 **Edit** 切换到编辑模式，**编辑没有次数和字符数的限制**，不像已经诞生 9 年的公众号，每篇文章最多只能修改 20 个字。

![](https://pic1.zhimg.com/v2-6120ce96c4d2a73c0eb18981122c0854_b.png)

由于项目的作者用到了 GitHub 的另外一个功能——**GitHub Actions**，这是一个自动化操作，当我们创建或修改 Issues 中的文章时，它都会运行一次写好的程序，在仓库的首页生成或**更新 README 文件**。

这里就体现为，它会在下方的「**最近更新**」自动生成我们最近更新的文章列表，方便访问我们博客仓库的人第一时间看到。

![](https://pic3.zhimg.com/v2-17a2223bff22e49edcacb631e2815256_b.png)

因为我这个是今天刚建的博客，看起来还不够壮观，这里放一下项目的原作者 @yihong 创建已久的博客，这或许才能真正让人感受到记录 的意义：

![](https://pic1.zhimg.com/v2-91a7b153640312795eadbfa10ee4a560_b.png)

以上就是本次想和你分享的内容。

看完文章如果觉得对你有帮助的话，别忘了点击底部的「**点赞 / 在看**」鼓励一下我，谢谢。

**效率工具指南**：分享推荐效率工具，好的产品值得被更多人知道。
---------------------------------

码字不易，对你有帮助的话，求个三连，谢谢。

![](https://pic3.zhimg.com/v2-1ec28a3afcc3d550beea27bb35b0efde_b.gif)