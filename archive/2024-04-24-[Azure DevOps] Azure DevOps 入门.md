> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [zhuanlan.zhihu.com](https://zhuanlan.zhihu.com/p/358782331)

1. 什么是 Azure DevOps
-------------------

[Azure DevOps](https://link.zhihu.com/?target=https%3A//azure.microsoft.com/zh-cn/services/devops/%3FWT.mc_id%3DWD-MVP-5003763) 是由微软开发的服务平台，它提供了多种工具，可用于更好地进行团队协作。它还具有用于自动构建过程，测试，版本控制和程序包管理的工具。

Azure DevOps 提供了 5 个主要模块：

*   [Azure Boards](https://link.zhihu.com/?target=https%3A//azure.microsoft.com/zh-cn/services/devops/boards/%3FWT.mc_id%3DWD-MVP-5003763)：这些是敏捷的工具，可以帮助我们规划、跟踪和讨论我们的工作，甚至与其他团队一起工作。  
    
*   [Azure Repos](https://link.zhihu.com/?target=https%3A//azure.microsoft.com/zh-cn/services/devops/repos/%3FWT.mc_id%3DWD-MVP-5003763)：提供无限的、云托管的私人和公共 Git 存储库。  
    
*   [Azure Pipelines](https://link.zhihu.com/?target=https%3A//azure.microsoft.com/zh-cn/services/devops/pipelines/%3FWT.mc_id%3DWD-MVP-5003763)：使用适用于任何语言、平台和云的 CI/CD 进行构建、测试和部署。  
    
*   [Azure Test Plans](https://link.zhihu.com/?target=https%3A//azure.microsoft.com/zh-cn/services/devops/test-plans/%3FWT.mc_id%3DWD-MVP-5003763)：使用适用于应用的手动测试和探索测试工具来提高代码整体质量。。  
    
*   [Azure Artifacts](https://link.zhihu.com/?target=https%3A//azure.microsoft.com/zh-cn/services/devops/artifacts/%3FWT.mc_id%3DWD-MVP-5003763)： 与整个团队共享来自公共源和专用源的 Maven、npm、NuGet 和 Python 包。以简单且可缩放的方式将包共享集成到 CI/CD 管道中。  
    

除此之外，[扩展市场](https://link.zhihu.com/?target=https%3A//marketplace.visualstudio.com/azuredevops) 上还有超过 1,000 个应用和工具可供选择。

接下来的文章我会以 WPF 应用为例子简单介绍 Azure Boards、Repos、Pipelines 的使用。

2. 开始使用
-------

![](https://pic3.zhimg.com/v2-697608730102fc83841d99f29424899a_r.jpg)

如果只是个人使用的话可以使用在线的 Azure DevOps Services，使用 Microsoft 或 Github 帐户可直接登录使用，5 人以内免费。也可以选择 Azure DevOps Server，功能上基本一样，只是部署和收费模式不一样。

在 Services 里我调不出中文界面，好像听说过只有 Server 版本提供了中文界面，所以接下来的介绍都以英文界面为标准。

要使用 Azure DevOps Services，首先需要创建组织：

1.  访问 [dev.azure.com](https://link.zhihu.com/?target=https%3A//azure.microsoft.com/zh-cn/services/devops/%3FWT.mc_id%3DWD-MVP-5003763)。
2.  点击 “免费开始使用” 按钮。
3.  使用 Microsoft 帐户或 Github 帐户登录。
4.  阅读并同意许可协议。

![](https://pic1.zhimg.com/v2-1ef854d151e5590df739fb6ad1f7fb0c_r.jpg)

1.  然后，输入组织的名称及位置：

![](https://pic4.zhimg.com/v2-9c8abc131034038c49299c3c923a2e7f_r.jpg)

稍等一会，组织创建好以后就可以使用这个独一无二的组织名称访问组织的页面，例如：`https://dev.azure.com/xxxx-company/`

3. 创建项目
-------

现在在首页的右边，可以看到创建项目的表单，依次输入 “Project name”、“Description”、“Visibility”、“Version Control” 和“Work item process”。

Version control 可以选择 Git 和 TFVC，现在一般都选择 Git。 Work item process 有 Basic、Agile、Scrum 和 CMMI 四种选择，这里我选择了 Scrum，更多信息可以参考 [Choose a process like Basic, Agile, Scrum, or CMMI](https://link.zhihu.com/?target=https%3A//docs.microsoft.com/en-us/azure/devops/boards/work-items/guidance/choose-process%3Fview%3Dazure-devops%26tabs%3Dbasic-process%26WT.mc_id%3DWD-MVP-5003763)。

最后点击 “Create project” 创建项目。

![](https://pic4.zhimg.com/v2-1a25fbf487cbfea2418e513f1db21c6b_r.jpg)

完成后，首页上显示了 “WPF” 这个项目，可以点击进入项目的页面。

4. 管理团队
-------

创建组织和项目后，如果需要拉人入伙，还需要管理他在团队中的位置。例如项目中有 Programer 和 Tester 两个团队，分别有不同的权限、接收不同的通知。管理用户首先需要创建它所在的团队，不过现在只是 Demo 项目就一切从简。在这个项目中，暂时只有一个团队 “wpf Team”，现在将刚刚添加的用户放进这个团队：

1.  进入 wpf 项目，点击左下角的 “Project settings” 进入 Project Settings 页面，在左侧菜单选中 Teams。
2.  进入 Teams 页面，在列出的 团队中选中 “wpf Team”。
3.  在 Members 列表的右上角，点击 “Add” 按钮。
4.  在 “Invite members to wpf Team” 表单中输入刚刚添加的用户，选择“Save”。

现在，团队有两个成员了。

5. 最后
-----

现在我们已经创建了组织和项目，还添加了团队成员，下一篇文章将会介绍如何使用 Azure Boards 管理工作项。

更多内容请参考官方文档：

[Azure DevOps documentation _ Microsoft Docs](https://link.zhihu.com/?target=https%3A//docs.microsoft.com/zh-cn/azure/devops/%3Fview%3Dazure-devops%26WT.mc_id%3DWD-MVP-5003763)