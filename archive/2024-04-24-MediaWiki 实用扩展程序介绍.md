> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [zhuanlan.zhihu.com](https://zhuanlan.zhihu.com/p/64283626)

2022 年 5 月 22 日更新
-----------------

### 写在前面

下文中提到的 Tabber 扩展已经停用了，现在改为 [TabberNeue](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3ATabberNeue)，当然我仍然倾向于用 [HeaderTabs](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3AHeader_Tabs)。  
Math 扩展从 1.38 版以后内置了，还是蛮期待的，有兴趣的可以去测试下。

### [HidePrefix](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3AHidePrefix)

作者就是觉得前缀很难看，所以才开发了这款扩展，因为他觉得用户根本就不关心你的前缀是什么。先看看效果：

![](https://pic4.zhimg.com/v2-229e7d6fee6dd22a2c8baf2fff93d407_r.jpg)

注意页面标题和地址栏中的页面标题，少了前缀`图书:`，确实看上去舒服多了。  
不过该扩展也有另外一个后遗症，就是在搜索结果中，如果关键字在标题中则会有前缀显示而不在标题中的则省略掉了前缀，所以强迫症患者慎用。

### [ShortDescription](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3AShortDescription)

![](https://pic4.zhimg.com/v2-86f70a9e323df199802efcb343874f03_r.jpg)

在标题下面加上简介，维基百科有类似功能，不过是通过模板的形式。另外 WikiData 有专用的简介魔术字，跟该扩展同名，不过既然我们用不上 WikiData 的数据，不如自己弄一个，结合语义化扩展更是如虎添翼。

### [Lingo](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3ALingo/zh)

先看图：

![](https://pic2.zhimg.com/v2-2c99a2b9c07748d13ba07bc166eeb6f9_b.jpg)

对于有缩写词需求的可以用上。

### [AutoSitemap](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3AAutoSitemap/zh)

自动生成 “stitemap.xml” 文件。

### [WikidataPageBanner](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3AWikidataPageBanner/zh)

![](https://pic1.zhimg.com/v2-6ff71c1da2faf8b922547238a06e5758_b.jpg)

虽然很早就用上了，但是一直没办法设计一条好的横幅，所以选择性地没放在之前的介绍中。

### [CSS](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3ACSS/zh)

对的，就是这么简单直接，有了它你可以直接在页面中嵌入 CSS 代码或者是 CSS 文件。至于是选择模板样式还是它，看自己的喜好了（前者用于维基基金会，后者用于 MyWikis 等维基农场，维护应该都不是什么问题）。

2021 年 9 月 13 日更新
-----------------

再次补充一个扩展，也是网友在其他文章的回复中提到过的，那就是 [Labeled Section Transclusion](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3ALabeled_Section_Transclusion/zh)。

> 标记式章节嵌入扩展允许选择性地嵌入已标记的章节文本并正常解析 Wiki 格式。它的功能类似普通维基中‎<onlyinclude> 标签的增强版本，可选择要包含的部分。这已在所有维基媒体的 wiki 上启用。  
> 普通嵌入主要用来将小模板的大部分内容传入，而标记式章节嵌入主要用于将大型页面的一小部分嵌入。  
> 两者也存在一些差异。在原生的模板嵌入中，章节是以结果标记，因此只能完全包含或跳过一个可能是不连续的章节。  
> 而在这里，章节以名称标记，且由调用者选择行为，可以按需要包含或跳过章节。不同的页面可以包括或排除自己选择的章节，不限数量且可任意重叠。  
> 通过名称而非行为标记章节将允许 “编辑章节” 链接对从较大文本中摘录的内容更适当体现，因为扩展现在可以考虑在页面开头跳过的部分，从而允许适当地偏移包含的部分。

这是官网给的说明，简单来说就是它能实现将某个条目中的章节嵌入到新的条目中，并且可以自定义哪些章节需要嵌入而无需考虑是否连续。  
如果你去查看维基百科的某些模板或模块的说明文档，你可能会发现之前很多重复工作维基都是用模板来实现的，比方说我把某些信息汇总成一个表格并保存为一个模板，需要的时候加入该模板即可。而采用 Labeled Section Transclusion 之后，只要你在某个特定页面建立过该表，那么通过`{{#lst:xxxx}}`的方式同样可以实现这种效果。至于选哪种方式就看自己的习惯了，不过在我看来，后者将大大减少模板的数量。

2021 年 1 月 11 日更新
-----------------

最近又陆陆续续地测试了几个扩展，包括官方系统中新增的，补充如下：  
开始前先上几张图，下面很多扩展都跟它们有关。

![](https://pic3.zhimg.com/v2-f921596fcb02eaaec6aa9de2224fafc2_r.jpg)![](https://pic1.zhimg.com/v2-f70e3cfac03899d87270312b348b6260_r.jpg)![](https://pic2.zhimg.com/v2-31588565bab12d88b41a780b4ea9e33d_r.jpg)

### [Cargo](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3ACargo)

> Cargo 是 MediaWiki 的一项扩展，可以提供轻量的方式来存储和查询数据，这些数据是通常通过信息框（infobox）等模板调用的。Cargo 在概念上类似于 Semantic MediaWiki 扩展，但是有许多优点，比如容易安装，容易使用。  
> Cargo 将所有的数据存在数据库表中，而这些表可以被 MediaWiki 自己的数据库（默认）或分开的数据替换。在大多数情况下，每个模板都会将其数据存储在单个数据库表中，每个相关模板参数对应一列。有少量例外：包含数组 / 列表以及包含地理位置的字段的处理会更复杂一些，这是因为大多数数据库系统缺乏对这些类型的原生支持。多个模板也可以将它们的数据存储在同一个数据库表中。

跟 Semantic MediaWiki 一样，都是用于语义化的插件，只是复杂程度会比后者小很多，适合新手入门。网上教程较少，官方的文档很多地方讲得不是很清晰，需要慢慢琢磨。  
自打用上这个和后面的 Page Forms，简直就是为自己打开了新世界的大门，MediaWiki 的可玩性瞬间提高 N 倍。  
有兴趣的可以参考本站的文章《[MeidaWiki 教程之 Cargo 篇](https://link.zhihu.com/?target=https%3A//tech.mindseed.cn/MediaWiKi/626.html)》。

### [Page Forms](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3APage_Forms)

> 页面表单（Page Forms），在 2016 年以前曾称语义表单（Semantic Forms），是 MediaWiki 的一个扩展，可供用户利用表单的方式，来新增、编辑和查询数据。 在最初创建时，该扩展仅仅是 Semantic MediaWiki 扩展的分支之一，旨在能够利用 SMW 来编辑模板，并存储模板参数。这也正是为何它曾经称作 “语义表单” 的原因。不过，如今，该扩展也可以与另外的扩展: Cargo 扩展配合使用，或在两者皆未安装的情况下工作。  
> 很简单地说，页面表单可让您能够在自己的 wiki 站点之上备有用来创建和编辑页面的表单以及用来查询数据的表单，且无须进行任何的编程。不但管理员可以创建和编辑表单，用户自己也可以这样。

这款插件最初就是为了配合 Semantic MediaWiki 使用的，如今同样能够适用于 Cargo，当然你也可以单独使用。它可以通过表单的方式快速添加数据，如果能够熟练掌握，某些特定类型的条目基本上可以直接用表单生成了。  
本站的《[如何用插件创建我的第一个 MediaWiki 信息框模板](https://link.zhihu.com/?target=https%3A//tech.mindseed.cn/MediaWiKi/604.html)》中也已经提到过它（那是我第一次接触 Page Forms），有兴趣的可以去翻看，至于教程文章正在翻译整理中，敬请期待。

### [VEForAll](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3AVEForAll)

这个完全是为了 Page Forms 而生的扩展，前身就是 VEForPageForms，它的作用就是为了能够在表单中使用可视化编辑器。因为从 1.31 版之后，表单对 WikiEditor 的支持就有问题，需要修改源码。索性就用可视化编辑器好了，尤其是 1.35 版之后，不再需要单独安装 Parsoid 服务，便捷度大大提高。  
上面第二张图中，条目内容就是开启了可视化编辑器的效果，在插入部分只支持少数（至少不支持模板，尝试开启还是失败了）。  
目前为测试版，可能会存在不确定的错误，比如跟下面的扩展 “神仙打架”。

### [HeaderTabs](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3AHeader_Tabs)

**_补充：最近还是用回了 Tabber，HeaderTabs 虽然显示效果要强于前者，但因为它把选项卡的标签定义为一级标题（H1)，所以从结构上来看欠妥。_**

想要使用选项卡，那么还是推荐使用这个，虽然文后提到另一款 Tabber，但个人还是喜欢这款（上面第一张图有显示效果）。  
**注意：**

1.  该扩展也可以用于表单中，但是可能会因为 CSS 的关系导致 VEForAll 无法正常使用，所以还是老老实实放在页面内容中好了。
2.  如果你把所有内容全部做成选项卡来显示，那么 popup 扩展的显示效果就会变成空白，所以最理想的方式就是不要将简介内容放进去（哪怕写简单一两句至少弹窗时也有内容显示）。

### [SemanticRating](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3ASemantic_Rating)

一个样式扩展，功能很简单，就是显示评分的 “小星星”（上面第一张图，书籍信息框最下面的评分就是它生成的）。  
**注意：**这款插件在可视化编辑器中可能会出错，导致模板内容看不见，但不影响使用。

### [Lockdown](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3ALockdown)

不想随随便便让人把你网站上的劳动成果窃取掉，那么就可以使用这款插件（虽然不符合互联网的开放精神）。该扩展可以限制特定的用户群体对特定命名空间和特殊的页面的各类动作（包括浏览、编辑等）。

### [CommentStreams](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3ACommentStreams)

MediaWiki 的评论系统，需要的人可以考虑（官方的样式跟网站有点格格不入，可以考虑美化一下）。

![](https://pic2.zhimg.com/v2-42c27da0d3c9e9e00ce07c586c8c37b1_r.jpg)

### [WikiSEO](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3AWikiSEO)

SEO 扩展，允许你添加常见的例如 “关键字” 和“描述”等元信息。

2018 年 9 月 29 日
---------------

_其实早在几个月之前就已经写好了这篇文章，只是一直懒得贴到知乎上，因为图片比较多，还要一一对号入座。_

才发现一个有意思的现象，MediaWki 的安装包里竟然自带了很多的插件，只是都没有启用。它是希望我们自己探索并发现吗？

这两天开始逐渐地倒腾起插件来，往站点上传的时候才注意到大凡实用的插件它都带了，可能会存在版本略低的现象。对于使用惯了默认不带插件或者仅有一两个插件的其他系统来说，官方内置这么多还真有些受宠若惊了！

**注：这段话写于第一次安装完 MediaWiki 之时，估计那时连配置过程都没好好研究，所以才会提出这种问题。**

相对于 WordPress 这种只带一两个插件的做法，MediaWiki 实在是太厚道了，安装包内竟然自带了这么多的扩展。而且随着版本的不断升级，所带的扩展也越来越多。

这次趁着升级，把用到的插件都整理一下，不然时间一久难免又忘。

官方自带扩展
------

以下扩展截止于 1.31.1 版（感觉这一版自带的扩展更多），并以字母顺序排列。

### **[CategoryTree](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3ACategoryTree)**

动态导航分类结构。

![](https://pic4.zhimg.com/v2-6925fbd81888256b74e8cba659fbe937_b.jpg)

### **[Cite](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3ACite)**

增加用于引用的`<ref[ name=id]>`和`<references/>`标签。

![](https://pic3.zhimg.com/v2-ca27819e437682ed1109362cd194a272_r.jpg)

添加参考引用的必备插件。

###   
[CiteThisPage](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3ACiteThisPage)

添加引用特殊页面和工具箱链接。

![](https://pic2.zhimg.com/v2-6e42453d21ec445979715c126bd866e5_r.jpg)

可以将条目的引用生成不同的引用格式，除了像维基百科这种相对具有权威性的站点，其引用还有些价值，个人站点貌似不实用。

### [CodeEditor](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3ACodeEditor)

使用 Ace 编辑器实现编辑 JavaScript 和 CSS 页面时的语法高亮功能。

![](https://pic4.zhimg.com/v2-a31eee2e63f4f699d7b4567387a2c9ff_r.jpg)

这个在编辑代码（更准确的说是在线浏览代码，微调代码）时有用，但如果只是从其他站点搬运代码过来的话，用处同样不大。

### [ConfirmEdit](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3AConfirmEdit)

提供验证码技术防止垃圾信息和密码破解破坏。

![](https://pic3.zhimg.com/v2-703bd5454dcad48900032f66d2df71b6_r.jpg)

见仁见智的插件，安全这东西不用麻烦，用了也麻烦。

### [Gadgets](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3AGadgets)

![](https://pic2.zhimg.com/v2-fa785cca6b2663b92b543c31f958e90d_r.jpg)

MediaWiki 的小工具扩展工具，可以说是整个系统强大功能的基石。

这部分可以单独开一篇文章来介绍。

### [ImageMap](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3AImageMap)

容许客户端可以使用`<imagemap>`标签整可点击图像地图。

![](https://pic4.zhimg.com/v2-077f534a45e9075b3d54540f4c48db7f_b.jpg)

鼠标在图片不同区域会有不同的动作，但是编辑数据比较麻烦，实用性不高。

### [InputBox](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3AInputBox)

允许包含预先设置的 HTML 表单。

![](https://pic4.zhimg.com/v2-02ae8ccda4f502af411ee2ef0ae78b2b_b.jpg)

### [Interwiki](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3AInterwiki)

新增特殊页面以查看和编辑跨 wiki 表。

![](https://pic4.zhimg.com/v2-94e12b76867967c9d2c96e3a2ef15a6b_r.jpg)

除了像维基百科这种大型站点存在跨 wiki 的情况，基本上个人站长都用不到。

### [LocalisationUpdate](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3ALocalisationUpdate)

自动更新本地化的信息，换句话说，就是在使用过程中如果发现系统、插件和皮肤有适合自己语言的更新就升级。

### [MultimediaViewer](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3AMultimediaViewer)

在全屏界面中以较大尺寸显示缩略图。

![](https://pic3.zhimg.com/v2-1c76d242e9a29eeb987a1cab827f68fe_r.jpg)

### [Nuke](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3ANuke)

让管理员可以批量删除页面。

![](https://pic1.zhimg.com/v2-3ffc9063ed5f644f4eaa12d3f7f288ec_r.jpg)

### [OATHAuth](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3AOATHAuth)

提供使用基于 HMAC 的一次性密码的身份验证支持。

### [ParserFunctions](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3AParserFunctions)

用逻辑函数加强解析器。如果你不希望自己从维基百科上下载的模板无法使用，那还是老老实实启用它。

### [PdfHandler](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3APdfHandler)

在图像模式中查看 PDF 文件的处理器。

### [Poem](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3APoem)

添加`<poem>`标签用于诗歌格式。

![](https://pic1.zhimg.com/v2-4283472d41e644becbae388470c022c0_r.jpg)

装不装就看你的需求了。

### [Renameuser](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3ARenameuser)

添加更改用户名的特殊页面（需要 renameuser 权限）。

![](https://pic2.zhimg.com/v2-14539392ef816d81198c28bd1c1ea579_r.jpg)

如果站点的用户数不多，其实用处不大。甚至有很多个人的维基站都是站长自己在维护，所有条目也都出自站长之手就更没有必要了。

### [ReplaceText](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3AReplace_Text)

允许管理员在内容页面的文本和标题上进行全局字符串查找和替换。

![](https://pic1.zhimg.com/v2-888e72654be842e1a845192337da2c10_r.jpg)

### [SpamBlacklist](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3ASpamBlacklist)

基于正则表达式的反垃圾工具允许将页面 URL 及注册用电子邮件地址列入黑名单。

![](https://pic3.zhimg.com/v2-28ae868ca04f271426c1acfdde5adef6_b.jpg)

### [SyntaxHighlight_GeSHi](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3ASyntaxHighlight)

使用 Pygments - Python syntax highlighter 以提供语法高亮`<syntaxhighlight>`。

![](https://pic1.zhimg.com/v2-fb41982040c5bbff502e245fc7238114_b.jpg)

### [TitleBlacklist](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3ATitleBlacklist)

允许管理员通过黑名单和白名单禁止页面和用户帐户的创建。

![](https://pic1.zhimg.com/v2-72e7e10690ac1c7d88862f69536ae9a4_r.jpg)

### [WikiEditor](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3AWikiEditor)

提供高级、可扩充的 wiki 文本编辑界面。

![](https://pic1.zhimg.com/v2-52928a89402fe2a14cb97407cfb51a7c_r.jpg)

维基的经典编辑器，功能简单，跟可视化编辑器 VisualEditor 比起来简直弱爆了，但其优点就是稳定。另外，如果你从维基百科等上扒资料的话，使用 wiki 编辑器进行复制粘贴会方便很多。

其实以上所带的官方插件有很多都用不上，如果你去看看维基网站甚至是 MediaWiki 自己的官网，就会发现他们也只用到一部分，所以安装或升级时可以考虑删掉部分无用的。  
**注：**即便删了，官网都会提供单独的插件可供下载。

接下来我们看看维基百科这个业内标杆所带的插件（以下按照使用率和功能性排序）。

### [VisualEditor](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3AVisualEditor)

MediaWiki 的可视化编辑器。

![](https://pic3.zhimg.com/v2-a4582ab386dc6a5ed5f9cbd3d35dd05a_r.jpg)

对新手而言绝对是编辑利器，安装颇为麻烦（需要 parsoid 的支持），后续会开一篇如何安装可视化编辑器的文章。

### [TemplateData](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3ATemplateData)

实现模板参数的数据存储（使用 JSON）。

![](https://pic3.zhimg.com/v2-ba3e4f79eb84154cbecd9cef0bb97d76_r.jpg)

配合可视化编辑器使用的一大利器，加入模板数据之后，可以自动生成模板参数表方便用户查阅，更重要的是在编辑条目时可以方便快速地添加数据。

![](https://pic3.zhimg.com/v2-5515657d7737dfed20df862256203eca_r.jpg)

### [RelatedArticles](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3ARelatedArticles)

添加链接至相关页面的页脚。

![](https://pic2.zhimg.com/v2-964a6329d0492f26ffae36b4ed247ac9_r.jpg)

增加同类主题的相关页面，方便用户的阅读体验。可以根据 Cirrus Search 在算法上确定也可以手动添加。

### [Disambiguator](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3ADisambiguator)

添加标签`__DISAMBIG__`以标记消歧义页面。并且增加两个新的特殊页面：

*   Special：DisambiguationPages - 列出维基上的所有消歧页面。
*   Special：DisambiguationPageLinks - 列出维基上链接到消歧页面的所有页面。

### [Popups](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3APopups)

当用户在页面链接上悬停时显示预览。

![](https://pic3.zhimg.com/v2-a2378a6791ae0e33a8620e5bdcbf5c7e_r.jpg)

当用户悬停在一个条目链接时，显示带有条目内容摘要的弹窗。

### [TimedMediaHandler](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3ATimedMediaHandler)

音频、视频和字幕的处理程序，支持 WebM、Ogg Theora、Vorbis、srt 格式。

![](https://pic1.zhimg.com/v2-ad58bce992955e081a1ba91b77fe3500_r.jpg)

如果你需要偶尔上传一些视频文件又不希望借助第三方平台的话，可以考虑安装。个人站点同样不太建议安装，毕竟空间和性能都摆在那里。

### [Echo](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3AEcho)

用于通知用户有关活动和消息的系统。

![](https://pic2.zhimg.com/v2-dc7cea5245b8426c667c92425c47b849_r.jpg)

### [CodeMirror](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3ACodeMirror)

在 wiki 文本编辑器中提供语法高亮显示。

![](https://pic2.zhimg.com/v2-73a46017d6dc9f85491b655da3c33aa1_r.jpg)

如果习惯了使用 wiki 编辑器，这个功能倒是比较实用。

### [Math](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3AMath)

在`<math>`...`</math>`标签间生成数学公式。

![](https://pic3.zhimg.com/v2-c0a07dd2b65c5efcd5daa4f6597f32ba_b.jpg)

### [RevisionSlider](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3ARevisionSlider)

显示一个滑块，它允许在差异页面上选择和比较修订。

![](https://pic4.zhimg.com/v2-8927890510391da12f3ac0b9442db893_r.jpg)

可以更容易地在修订版本间移动。

### [TemplateWizard](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3ATemplateWizard)

提供模板插入向导的 Wiki 编辑器插件。

![](https://pic4.zhimg.com/v2-fe8fa8a342af7d7a1685489f1bb6833b_r.jpg)

如果你使用的是可视化编辑器，那么这个界面你可能已经很熟悉了。如果你还是喜欢采用 wiki 编辑器但又希望增加使用体验那么可以考虑采用本插件。

### [CirrusSearch](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3ACirrusSearch)

在`Special:Search`中提供高级搜索功能的轻松访问。

![](https://pic2.zhimg.com/v2-6755312c62ec2f8b2bd3a9682e6a1ee9_r.jpg)

增强功能之一，提高搜索效率。目前仍在测试阶段，所以请谨慎使用。

### [Flow](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3AStructuredDiscussions)

讨论系统，又称 StructuredDiscussions。

![](https://pic3.zhimg.com/v2-6b8d141aeba092930a82051d3cf0ee9a_r.jpg)

协作编辑或者是多用户反馈可用，个人站点请斟酌。

### [Quiz](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3AQuiz)

允许创建测验问卷。

![](https://pic1.zhimg.com/v2-032c156b23e59dfe98c26ac7016a44dc_r.jpg)

这其实是为维基教育准备的一个插件，主要用来做试卷测试的，当然你也可以用它实现更多功能。

### [TemplateStyles](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3ATemplateStyles)

实施基于模板的样式表。

引入一个`<templatestyles>`标签，将其放在模板中允许模板具有自定义样式，而无需将它们放在 MediaWiki：Common.css 中。

### [TemplateSandbox](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3ATemplateSandbox)

呈现一个有传输自沙盒的模板的页面。

![](https://pic1.zhimg.com/v2-b054206ff7ed7c32983140b7c42b13d8_r.jpg)

为了鼓励用户大胆尝试编辑和修改模板及其他页面而无需担心造成破坏，建议使用沙盒模式。在维基百科中是一个很实用的功能，但在个人站点中基本可以忽略。

### [Abuse Filter](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3AAbuseFilter)

对编辑行为自动进行条件判定。

![](https://pic2.zhimg.com/v2-01843c3b62ea4b3f7d182c4822864b05_r.jpg)

允许具有权限的用户设置当用户的操作（例如编辑）匹配特定标准时，要进行的特定操作。个人站点用户数量少的情况可以不考虑。

### [UrlShortener](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3AUrlShortener)

任意 URL 的缩短器。

![](https://pic1.zhimg.com/v2-5bbded0cdbbfe09db974d163edbc8850_r.jpg)

生成短链的扩展，需要 mod_rewrite（或等效）的功能。玩玩可以，实用性同样不强。

### [EasyTimeline](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3AEasyTimeline)

添加`<timeline>`标签以创建时间轴。

![](https://pic1.zhimg.com/v2-5f5ea996dd1cadf5c6af1f39d1c69cb8_r.jpg)

简单的时间轴工具（默认的样式粗糙），如果你需要做些里程碑或者历史图表可以考虑一下。

### [Graph](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3AGraph)

允许`<graph>`标签或全部页面成为基于 Vega 的图形。

![](https://pic4.zhimg.com/v2-26b68c5717b3795f70a68d37745fcf7f_r.jpg)

专业插件之一（看着好高大上的感觉），反正我不懂 Vega，有兴趣的可以看看 [Demo](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3AGraph/Demo)。

### [Kartographer](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3AKartographer)

允许将地图添加至 wiki 页面。

![](https://pic1.zhimg.com/v2-705d0243dd4dae6b5b1cf2ce2fc729f4_b.jpg)

采用 OpenStreetMap 的地图数据，汉化一般。

### [MsUpload](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3AMsUpload)

为一个或多个文件同时开启轻松上传。

![](https://pic2.zhimg.com/v2-c365ebe5f0ed361f74d6538c14608b95_r.jpg)

如果你使用的是 wiki 编辑器，那么这个插件可以让你轻松地批量上传文件。如果你习惯用可视化编辑器的话，也无所谓了。

### [Tabber](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3ATabber)

允许在页面内创建标签页。

![](https://pic1.zhimg.com/v2-fccd5d0ac3574e64009e0820bde502c4_b.jpg)

如果你想让你的维基站点有点不一样的特色，或许可以尝试一下。

### [MobileDetect](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3AMobileDetect)

检测移动设备并允许使用`<nomobile>`和`<mobileonly>`标签控制在不同情况下的显示内容。

### [RandomSelection](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3ARandomSelection)

显示给定集合中的随机选项，并且根据选项可以生成随机内容（比如精选文章、随机问候等）。千万不要跟 MediaWiki 的随机页面搞混了。

### [External Data](https://link.zhihu.com/?target=https%3A//www.mediawiki.org/wiki/Extension%3AExternal_Data)

> 该外部数据扩展允许链接到 MediaWiki 网页进行检索、过滤和格式化从一个或多个源的结构化数据。这些来源可以包括外部 URL、常规 Wiki 页面、上传的文件、本地服务器上的文件、数据库或 LDAP 目录。

总之看上去很强大，但还没到研究它的地步（精力不够）。

### [Semantic MediaWiki](https://link.zhihu.com/?target=https%3A//www.semantic-mediawiki.org/wiki/Semantic_MediaWiki)

让您更亲近 wiki——对机器与人都是如此。高阶玩家的扩展。

好了，以上就是常用的各种插件，大家有兴趣可以慢慢尝试。

以上同样来自本人的个人小站

[MediaWiki 实用扩展程序介绍 - 心站日志](https://link.zhihu.com/?target=https%3A//tech.mindseed.cn/Tutorial/MediaWiki%25E5%25AE%259E%25E7%2594%25A8%25E6%2589%25A9%25E5%25B1%2595%25E7%25A8%258B%25E5%25BA%258F%25E4%25BB%258B%25E7%25BB%258D.html)