> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [zhuanlan.zhihu.com](https://zhuanlan.zhihu.com/p/667619738?utm_psn=1756791397623988224)

前情回顾
----

[Mr Figurant：高级宏观 10：资产定价模型](https://zhuanlan.zhihu.com/p/667606613)

本节要点
----

参考大卫 · 罗默的教材和焼田党的教材，本文主要梳理了以下内容：

*   无增长世代交叠模型：引入异质性代理人（heterogeneous agents）的简单方法
*   有增长世代交叠模型：人口驱动型增长；动态无效率（Dynamic Inefficiency）
*   世代交叠框架中的生育（Fertility）：人口内生增长与社会保障计划的影响

考试大纲：

*   世代交迭模型和李嘉图等价

*   1. **世代交迭模型**相比拉姆齐模型的优缺点
*   2. **两代世代交迭模型**：一般均衡、动态无效及其根源
*   3. 李嘉图等价与不等价的含义

核心概念
----

**问 1**：**世代交叠模型有什么特点？世代交叠的过程是怎样的？**

**答**：世代交叠模型引入了异质性代理人，具有同代同质、同期异质的特点。每个代理人都有一个固定的、有限的周期，而每期都有不同的一代人处于生命的不同阶段。年轻人提供劳动，得到劳动收入。年轻人会花掉一部分收入，把剩下的存起来，变成老年人后，这些储蓄转化为资本供给。如此反复，形成储蓄和消费的世代交叠。

**问 2**：**为什么世代交叠模型是动态无效率的？有什么解决方法？**

**答**：动态无效率的根源在于经济的跨期结构。在分散经济中，在第二个时期分配资源用于消费的唯一途径是在第一个时期进行储蓄。计划者的解决办法是，让年轻人与老年人签订跨期合同，在一个时期内将资源分配给不同的一代，将商品从年轻人手中转移到老年人手中，当年轻人变老时，重复这个转移方案。

**问 3**：**当税率增加时，稳态生育率的变化在无托儿服务和有托儿服务的情况下有什么差异**？

*   **无托儿**：增税降低了税后工资率，即养娃的机会成本，因此父母增加孩子的数量。子女数量的增加往往会使每位退休人员的社会保障福利增加，因为在这种情况下工资率是外生的。每个子女的抚养时间是不变的，意味着增加了父母养娃的时间。
*   **有托儿**：和无托儿一样，稳态下养娃的时间随着税率增加。稳态的托儿服务对税率的反应是不确定的，取决于谁占主导：一方面，较高的育儿时间增加了托儿服务的边际产品，因此其需求更高；另一方面，增税会降低工资率，这导致市场对托儿服务的需求降低。

1. 无增长 OLG
----------

本节和下一节主要参考罗默第二章 B 部分的内容。

### 代理人

*   离散时间
*   个体生活在两个时期：年轻和年老
*   $t$t 时期出生的一代由一个具有竞争行为的个体代理人

设代理人的生命中两个时期的消费为 $c^t_ 1, c^t_ 2$c^t_ 1, c^t_ 2 ，第 $t$t 代的代理人的效用函数为 $u(c^t_ 1, c^t_ 2)$u(c^t_ 1, c^t_ 2) ：

*   $c^t_ 1$c^t_ 1 ：在 $t$t 期发生
*   $c^t_ 2$c^t_ 2 ：在 $t+1$t+1 期发生

禀赋（Endowment）：代理人第一阶段的一单位劳动

*   总劳动供给是年轻的代表代理人的禀赋， $L = 1$L = 1

*   假设人口没有增长

*   代理人的一生分为工作年限（年轻）和退休年限（年老）

*   下文称两类人群为年轻人和老年人

### 生产技术

*   有一种独特的商品，既可以消费，也可以作为资本使用
*   根据 $Y = F(K,L)$Y = F(K,L) ，生产发生在每个时期

*   $K$K 是资本，没有折旧
*   生产函数的规模收益为常数

*   在生产部门有大量相同的公司

*   所有人都是价格接受者，并由一家公司代表

### 期内事件顺序

*   ① 劳动力和资本要么被用于生产，要么支付边际产品

*   年轻人：提供劳动 $L_t$L_t ，得到劳动收入 $w_tL_t$w_tL_t
*   老年人：提供资本 $K_t$K_t ，支付资本收益 $r_tK_t$r_tK_t

*   ② 生产品销售给代理人

*   代理人的收入与产量相等： $w_tL_t + r_tK_t = Y_t$w_tL_t + r_tK_t = Y_t

*   因为欧拉定理，以及生产函数是一次齐次的

*   代理人要么消费，要么储蓄

*   ③ 老年人消费他们提供资本的收入： $(1 + r_t)K_t$(1 + r_t)K_t

*   不存钱，因为没有下一期

*   ④ 年轻人会花掉一部分收入，把剩下的存起来

*   当储蓄者进入人生第二阶段时，储蓄转化为资本供给

*   ⑤ 在下一个时期，劳动力是给定的，资本来自先前的储蓄

*   回到步骤①

### 均衡

用 $k_t$k_t 和 $y_t$y_t 表示每单位资本和劳动的产出。当没有人口增长的时候：

*   $L = 1$L = 1
*   $k_t=K_t$k_t=K_t
*   $y_t=Y_t$y_t=Y_t

均衡中：（推导见[高级宏观 05：拉姆齐模型](https://zhuanlan.zhihu.com/p/658837650)中的数学附录）

*   $r_t = f'(k_t)$r_t = f'(k_t)
*   $w_t = f(k_t) − k_tf'(k_t)$w_t = f(k_t) − k_tf'(k_t)

这些都是竞争企业的最优条件。

第 $t$t 代的代理人会最大化 $u(c^t_ 1, c^t_ 2)$u(c^t_ 1, c^t_ 2) ，约束为：

> $\displaystyle c_1^t + \frac{c_2^t}{1+r_{t+1}} = w_t$\displaystyle c_1^t + \frac{c_2^t}{1+r_{t+1}} = w_t

均衡第一阶段消费是一个函数：

> $c^t_ 1 = c_1(w_t, r_{t+1})$c^t_ 1 = c_1(w_t, r_{t+1})

年轻人的储蓄：

> $s_t = w_t − c^t_ 1 = s(w_t, r_{t+1})$s_t = w_t − c^t_ 1 = s(w_t, r_{t+1})

*   $t$t 期的储蓄是 $t + 1$t + 1 期的资本存量

> $s_t = k_{t+1}$s_t = k_{t+1}

在 $t + 1$t + 1 时期，均衡利率满足 $r_{t+1} = f'(k_{t+1})$r_{t+1} = f'(k_{t+1}) ，因此储蓄的方程为：

> $s(w(k_t), r(k_{t+1}))=k_{t+1}$s(w(k_t), r(k_{t+1}))=k_{t+1}

如果上面的方程有唯一解，有

> $k_{t+1} = g(k_t) =s(w(k_t), r (g(k_t)))$ k_{t+1} = g(k_t) =s(w(k_t), r (g(k_t)))

给定 $k_0$k_0 ，函数 $g$g 给出唯一路径 $\{k_t\}_{t=1}^\infty$\{k_t\}_{t=1}^\infty ，其中：

1.  生成价格序列 $\{w_t,r_t\}_{t=1}^\infty$\{w_t,r_t\}_{t=1}^\infty
2.  确定给定价格的企业对资本 $k_t$k_t 的需求
3.  确定资本的供给，根据代理人的效用最大化条件
4.  然后特征化一个均衡

**例**：令效用函数为：

> $u(c^t_ 1, c^t_ 2) = (1-\theta) \log c_1^t + \theta \log c_2^t$u(c^t_ 1, c^t_ 2) = (1-\theta) \log c_1^t + \theta \log c_2^t

等价于：

> $u(c^t_ 1, c^t_ 2) = \log c_1^t + \rho \log c_2^t$u(c^t_ 1, c^t_ 2) = \log c_1^t + \rho \log c_2^t

其中折现因子 $\rho$\rho 衡量持久性（longevity）：

> $\displaystyle \rho = \frac{\theta}{1-\theta}$\displaystyle \rho = \frac{\theta}{1-\theta}

对于生产函数，令

> $Y_t = K^α _t L^{1−α} _t$Y_t = K^α _t L^{1−α} _t

有：

> $y_t = f(k_t) = k^α$y_t = f(k_t) = k^α

由家庭的优化，函数 $k_{t+1}=g(k_t)$ k_{t+1}=g(k_t) 决定于：

> $s (w(k_t), r (g(k_t))) = g(k_t) = k_{t+1}$s (w(k_t), r (g(k_t))) = g(k_t) = k_{t+1}

得到：

> $θw(k_t) = θ [(1 − α)k^α_ t ] = k_{t+1}$θw(k_t) = θ [(1 − α)k^α_ t ] = k_{t+1}

稳态：

> $k_{t+1} = k_t = k^∗$k_{t+1} = k_t = k^∗  
> $k^∗ = θ(1 − α)(k^∗)^α$k^∗ = θ(1 − α)(k^∗)^α  
> $k^∗ = [θ(1 − α)]^{1/(1−α)}$k^∗ = [θ(1 − α)]^{1/(1−α)}

稳态利率：

> $\displaystyle r^* = \frac{\alpha}{1-\alpha } \frac{1}{\theta}$\displaystyle r^* = \frac{\alpha}{1-\alpha } \frac{1}{\theta}

*   $θ$θ 越高，代理人对退休消费的偏好越大

*   年轻人储蓄更多，因此资本存量更高，利率更低

*   $α$α 越大，资本占比越大

*   资本收入全部被老年人消费
*   年轻人的收入更低，他们存钱，因此储蓄更低
*   资本存量减少，利率提高

### 动态与稳定

对于任意 $k_0$k_0 ，例子中的 $k_t$k_t 收敛于稳态，该稳态是**全局稳定的**（globally stable）

定义 $k$k 和 $k^*$k^* 之间的距离， $\tilde k_t:=k_t-k^*$\tilde k_t:=k_t-k^* 。由泰勒近似：（ $k$k 的动态图见附录）

*   $\tilde k_{t+1}=\lambda \tilde k_{t}$\tilde k_{t+1}=\lambda \tilde k_{t}
*   $\lambda = g'(k^*)$\lambda = g'(k^*)

稳态是**局部稳定的**（locally stable） $\Leftrightarrow$ \Leftrightarrow 存在 $ε$ε ，使得 $|\tilde k_0| < \varepsilon$ |\tilde k_0| < \varepsilon ，

> $\displaystyle \lim_{t \rightarrow \infty} \tilde k_t = 0$\displaystyle \lim_{t \rightarrow \infty} \tilde k_t = 0

稳态是局部稳定的，当且仅当 $|\lambda| < 1$|\lambda| < 1 。低 $|λ|$|λ| 意味着快速收敛。

如果 $λ$λ 为负，则在稳态周围存在**蛛网动态**（cobweb dynamics）。

2. 有增长 OLG
----------

### 增长

现在允许人口（和劳动力供给）正增长： $L_{t+1 }= (1 + n)L_t$L_{t+1}= (1 + n)L_t 。

单位劳动力资本的动态变为：

> $\displaystyle k_{t+1} = \frac{s(w(k_t),r(k_{t+1}))}{1+n}$\displaystyle k_{t+1} = \frac{s(w(k_t),r(k_{t+1}))}{1+n}

*   资本是由老年人在年轻时储蓄的，而现在的年轻人更多
*   之前的曲线 $k_{t+1} = g(k_t)$k_{t+1} = g(k_t) 向下平移

根据先前特定的效用和生产函数：

> $\displaystyle r^* = \frac{\alpha(1+n)}{1-\alpha } \frac{1}{\theta}$\displaystyle r^* = \frac{\alpha(1+n)}{1-\alpha } \frac{1}{\theta}

*   $n$n 越大，单位劳动资本越低，因此稳态利率越高

### 消费的黄金水平

*   在任何人口正增长的模型中，为了使资本劳动比率保持不变，需要第 $t$t 期的总储蓄为 $nK_t$nK_t

*   在拉姆齐模型中，盈亏平衡投资（总水平）为 $(n + g + δ)K_t$(n + g + δ)K_t ，附加项来自生产率增长和贬值

*   人均消费： $F(k_t, 1)/L_t − nK_t/L_t = f(k_t) − nk_t$F(k_t, 1)/L_t − nK_t/L_t = f(k_t) − nk_t

*   使人均消费最大化的资本劳动比率 $\tilde k$\tilde k 为 $f ’ (\tilde k) = n$f ’ (\tilde k) = n （资本存量的黄金法则）

*   稳态资本存量可能高于或低于黄金法则水平

*   也就是说可能 $r ^∗ > n$r ^∗ > n 也可能 $r ^∗ < n$r ^∗ < n
*   这是因为储蓄是由第二阶段的个人消费驱动的

### 动态无效率

*   如果 $f' (k ^*) < n$f' (k ^*) < n ，则均衡不是帕累托最优
*   从 $k_0 = k^ *$k_0 = k^ * 开始，社会计划者可以做以下的事情：

*   ① 在 $t = 0$t = 0 ，减少储蓄使得 $k_1 =\tilde k$k_1 =\tilde k 而不是 $k_1 =k^*$k_1 =k^*
*   ② 从 $t = 1$t = 1 开始，保持储蓄计划，使所有 $t≥1$t≥1 时 $k_t =\tilde k$k_t =\tilde k

*   从 $t = 1$t = 1 开始，代理人的效用被严格提高，因为 $\tilde k$\tilde k 是最大化子（maximizer）
*   在 $t = 0$t = 0 ，消费甚至更高：

*   $f(k^∗) + (k^∗ − \tilde k) − n \tilde k > f( \tilde k) − \tilde n$f(k^∗) + (k^∗ − \tilde k) − n \tilde k > f( \tilde k) − \tilde n

*   上面的计划是帕累托改进

*   这被称为**动态无效率**（dynamic inefficiency）（图见附录）
*   这种无效率是由于模型的动态结构

### 转移方案

*   在分散经济中，在第二个时期分配资源用于消费的唯一途径是在第一个时期进行储蓄
*   但是计划者可以在一个时期内将资源分配给不同的一代

*   将商品从年轻人手中转移到老年人手中
*   当年轻人变老时，重复这个转移方案

*   上述分配在分散市场中是不可能的

*   年轻人不能与老年人签订跨期合同

*   萨缪尔森表明 [1](1)(#ref_1)，在分散经济中引入货币就足以实现有效配置

3. OLG 与生育
----------

本节主要参考焼田党，Yakita (2017)[2](2)(#ref_2) 的第四章内容。

1）模型设定
------

这里参考了 4.2 节。

### 孩子数量

考虑一个小型开放经济体，利率外生且恒定。每个人的生命都有两个阶段：

*   年轻：工作、养娃
*   年老：退休

第 $t$t 代代表性个体的终身效用为：

> $u_t = \log c^1_t + ε \log n_t + ρ \log c^2_{t +1}$u_t = \log c^1_t + ε \log n_t + ρ \log c^2_{t +1}

*   $c^1_t$c^1_t ：年轻时的消费
*   $c^2_{t +1}$c^2_{t +1} ：年老时的消费
*   $n_t$ n_t ：孩子的数量
*   $ε$ε ：有孩子的效用权重， $ε > 0$ε > 0
*   $ρ$ρ ：时间折现因子， $0 < ρ < 1$0 < ρ < 1

### 生育函数

抚养孩子要么耗费父母的时间，要么耗费托儿服务。根据 Apps and Rees (2004)[3](3)(#ref_3)：

> $n_t = θχ^σ_t z^{1−σ}_ t$n_t = θχ^σ_t z^{1−σ}_ t

*   $θ > 0$θ > 0
*   $χ_t$χ_t ：在市场上购买的托儿服务
*   $z_t$z_t ：父母养娃的时间
*   $σ$σ ：市场托儿服务在养娃中的贡献程度， $σ ∈ (0, 1)$σ ∈ (0, 1)

假设托儿服务是使用消费品生产的，把消费品的价格统一到 1。

### 预算约束

每个人在工作和在家养娃之间分配时间。

第一阶段预算约束：

> $(1 − τ )w(1 − z_t) = c^1_t + χ_t + s_t$(1 − τ)w(1 − z_t) = c^1_t + χ_t + s_t

*   $τ$τ ：社会保障的工资税率
*   $w$w ：税前工资率，由外生利率和生产函数决定
*   $s_t$s_t ：退休储蓄

第二阶段预算约束：

> $rs_t + β_{t+1} = c^2_{t +1}$rs_t + β_{t+1} = c^2_{t +1}

*   $r$r ：外生的总利率
*   $β_{t+1}$β_{t+1} ：社会保障福利

联立两个预算约束，得到生命周期预算约束：

> $\displaystyle (1 − τ )w(1 − z_t) = c^1_t + \frac{c^2_{t +1} − β_{t+1}}{r} + \chi_t$\displaystyle (1 − τ)w(1 − z_t) = c^1_t + \frac{c^2_{t +1} − β_{t+1}}{r} + \chi_t

*   个体在上述约束下选择 $c^1_t ,c^2_{t +1},n_t$ c^1_t ,c^2_{t +1},n_t 来最大化终身效用函数。
*   在此过程中还选择了 $z_t,χ_t$z_t,χ_t 来最小化抚养孩子的成本

### 政府

政府实行**现收现付**（PAYG，pay-as-you-go）的社会保障制度。对老年人的支付 $β$β 由劳动所得税以固定税率 $τ$τ 提供资金。假设政府在每个时期平衡预算，则其预算约束为：

> $τw(1 − z_{t+1})N_{t+1 }= β_{t+1}N_t$τw(1 − z_{t+1})N_{t+1 }= β_{t+1}N_t

*   $N_t$N_t ：在一段时期内工人（年轻人）的人口

*   $n_t = N_{t+1}/N_t$n_t = N_{t+1}/N_t

按每个工人计算，政府的预算约束可以写成：

> $τw(1 − z_{t+1})n_t = β_{t+1}$τw(1 − z_{t+1})n_t = β_{t+1}

2）无托儿服务均衡
---------

这里参考了 4.3.1 节。

### 一阶条件

接下来讨论无托儿服务（ $χ_t = 0$χ_t = 0 ）的均衡。父母必须用自己的时间来抚养孩子。

假设每个孩子的养育时间是一个常数：

*   $z_t = zn_t$z_t = zn_t
*   $n_t = z_t/z$n_t = z_t/z

第 $t$t 代个体的拉格朗日函数为：

> $\displaystyle \mathcal L = \log c^1_t + ε \log n_t + ρ \log c^2_{t +1} + λ_t \left[ \displaystyle (1 − τ )w(1 − z_t) - c^1_t - \frac{c^2_{t +1} − β_{t+1}}{r} - \chi_t \right]$\displaystyle \mathcal L = \log c^1_t + ε \log n_t + ρ \log c^2_{t +1} + λ_t \left[ \displaystyle (1 − τ )w(1 − z_t) - c^1_t - \frac{c^2_{t +1} − β_{t+1}}{r} - \chi_t \right]

一阶条件为：

> $\displaystyle \frac{\partial \mathcal L}{\partial c_t^1}=0 \Rightarrow \frac{1}{c_t^1} = \lambda_t$\displaystyle \frac{\partial \mathcal L}{\partial c_t^1}=0 \Rightarrow \frac{1}{c_t^1} = \lambda_t  
> $\displaystyle \frac{\partial \mathcal L}{\partial c_{t+1}^2}=0 \Rightarrow \frac{ρ}{c_{t+1}^2} = \frac{\lambda_t}{r}$\displaystyle \frac{\partial \mathcal L}{\partial c_{t+1}^2}=0 \Rightarrow \frac{ρ}{c_{t+1}^2} = \frac{\lambda_t}{r}  
> $\displaystyle \frac{\partial \mathcal L}{\partial n_t}=0 \Rightarrow \frac{ε}{n_t} = λ_tw(1 − τ )z$\displaystyle \frac{\partial \mathcal L}{\partial n_t}=0 \Rightarrow \frac{ε}{n_t} = λ_tw(1 − τ )z

### 动力系统

联立得最优储蓄和生育计划是：

> $\displaystyle s_t = \frac{ρ}{1 + ρ + ε} w(1 − τ ) − \frac{1 + ε}{1 + ρ + ε} \frac{β_{t+1}}{r}$\displaystyle s_t = \frac{ρ}{1 + ρ + ε} w(1 − τ ) − \frac{1 + ε}{1 + ρ + ε} \frac{β_{t+1}}{r}  
> $\displaystyle n_t = \frac{ε}{z(1 + ρ + ε)} \left[ 1 +\frac{β_{t+1}}{rw(1 − τ )} \right]$\displaystyle n_t = \frac{ε}{z(1 + ρ + ε)} \left[ 1 +\frac{β_{t+1}}{rw(1 − τ )} \right]

结合政府的预算约束、假设和最优生育计划，得到：

> $\displaystyle n_t = \frac{ε}{z(1 + ρ + ε)} \left[ 1 +\frac{τ (1 − zn_{t+1})n_t}{r(1 − τ )} \right]$\displaystyle n_t = \frac{ε}{z(1 + ρ + ε)} \left[ 1 +\frac{τ (1 − zn_{t+1})n_t}{r(1 − τ )} \right]  
> $\displaystyle z_t = \frac{ε}{1 + ρ + ε} \left[ 1 +\frac{τ (1 − z_{t+1})z_t}{r(1 − τ )z} \right]$\displaystyle z_t = \frac{ε}{1 + ρ + ε} \left[ 1 +\frac{τ (1 − z_{t+1})z_t}{r(1 − τ )z} \right]

### 动力系统稳定性

以上给出了系统的动力学。

*   一代子女的数量取决于社会保障福利
*   反过来，社会保障福利取决于他们子女的数量

动力系统的稳定条件为：

> $\displaystyle -1<\frac{dn_{t+1}}{dn_t}<1$\displaystyle -1<\frac{dn_{t+1}}{dn_t}<1

有：

> $\displaystyle \frac{dn_{t+1}}{dn_t} = - \frac{\displaystyle \frac{z(1 + ρ + ε)}{ε}\bigg/ \frac{\tau}{r(1 − τ )}− (1 − zn)}{zn}$\displaystyle \frac{dn_{t+1}}{dn_t} = - \frac{\displaystyle \frac{z(1 + ρ + ε)}{ε}\bigg/ \frac{\tau}{r(1 − τ )}− (1 − zn)}{zn}

*   $n$n ：稳态生育率
*   描述了系统在稳定状态下对初始偏差的响应

联立得：

> $\displaystyle 1 − 2zn < \frac{z(1 + ρ + ε)}{ε } \frac{r(1 − τ )}{τ}< 1$\displaystyle 1 − 2zn <\frac{z(1 + ρ + ε)}{ε } \frac{r(1 − τ )}{τ}< 1

稳态生育率满足：

> $\displaystyle Ψ(n) := \frac{τz}{r(1 − τ )} n^2 + \left[ \frac{z(1 + ρ + ε)}{ε } - \frac{τ}{r(1 − τ )} \right]n − 1 = 0$\displaystyle Ψ(n) := \frac{τz}{r(1 − τ )} n^2 + \left[ \frac{z(1 + ρ + ε)}{ε } - \frac{τ}{r(1 − τ )} \right]n − 1 = 0

*   $Ψ(0) = −1 < 0$Ψ(0) = −1 < 0

另一方面：

> $\displaystyle Ψ(1) = \frac{z \{[τ + r(1 − τ )] ε + r(1 − τ )(1 + ρ)\} − ε [τ + r(1 − τ )]}{r(1 − τ )ε}$\displaystyle Ψ(1) = \frac{z \{[τ + r(1 − τ )] ε + r(1 − τ )(1 + ρ)\} − ε [τ + r(1 − τ )]}{r(1 − τ )ε}

如果以下参数约束成立，则 $Ψ(1) < 0$Ψ(1) < 0 ：

> $\displaystyle ε > \frac{z}{1 − z} \frac{1 + ρ}{[τ/(1 − τ )] + r}$\displaystyle ε > \frac{z}{1 − z} \frac{1 + ρ}{[τ/(1 − τ )] + r}

*   抚养成本 $z$z 足够小，或
*   生育偏好 $ε$ε 足够高

若上述条件成立，则 $Ψ(0) < 0$Ψ(0) < 0 且 $Ψ(1) < 0$Ψ(1) < 0 ，故 1">$n > 1$n > 1 ，有内生的人口增长。

### 工资税率变化

由稳态：

> $\displaystyle \frac{dn}{d\tau} = \frac{\displaystyle n(1 − zn) \big/ [r(1 − τ )^2]}{\displaystyle \frac{z(1 + ρ + ε)}{ε}-(1 − 2zn) \frac{τ}{r(1 − τ )}} > 0$\displaystyle \frac{dn}{d\tau} = \frac{\displaystyle n(1 − zn) \big/ [r(1 − τ )^2]}{\displaystyle \frac{z(1 + ρ + ε)}{ε}-(1 − 2zn) \frac{τ}{r(1 − τ )}} > 0

其中，分母大于 0。

增税降低了税后工资率（养娃的机会成本）。因此，父母增加孩子的数量。子女数量的增加往往会使每位退休人员的社会保障福利增加，因为在这种情况下工资率是外生的。每个子女的抚养时间是不变的，意味着增加了父母养娃的时间。

### 福利分析

终身福利的稳态为：

> $\displaystyle u =\log c_1 + ε \log n + ρ \log c_2 \\\ \ \displaystyle =(1 + ρ) \log \left[ (1 − τ )w + \frac{w\tau}{r} (1 − zn)n \right] + ε \log n + Γ$\displaystyle u =\log c_1 + ε \log n + ρ \log c_2 \\\ \ \displaystyle =(1 + ρ) \log \left[ (1 − τ )w + \frac{w\tau}{r} (1 − zn)n \right] + ε \log n + Γ

*   $Γ$Γ ：常数项的和

提高工资税对福利的影响是不确定的：

> $\displaystyle \frac{du}{dτ} = \frac{1 + ρ}{(1 − τ )r + τ (1 − zn)n} \left\{ [(1 − zn)n − r] + τ (1 − 2zn) \frac{dn}{d\tau} \right\} + \frac{ε}{n} \frac{dn}{d\tau}$\displaystyle \frac{du}{dτ} = \frac{1 + ρ}{(1 − τ )r + τ (1 − zn)n} \left\{ [(1 − zn)n − r] + τ (1 − 2zn) \frac{dn}{d\tau} \right\} + \frac{ε}{n} \frac{dn}{d\tau}

虽然增加了子女数量，但也增加了养育子女的时间，减少了消费。

3）有托儿服务均衡
---------

这里参考了 4.3.2 节。

### 一阶条件

第 $t$t 代个体的拉格朗日函数为：（与之前相比，代入了 $n_t = θχ^σ_t z^{1−σ}_ t$n_t = θχ^σ_t z^{1−σ}_ t ）

> $\displaystyle \mathcal L = \log c^1_t + ε \log ( θχ^σ_t z^{1−σ}_ t) + ρ \log c^2_{t +1} + λ_t \left[ \displaystyle (1 − τ )w(1 − z_t) - c^1_t - \frac{c^2_{t +1} − β_{t+1}}{r} - \chi_t \right]$\displaystyle \mathcal L = \log c^1_t + ε \log (θχ^σ_t z^{1−σ}_ t) + ρ \log c^2_{t +1} + λ_t \left[ \displaystyle (1 − τ )w(1 − z_t) - c^1_t - \frac{c^2_{t +1} − β_{t+1}}{r} - \chi_t \right]

一阶条件为：

> $\displaystyle \frac{\partial \mathcal L}{\partial c_t^1}=0 \Rightarrow \frac{1}{c_t^1} = \lambda_t$\displaystyle \frac{\partial \mathcal L}{\partial c_t^1}=0 \Rightarrow \frac{1}{c_t^1} = \lambda_t （和之前一样）  
> $\displaystyle \frac{\partial \mathcal L}{\partial c_{t+1}^2}=0 \Rightarrow \frac{ρ}{c_{t+1}^2} = \frac{\lambda_t}{r}$\displaystyle \frac{\partial \mathcal L}{\partial c_{t+1}^2}=0 \Rightarrow \frac{ρ}{c_{t+1}^2} = \frac{\lambda_t}{r} （和之前一样）  
> $\displaystyle \frac{\partial \mathcal L}{\partial χ_t}=0 \Rightarrow \frac{εσ}{χ_t} = λ_t$\displaystyle \frac{\partial \mathcal L}{\partial χ_t}=0 \Rightarrow \frac{εσ}{χ_t} = λ_t  
> $\displaystyle \frac{\partial \mathcal L}{\partial z_t}=0 \Rightarrow \frac{ε(1 -σ)}{z_t} = w(1-\tau)λ_t$\displaystyle \frac{\partial \mathcal L}{\partial z_t}=0 \Rightarrow \frac{ε(1 -σ)}{z_t} = w(1-\tau)λ_t

### 动力系统

联立得最优储蓄、托儿服务时间和养娃时间为：

> $\displaystyle s_t = \frac{ρ}{1 + ρ + ε} w(1 − τ ) − \frac{1 + ε}{1 + ρ + ε} \frac{β_{t+1}}{r}$\displaystyle s_t = \frac{ρ}{1 + ρ + ε} w(1 − τ ) − \frac{1 + ε}{1 + ρ + ε} \frac{β_{t+1}}{r}（和之前一样）  
> $\displaystyle \chi_t = \frac{εσ}{1 + ρ + ε} \left[ (1-\tau)w +\frac{β_{t+1}}{r} \right]$\displaystyle \chi_t = \frac{εσ}{1 + ρ + ε} \left[ (1-\tau)w +\frac{β_{t+1}}{r} \right]  
> $\displaystyle z_t = \frac{ε(1-σ)}{w(1-\tau)(1 + ρ + ε)} \left[ (1-\tau)w +\frac{β_{t+1}}{r} \right]$\displaystyle z_t = \frac{ε(1-σ)}{w(1-\tau)(1 + ρ + ε)} \left[ (1-\tau)w +\frac{β_{t+1}}{r} \right]

代入 $n_t = θχ^σ_t z^{1−σ}_ t$n_t = θχ^σ_t z^{1−σ}_ t 得到最优生育计划：

> $\displaystyle n_t = \frac{θεσ^σ(1 - σ)^{1-σ}}{(1 + ρ + ε)[w(1-\tau)]^{1-\sigma}} \left[ (1-\tau)w +\frac{β_{t+1}}{r} \right]$\displaystyle n_t = \frac{θεσ^σ(1 - σ)^{1-σ}}{(1 + ρ + ε)[w(1-\tau)]^{1-\sigma}} \left[ (1-\tau)w +\frac{β_{t+1}}{r} \right]

由于国内储蓄不影响要素价格，所以市场均衡由劳动力市场的均衡条件来定义。

将 $z_t$ z_t 和 $n_t$ n_t 的表达式联立得：

> $n_t = [(1-\tau)w]^{\sigma} \theta [\sigma / (1-\sigma)] ^\sigma z_t$n_t = [(1-\tau)w]^{\sigma} \theta [\sigma / (1-\sigma)] ^\sigma z_t

代回 $z_t$ z_t 的表达式得：

> $\displaystyle \frac{1+\rho+\varepsilon}{\varepsilon(1-\sigma)}= \frac{1}{z_t}+\frac{ (1-z_{t+1})\tau}{r(1-\tau)} [(1-\tau)w]^\sigma \theta [\sigma / (1-\sigma)] ^\sigma$\displaystyle \frac{1+\rho+\varepsilon}{\varepsilon(1-\sigma)}= \frac{1}{z_t}+\frac{ (1-z_{t+1})\tau}{r(1-\tau)} [(1-\tau)w]^\sigma \theta [\sigma / (1-\sigma)] ^\sigma

这个方程给出了系统的动力学。当 $σ= 0$σ= 0 且 $θ = 1$θ = 1 时，就变为无托儿服务均衡下的方程：

> $\displaystyle z_t = \frac{ε}{1 + ρ + ε} \left[ 1 +\frac{τ (1 − z_{t+1})z_t}{r(1 − τ )z} \right]$\displaystyle z_t = \frac{ε}{1 + ρ + ε} \left[ 1 +\frac{τ (1 − z_{t+1})z_t}{r(1 − τ )z} \right]

### 动力系统稳定性

和前面的例子一样，假设存在一个稳态 $z$z ，得到：

> $\displaystyle \frac{dz_{t+1}}{dz_t} = - \frac{1/z^2}{(w/r)\tau\theta [w(1-\tau)^{\sigma-1}[\sigma/(1-\sigma)]^\sigma} < 0$\displaystyle \frac{dz_{t+1}}{dz_t} = - \frac{1/z^2}{(w/r)\tau\theta [w(1-\tau)^{\sigma-1}[\sigma/(1-\sigma)]^\sigma} < 0

稳定性条件为：

> $z^2 > \{ (w/r) \tau \theta [(1-\tau)w]^{\sigma-1} [\sigma / (1-\sigma )^\sigma ] \}^{-1}$z^2 > \{(w/r) \tau \theta [(1-\tau)w]^{\sigma-1} [\sigma / (1-\sigma )^\sigma ] \}^{-1}

假设满足稳定条件，则有稳态育儿时间 $z$z 满足：（去掉 $z$z 的全部脚标）

> $\displaystyle \frac{1+\rho+\varepsilon}{\varepsilon(1-\sigma)}= \frac{1}{z}+\frac{ (1-z)\tau}{r(1-\tau)} [(1-\tau)w]^\sigma \theta [\sigma / (1-\sigma)] ^\sigma$\displaystyle \frac{1+\rho+\varepsilon}{\varepsilon(1-\sigma)}= \frac{1}{z}+\frac{ (1-z)\tau}{r(1-\tau)} [(1-\tau)w]^\sigma \theta [\sigma / (1-\sigma)] ^\sigma

### 工资税率变化

现在研究增税对父母养娃时间、市场托儿服务购买和生育率的影响。

上式对 $\tau$\tau 求导得：（对两端同时求导、采用链式法则后，移项化简）

> $\displaystyle \frac{dz}{d\tau} = \frac{(1-z)w^2 \theta [\sigma / (1-\sigma)]^\sigma [(1-\tau)w]^{\sigma-2}(1-\sigma \tau)}{(r/z^2)+w\tau \theta [(1-\tau )w]^{\sigma-1} [\sigma / (1-\sigma)]^\sigma}>0$\displaystyle \frac{dz}{d\tau} = \frac{(1-z)w^2 \theta [\sigma / (1-\sigma)]^\sigma [(1-\tau)w]^{\sigma-2}(1-\sigma \tau)}{(r/z^2)+w\tau \theta [(1-\tau )w]^{\sigma-1} [\sigma / (1-\sigma)]^\sigma}>0

也就是说，工资税率的提高增加了父母抚养孩子的时间。该无托儿服务均衡类似，解读类似。

由于满足：

> $χ =[σ/(1 - σ)](1%20-%20σ))))wz$χ =[σ/(1 - σ)](1%20-%20σ))))wz

由 $\chi_t,z_t$\chi_t,z_t 的表达式，得：

> $\displaystyle \frac{d \chi}{d t} = \frac{\sigma}{1-\sigma }wz \frac{1-\tau}{\tau} \left( \frac{\tau }{z} \frac{dz}{d\tau}-\frac{\tau}{1-\tau} \right)$ \displaystyle \frac{d \chi}{d t} = \frac{\sigma}{1-\sigma }wz \frac{1-\tau}{\tau} \left( \frac{\tau }{z} \frac{dz}{d\tau}-\frac{\tau}{1-\tau} \right)

其中， $\displaystyle \frac{d \chi}{d t}$\displaystyle \frac{d \chi}{d t} 的正负由 $\displaystyle \left( \frac{\tau }{z} \frac{dz}{d\tau}-\frac{\tau}{1-\tau} \right)$ \displaystyle \left(\frac{\tau}{z} \frac{dz}{d\tau}-\frac{\tau}{1-\tau} \right) 决定。

定义：

> $\displaystyle η\equiv \frac{\tau }{z} \frac{dz}{d\tau} > 0$ \displaystyle η\equiv \frac{\tau}{z} \frac{dz}{d\tau} > 0

如果抚养孩子的时间相对于税率的弹性足够大，$\eta > \tau / (1-\tau)$\eta > \tau / (1-\tau) ，较高的税率会导致更多的市场托儿服务的购买，反之亦然。如果家长们对税金上调反应强烈，投入更多的时间，市场上的托儿服务就会比投入的时间少。这将促使家长更多地增加对市场托儿服务的购买。

相反，如果父母养育子女时间的税收弹性足够小，则加税对市场托儿服务购买的负面影响将大于父母时间投入的积极投入效应，因为加税降低了相对于市场托儿服务价格（固定为 1）的税后工资率。即使增税增加了父母抚养孩子的时间，但减少了市场育儿服务的购买，也有可能增加子女数量。

由 $z_t$ z_t 和 $n_t$ n_t 的联立式：

> $\displaystyle \frac{d n}{d \tau} = \theta [\sigma / (1-\sigma)]^\sigma (z/\tau ) [ (1-\tau)w]^\sigma \left( \eta - \sigma \frac{\tau}{1-\tau} \right)$ \displaystyle \frac{d n}{d \tau} = \theta [\sigma / (1-\sigma)]^\sigma (z/\tau ) [ (1-\tau)w]^\sigma \left( \eta - \sigma \frac{\tau}{1-\tau} \right)

其中， $\displaystyle\frac{d n}{d \tau}$ \displaystyle\frac{d n}{d \tau} 的正负由 $\displaystyle \left( \eta - \sigma \frac{\tau}{1-\tau} \right)$ \displaystyle \left(\eta - \sigma \frac{\tau}{1-\tau} \right) 决定。

也就是说，即使父母减少了市场托儿服务的购买，如果育儿时间相对于税率的弹性 $η$η 相对于生育率的市场托儿弹性 $σ$σ 足够大，工资税的增加也会导致更高的生育率。另一方面，如果因增税而导致的市场保育服务购买量减少的幅度足够大，生育率就会下降。

将三种情况总结如下：

*   情况 1：若 $\eta > \tau / (1-\tau)$\eta > \tau / (1-\tau) ，则 $dχ/dτ > 0$dχ/dτ > 0 且 $dn/dτ > 0$dn/dτ > 0
*   情况 2：若 $τ/(1 - τ)> η > στ/(1 - τ)$τ/(1 - τ)> η > στ/(1 - τ) ，则 $dχ/dτ < 0$dχ/dτ < 0 且 $dn/dτ > 0$dn/dτ > 0
*   情况 3：若 $στ/(1 - τ) < η$ στ/(1 - τ) < η ，则 $dχ/dτ < 0$dχ/dτ < 0 且 $dn/dτ < 0$dn/dτ < 0

提高工资税是否会提高生育率取决于父母抚养子女时间的税收弹性。

如果弹性足够大（即情况 1），其结果与没有市场化托儿服务的情况类似，即使税率较高，生育率也会较高。也就是说，抚养孩子的机会成本较低，诱使个人用 “孩子” 代替商品消费。

当增税导致育儿时间大幅增加时，即使税后工资率较低，也会促使父母增加购买市场育儿服务。因此，孩子的数量和购买托儿服务在这种情况下增加。如果增税导致的育儿时间增加不是很大（即情况 2），父母可能会减少购买托儿服务，而替代效应导致生育率上升。但是，如果父母抚养子女的时间弹性足够小，则增税会导致生育率下降。工资收入的减少反而减少了对孩子的需求。

最后一种情况（即情况 3）是一个显著的结果，在家庭以外的儿童保育服务是可用的。也就是说，与 ${dz}/{dt}$ {dz}/{dt} 的表达式一起，工资税的削减同时提高了生育率和市场劳动力供给，与父母购买更多的市场托儿服务有关。在这种情况下，减税一方面减少了在家养育孩子的时间，另一方面增加了在家养育孩子的机会成本，增加了市场上儿童保育的购买。

### 福利分析

在市场上提供托儿服务的情况下，可以获得以下福利效应。稳态终身效用为：

> $u= \log c^1 + \varepsilon \log n + \rho \log c^2 \\\ \ = (1+\sigma \varepsilon +\rho ) \log (1-\tau) + (1+\rho+\varepsilon) \log z + \Gamma'$u= \log c^1 + \varepsilon \log n + \rho \log c^2 \\\ \ = (1+\sigma \varepsilon +\rho) \log (1-\tau) + (1+\rho+\varepsilon) \log z + \Gamma'

*   $\Gamma'$\Gamma' ：有市场托儿服务时的常数项之和

得到：

> $\displaystyle \frac{d u}{d \tau} = \frac{1 +ε + ρ}{\tau} \left( \eta - \frac{\tau}{1-\tau} \frac{1+\rho +\sigma\varepsilon}{1+\rho +\varepsilon}\right)$\displaystyle \frac{d u}{d \tau} = \frac{1 +ε + ρ}{\tau} \left( \eta - \frac{\tau}{1-\tau} \frac{1+\rho +\sigma\varepsilon}{1+\rho +\varepsilon}\right)

其中， $\displaystyle \frac{d u}{d \tau}$ \displaystyle \frac{d u}{d \tau} 的正负由 $\displaystyle\left( \eta - \frac{\tau}{1-\tau} \frac{1+\rho +\sigma\varepsilon}{1+\rho +\varepsilon}\right)$\displaystyle\left(\eta - \frac{\tau}{1-\tau} \frac{1+\rho +\sigma\varepsilon}{1+\rho +\varepsilon}\right) 决定。

福利效应不能先验地确定。然而，

*   如果养育子女时间的税收弹性足够大，增税会增加稳态终身效用
*   如果养育子女时间的税收弹性足够小，减税将增加稳态终身效用

上式暗示存在一个最优的工资税率，尽管不能明确地推导出它，因为 $η$η 也可以是 $z,τ$z,τ 的函数。应该指出的是，工资税率变化对福利的影响并不取决于动态资源配置的效率。

### 模型比较

有市场托儿服务的模型和没有市场托儿服务的模型中一样，稳态下抚养孩子的时间在税率上是增加的。如果稳态的生育率随税率而下降，则必然是稳态的儿童服务市场 $\chi^*$\chi^* 对税率的反应是不确定的。这是因为：

*   一方面，较高的育儿时间增加了市场托儿服务的边际产品，因此其需求更高；
*   另一方面，与市场上固定为 1 的托儿服务相比，上调税金会降低工资率，这导致市场对托儿服务的需求降低。

$dn^* / d \tau$dn^* / d \tau 的符号取决于这两种相互冲突的影响中哪一种占主导地位。

练习题
---

### 有增长 OLG

**Q-1-F-2-2.** **生产率增长的世代交叠模型**。

（1）考虑具有世代交叠环境的经济。每个代理人生活两个时期，在第一个时期无弹性地提供 1 单位劳动，劳动收入为 $W_t$W_t 。出生在 $t$t 时期的代理人年轻时消费 $C_t^1$C_t^1 ，储蓄 $S_t = W_t - C_t^1$S_t = W_t - C_t^1 ，年老时消费 $C_t^2 = R_{t+1}$C_t^2 = R_{t+1} ，其中 $R_{t+1}$R_{t+1} 表示储蓄的总收益。第 $t$t 代代理人选择 $C_t^1$C_t^1 和 $C_t^2$C_t^2 来最大化效用函数：

> $\ln C_t^ 1 + \beta \ln C_t^2$ \ln C_t^ 1 + \beta \ln C_t^2

问：**写出具有代表性的第 $t$t 代代理人的优化问题，并推导出最优性条件，包括欧拉方程和储蓄方程。储蓄 $S_t$S_t 如何取决于利率 $R_{t+1}$R_{t+1} ？提供一个直观的解释**。

**解**：第 $t$t 代代理人的优化问题为：

> $\displaystyle \max _{C_t^1,C_t^2} \ln C_t^1 + \beta \ln C_t^2$\displaystyle \max _{C_t^1,C_t^2} \ln C_t^1 + \beta \ln C_t^2

约束为：

> $\displaystyle C_t^1 + \frac{C_t^2}{R_{t+1}} = W_t$\displaystyle C_t^1 + \frac{C_t^2}{R_{t+1}} = W_t

拉格朗日函数为：

> $\mathcal L = \ln C_t^1 + \beta \ln C_t^2 - \lambda \left( \displaystyle C_t^1 + \frac{C_t^2}{R_{t+1}} - W_t \right)$\mathcal L = \ln C_t^1 + \beta \ln C_t^2 - \lambda \left(\displaystyle C_t^1 + \frac{C_t^2}{R_{t+1}} - W_t \right)

对 $C_t^1$ C_t^1 和 $C_t^2$ C_t^2 的一阶条件分别为：

> $\displaystyle \frac{1}{C_t^1}=\lambda$\displaystyle \frac{1}{C_t^1}=\lambda  
> $\displaystyle \frac{\beta}{C_t^2}=\frac{\lambda}{R_{t+1}}$\displaystyle \frac{\beta}{C_t^2}=\frac{\lambda}{R_{t+1}}

联立得欧拉方程：

> $C_t^2 = \beta R_{t+1} C_t^1$C_t^2 = \beta R_{t+1} C_t^1

将欧拉方程代入预算约束，结合 $S_t$S_t 的表达式，得到储蓄规则：

> $\displaystyle S_t = \frac{\beta}{1+\beta}W_t$\displaystyle S_t = \frac{\beta}{1+\beta}W_t

因此，储蓄与利率 $R_{t+1}$R_{t+1} 无关。这是因为替代效应和收入效应完全抵消了对数效用函数。

（2）考虑生产部门。根据道格拉斯生产函数，企业在竞争市场中使用劳动力 $L_t$L_t 和资本 $K_t$K_t 来生产消费品：

> $Y_t = K_ t ^\alpha (A_tL_t)^{1-\alpha} , \alpha \in (0, 1)$Y_t = K_ t ^\alpha (A_tL_t)^{1-\alpha} , \alpha \in (0, 1)

*   $A_t$A_t ：劳动增强生产力。

问：**写出代表性企业的优化问题，并推导出最优性条件。提供直观的解释**。

**解**：企业利润最大化的问题是：

> $\displaystyle \max_{K_t,L_t} K_t^\alpha (A_tL_t)^{1-\alpha }-W_tL_t − R_tK_t$\displaystyle \max_{K_t,L_t} K_t^\alpha (A_tL_t)^{1-\alpha }-W_tL_t − R_tK_t

一阶条件分别得到：

*   $W_t =(1 − \alpha )K^\alpha _t A^{1−\alpha }_ t L^{−\alpha }_t$W_t =(1 − \alpha)K^\alpha _t A^{1−\alpha }_ t L^{−\alpha }_t
*   $R_t = \alpha K_t ^{\alpha - 1} (A_tL_t)^{1-\alpha }$R_t = \alpha K_t ^{\alpha - 1} (A_tL_t)^{1-\alpha }

对这些一阶条件的解释是，代表企业选择生产要素的数量，使其边际产量等于其各自的成本。

（3）设资本完全折旧，也就是 $K_{t+1} = L_tS_t$K_{t+1} = L_tS_t 。人口和生产力以恒定的速度增长。

*   $L_{t+1} =(1 + n)L_t$L_{t+1} =(1 + n)L_t
*   $A_{t+1} =(1 + g)A_t$A_{t+1} =(1 + g)A_t

$L_t$L_t 是在 $t$t 期出生的提供劳动力的个体的总数，所以 $t$t 期的老年人口是 $L_{t - 1}$L_{t - 1} 。

问：**设每有效劳动单位的资本为 $k_t = K_t / (A_tL_t)$k_t = K_t / (A_tL_t) ，求出其稳态值 $k_{ss}$k_{ss} 。 $k_{ss}$k_{ss} 会如何变化，以应对 $\beta$\beta 增加？提供直观的解释**。

**解**：联立：

> $K_{t+1} = L_tS_t$K_{t+1} = L_tS_t ， $\displaystyle \displaystyle S_t = \frac{\beta}{1+\beta}W_t$\displaystyle \displaystyle S_t = \frac{\beta}{1+\beta}W_t ， $W_t =(1 − \alpha )K^\alpha _t A^{1−\alpha }_ t L^{−\alpha }_t$W_t =(1 − \alpha)K^\alpha _t A^{1−\alpha }_ t L^{−\alpha }_t

得到：

> $\displaystyle K_{t+1} = \frac{\beta (1-\alpha )}{1+\beta} K_t^\alpha A_t^{1-\alpha }L_t ^{1-\alpha }$\displaystyle K_{t+1} = \frac{\beta (1-\alpha )}{1+\beta} K_t^\alpha A_t^{1-\alpha }L_t ^{1-\alpha }

两侧除以 $A_tL_t$A_tL_t 得到：

> $\displaystyle \frac{K_{t+1}}{A_tL_t} = \frac{K_{t+1}}{A_{t+1}L_{t+1}}· \frac{A_{t+1}L_{t+1}}{A_tL_t} = k_{t+1} (1+g) (1+n) \\\displaystyle \ \ \ \ \ \ \ \ \ \ = \frac{\beta (1-\alpha )}{1+\beta} \left( \frac{K_t}{A_tL_t} \right)^\alpha = \frac{\beta (1-\alpha )}{1+\beta} k_t^\alpha$\displaystyle \frac{K_{t+1}}{A_tL_t} = \frac{K_{t+1}}{A_{t+1}L_{t+1}}· \frac{A_{t+1}L_{t+1}}{A_tL_t} = k_{t+1} (1+g) (1+n) \\\displaystyle \ \ \ \ \ \ \ \ \ \ = \frac{\beta (1-\alpha )}{1+\beta} \left( \frac{K_t}{A_tL_t} \right)^\alpha = \frac{\beta (1-\alpha )}{1+\beta} k_t^\alpha

得到：

> $\displaystyle k_{t+1} = \frac{\beta (1-\alpha)}{(1+\beta)(1+g)(1+n)}k_t^\alpha$ \displaystyle k_{t+1} = \frac{\beta (1-\alpha)}{(1+\beta)(1+g)(1+n)}k_t^\alpha

令 $k^{ss} = k_{t+1} = k_t$k^{ss} = k_{t+1} = k_t ，稳态 $k$k 为：

> $\displaystyle k^{ss} = \left[ \frac{\beta (1-\alpha)}{(1+\beta)(1+g)(1+n)} \right]^{1/(1-\alpha)}$\displaystyle k^{ss} = \left[ \frac{\beta (1-\alpha)}{(1+\beta)(1+g)(1+n)} \right]^{1/(1-\alpha)}

其中 $1/(1-\alpha)>0$1/(1-\alpha)>0 ，有：

> $\displaystyle \frac{\partial}{\partial \beta} \left[ \frac{\beta (1-\alpha)}{(1+\beta)(1+g)(1+n)} \right] \\\displaystyle= \frac{(1-\alpha)(1+\beta)(1+g)(1+n)-\beta (1-\alpha)(1+g)(1+n)}{(1+\beta)^2 (1+g)^2 (1+n)^2} > 0$\displaystyle \frac{\partial}{\partial \beta} \left[ \frac{\beta (1-\alpha)}{(1+\beta)(1+g)(1+n)} \right] \\\displaystyle= \frac{(1-\alpha)(1+\beta)(1+g)(1+n)-\beta (1-\alpha)(1+g)(1+n)}{(1+\beta)^2 (1+g)^2 (1+n)^2} > 0

因此，较高的折现因子 $\beta$\beta 导致较高的稳态资本存量 $k^{ss}$k^{ss} 。直觉是， $\beta$\beta 表示代理人对第二阶段消费的偏好，因此随着 $C^2_t$C^2_t 变得更重要，代理人会通过储蓄来增加 $C^2_t$C^2_t ，从而导致更高的资本存量。

### 不确定性下的 OLG

**Q-2-F-2-3.** 考虑具有世代交叠的经济。每个周期都有固定数量的 $L$L 个个体出生并存活两个周期。年轻时，他们以工资 $w_t$w_t 提供 1 单位劳动，消费 $C_{y,t}$C_{y,t} ，储蓄 $S_t = w_t - C_{y,t}$S_t = w_t - C_{y,t} 。他们年老时消费 $C_{o,t+1} = R_{t+1}S_t$C_{o,t+1} = R_{t+1}S_t ，其中 $R_{t+1}$R_{t+1} 表示储蓄的总实际回报。 $R_t$R_t 和 $w_t$w_t 由具有柯布 - 道格拉斯生产函数 $Y_t = K_t^\alpha L_t ^{1-\alpha}$Y_t = K_t^\alpha L_t ^{1-\alpha} 的竞争企业决定。

代理人的效用函数为：

> $U = \ln C_{y,t }+ \ln C_{o,t+1}$U = \ln C_{y,t}+ \ln C_{o,t+1}

然而，贴现系数 $\beta$\beta 存在不确定性。代理人年老时健康的概率为 $p$p ，这种情况下的 $\beta =\beta_H \in (0,1)$\beta =\beta_H \in (0,1) ；年老时健康的概率为 $1-p$1-p ，这种情况下的 $\beta =\beta_S \in (0,\beta_H)$\beta =\beta_S \in (0,\beta_H) 。代理人是风险中性的，并最大化他们的期望效用。问：

（1）**写出代表性代理人的期望效用最大化问题和最优性条件**。

**解**：代表性代理人的期望效用最大化问题：

> $\displaystyle \max_{C_{y,t},C_{o,t+1}} \ \ln C_{y,t} + p \beta _H \ln C _{o,t+1} + (1-p) \beta_S \ln C_{o,t+1}$\displaystyle \max_{C_{y,t},C_{o,t+1}} \ \ln C_{y,t} + p \beta _H \ln C _{o,t+1} + (1-p) \beta_S \ln C_{o,t+1}

约束为：

*   $C_{y,t} + S_t = w_t$C_{y,t} + S_t = w_t
*   $C_{o,t+1} = R_{t+1} S_t$C_{o,t+1} = R_{t+1} S_t

联立得：

> $\displaystyle C_{y,t}+\frac{C_{o,t+1} }{R_{t+1}} =w_t$\displaystyle C_{y,t}+\frac{C_{o,t+1} }{R_{t+1}} =w_t

定义 $\bar \beta = p \beta_H + (1-p) \beta_S$\bar \beta = p \beta_H + (1-p) \beta_S ，代理人的拉格朗日函数为：

> $\displaystyle \mathcal L = \ln C_{y,t} + \bar \beta \ln C_{o,t+1} - \lambda \left( C_{y,t}+\frac{C_{o,t+1} }{R_{t+1}} -w_t\right)$\displaystyle \mathcal L = \ln C_{y,t} + \bar \beta \ln C_{o,t+1} - \lambda \left( C_{y,t}+\frac{C_{o,t+1} }{R_{t+1}} -w_t\right)

对 $C_{y,t}$ C_{y,t} 和 $C_{o,t+1}$C_{o,t+1} 的一阶条件分别为：

> $\displaystyle \frac{1}{C_{y,t} }=\lambda$\displaystyle \frac{1}{C_{y,t} }=\lambda  
> $\displaystyle \frac{\bar \beta}{C_{o,t+1}}=\frac{\lambda}{R_{t+1}}$\displaystyle \frac{\bar \beta}{C_{o,t+1}}=\frac{\lambda}{R_{t+1}}

联立得：

> $C_{o,t+1}=\bar \beta R_{t+1} C_{y,t}$C_{o,t+1}=\bar \beta R_{t+1} C_{y,t}

（2）**求每单位劳动的消费** $c_t$c_t **的稳态，其中：**

> $\displaystyle c_t = \frac{C_{y,t}+C_{o,t}}{L}$\displaystyle c_t = \frac{C_{y,t}+C_{o,t}}{L}

**解**：将（1）的结果代入预算约束，得到：

> $\displaystyle C_{y,t}+\bar \beta C_{y,t}=w_t$\displaystyle C_{y,t}+\bar \beta C_{y,t}=w_t

即：

> $\displaystyle C_{y,t} = \frac{w_t}{1+\bar \beta}$\displaystyle C_{y,t} = \frac{w_t}{1+\bar \beta}

代入得：

> $\displaystyle S_t = w_t - C_{y,t} = \frac{\bar \beta}{1+\bar \beta}w_t$ \displaystyle S_t = w_t - C_{y,t} = \frac{\bar \beta}{1+\bar \beta}w_t

其中 $w_t$w_t 通过企业的利润最大化问题得到：

> $\displaystyle \max K_t^\alpha L^{1-\alpha} - w_tL - R_tK_t$\displaystyle \max K_t^\alpha L^{1-\alpha} - w_tL - R_tK_t

由一阶条件可得：

> $\displaystyle w_t = (1-\alpha) K_t ^\alpha L^{-\alpha}$\displaystyle w_t = (1-\alpha) K_t ^\alpha L^{-\alpha}

因此：

> $\displaystyle S_t = \frac{\bar \beta (1-\alpha)}{1+\bar \beta} K_t ^\alpha L^{-\alpha}$\displaystyle S_t = \frac{\bar \beta (1-\alpha)}{1+\bar \beta} K_t ^\alpha L^{-\alpha}

代入得：

> $\displaystyle K_{t+1} = S_t L = \frac{\bar \beta (1-\alpha)}{1+\bar \beta} K_t ^\alpha L^{1-\alpha}$\displaystyle K_{t+1} = S_t L = \frac{\bar \beta (1-\alpha)}{1+\bar \beta} K_t ^\alpha L^{1-\alpha}

在稳态条件下有 $K^{ss} = K_t = K_{t+1}$K^{ss} = K_t = K_{t+1} ，得：

> $\displaystyle K^{ss} = \frac{\bar \beta (1-\alpha)}{1+\bar \beta} (K^{ss})^\alpha L^{1-\alpha}$\displaystyle K^{ss} = \frac{\bar \beta (1-\alpha)}{1+\bar \beta} (K^{ss})^\alpha L^{1-\alpha}  
> $\displaystyle k^{ss} = \frac{K^{ss}}{L} = \left[ \frac{(1-\alpha)\bar \beta}{1+\bar\beta} \right] ^{1/1(1-\alpha)}$\displaystyle k^{ss} = \frac{K^{ss}}{L} = \left[ \frac{(1-\alpha)\bar \beta}{1+\bar\beta} \right] ^{1/1(1-\alpha)}  
> $\displaystyle y^{ss} = \frac{Y^{ss}}{L} = (k^{ss})^\alpha = \left[ \frac{(1-\alpha)\bar \beta}{1+\bar\beta} \right] ^{\alpha/1(1-\alpha)}$\displaystyle y^{ss} = \frac{Y^{ss}}{L} = (k^{ss})^\alpha = \left[ \frac{(1-\alpha)\bar \beta}{1+\bar\beta} \right] ^{\alpha/1(1-\alpha)}

在稳态下，总产量为

> $Y^{ss} = r^{ss}K^{ss}+w^{ss}L$Y^{ss} = r^{ss}K^{ss}+w^{ss}L

其中 $K^{ss}$K^{ss} 为年轻人的储蓄，也是被老人消耗的本金。因此，在稳态下，产量等于总消费量。

（3）**每单位劳动的稳态消费如何依赖于 $p$p ？解释这个问题。**

**解**：显然 $c^{ss}$c^{ss} 随着 $\bar \beta/(1+\bar \beta)$\bar \beta/(1+\bar \beta) 严格递增，同样随着 $\bar \beta$\bar \beta 严格递增，随着 $p$p 严格递增。这是因为年老时身体健康的概率越高，预期折现系数越大，鼓励年轻的代理人储蓄更多，因此资本存量和单位劳动产出更高。

### R2.18

**Q-6-2.** **基本世代交叠模型**（教材习题 2.18）。

设 $L_t$L_t 个两期生活的个体在 $t$t 期出生，且

> $L_t=(1+n)L_{t-1}$L_t=(1+n)L_{t-1}

为简单起见，令效用是对数函数，且没有折现：

> $U_t = \ln (C_{1t})+\ln (C_{2t+1})$U_t = \ln (C_{1t})+\ln (C_{2t+1})

在时间 $t$t 出生的每个人都被赋予了 $A$A 单位的单一商品。这种商品既可以消费，也可以储存。在接下来的一段时间里，每单位的存储量产生 $\chi>0$\chi>0 单位的商品。

最后，假设在初始时期 0，除了 $L_0$L_0 个年轻人每个人都有 $A$A 单位的商品，还有 $[1/(1+n)]L_0$[1/(1+n)]L_0 个个体只在 0 时期存活。每个老年人都有 $Z$Z 数量的商品的禀赋；效用就是初始阶段的消费 $C_{20}$C_{20}。问：

（1）**描述这种经济的分散均衡**。提示：考虑到世代交叠的结构，任何一代的成员会与另一代的成员进行交易吗？

**解**：拉格朗日函数为：

> $\displaystyle \mathcal L = \ln (C_{1t}) + \ln(C_{2t+1})-\lambda \left( C_{1t}+\frac{C_{2t+1}}{\chi}-A\right)$\displaystyle \mathcal L = \ln (C_{1t}) + \ln(C_{2t+1})-\lambda \left( C_{1t}+\frac{C_{2t+1}}{\chi}-A\right)

对 $C_{1t}$C_{1t} 和 $C_{2t+1}$C_{2t+1} 的一阶条件分别为：

> $\displaystyle \frac{1}{C_{1t}} = \lambda$ \displaystyle \frac{1}{C_{1t}} = \lambda ； $\displaystyle \frac{1}{C_{2t+1}} = \frac \lambda \chi$\displaystyle \frac{1}{C_{2t+1}} = \frac \lambda \chi

联立得：

> $C_{2t+1} = \chi C_{1t}$C_{2t+1} = \chi C_{1t}

代入预算约束

> $\displaystyle C_{1t}+\frac{C_{2t+1}}{\chi}=A$\displaystyle C_{1t}+\frac{C_{2t+1}}{\chi}=A

中，得到：

> $\displaystyle C_{1t}=\frac{A}{2}$\displaystyle C_{1t}=\frac{A}{2} ； $\displaystyle C_{2t}=\frac{\chi A}{2}$\displaystyle C_{2t}=\frac{\chi A}{2}

（2）**考虑一个路径中，代理人的禀赋的比例** $f_t$f_t **随时间的推移恒定。这条路径上的人均总消费（即所有年轻人的消费加上所有老年人的消费）是** $f$f **的函数吗？若** $\chi < 1+n$\chi < 1+n **，** $f(0 \le f \le 1)$f(0 \le f \le 1) **取什么值使得人均消费最大化？**

**解**：总消费是指年轻人和老年人消费的商品的总和：

> $C_t=C_{1t}L_t + C_{2t}L_{t-1} = (1-f) AL_t + f \chi AL_{t-1}$C_t=C_{1t}L_t + C_{2t}L_{t-1} = (1-f) AL_t + f \chi AL_{t-1}

在世代交叠模型文献中，每劳动消费（consumption per labor）一般指代 $C_t/L_t$C_t/L_t ，即：

> $\displaystyle \frac{C_t}{L_t} = (1-f) A + \frac{f \chi A}{1+n}$\displaystyle \frac{C_t}{L_t} = (1-f) A + \frac{f \chi A}{1+n}

而不是：

> $\displaystyle \frac{C_t}{L_t+L_{t-1}} = \frac{C_t}{L_t +\displaystyle \frac{L_t}{1+n}} = \frac{1+n}{2+n} \frac{C_t}{L_t}$\displaystyle \frac{C_t}{L_t+L_{t-1}} = \frac{C_t}{L_t +\displaystyle \frac{L_t}{1+n}} = \frac{1+n}{2+n} \frac{C_t}{L_t}

当 $n$n 固定时， $C_t/L_t$C_t/L_t 的最大化子也能最大化 $C_t/(L_t+L_{t-1})$C_t/(L_t+L_{t-1}) 。

$C_t/L_t$C_t/L_t 式子对 $f$f 求一阶条件，得到 $\chi = 1+n$\chi = 1+n 。而若 $\chi < 1+n$\chi < 1+n ，

> $\displaystyle \frac{\partial (C_t/L_t)}{\partial f} = \left[ \frac{\chi}{1+n}-1 \right]A < 0$\displaystyle \frac{\partial (C_t/L_t)}{\partial f} = \left[ \frac{\chi}{1+n}-1 \right]A < 0

这意味着减少 $f$f 总能严格提高每劳动消费。所以最优的 $f$f 是 0。

不难看出 $\chi = 1+r$\chi = 1+r ，其中 $r$r 为净利率。所以 $\chi < 1+n$\chi < 1+n 等价于 $r < n$r < n ，对应于导致动态效率低下的资本过度积累。

（3）**这种情况下分散均衡帕累托有效吗？如果不是，社会计划者如何提高福利？**

**解**：根据（2），消费最大化的分配不同于分散均衡中的分配。所以后者不是帕累托效率。社会计划者可以在每个时期从每个年轻的代理人那里拿走一个单位的商品。这样，储蓄的有效回报率是 $(1+n)$(1+n) ，因为年轻人可用的消费品以这个速度增长。而且，这个比率大于分散市场的储蓄回报率。这种代际转移在分散均衡中是不可行的，因为不同代际的代理人不能签订跨期契约。

图片附录
----

### 世代交叠

![](https://pic2.zhimg.com/v2-fe6cce09cd43afd9b28cde97b9692f3d_r.jpg)![](https://pic1.zhimg.com/v2-d570b5800b03cedc7d5f125c663b4e80_r.jpg)

### k 的动态

![](https://pic3.zhimg.com/v2-dfefd1f89f93c8a01a4157450391e94a_r.jpg)

### 动态无效率

![](https://pic4.zhimg.com/v2-9a837815155e032b4ec3d2edde2630bf_r.jpg)

### R2.14 图

![](https://pic2.zhimg.com/v2-437f1a0b42df2df3f6a80b87388124b5_r.jpg)

补充练习
----

### 有托儿服务均衡

**Q-6-2B. 内生生育与市场托儿服务**。假设托儿服务存在市场，即代表性的 $t$t 代也需要选择 $n_t = θχ^σ_t z^{1−σ}_ t$n_t = θχ^σ_t z^{1−σ}_ t 中的 $χ_t$χ_t 。问：

（1）**写出代表性代理人的优化问题，并推导出一阶条件**。

**解**：第 $t$t 代代表性个体的终身效用为：

> $u_t = \log c^1_t + ε \log ( θχ^σ_t z^{1−σ}_ t) + ρ \log c^2_{t +1}$u_t = \log c^1_t + ε \log (θχ^σ_t z^{1−σ}_ t) + ρ \log c^2_{t +1}

生命周期预算约束：

> $\displaystyle (1 − τ )w(1 − z_t) = c^1_t + \frac{c^2_{t +1} − β_{t+1}}{r} + \chi_t$\displaystyle (1 − τ)w(1 − z_t) = c^1_t + \frac{c^2_{t +1} − β_{t+1}}{r} + \chi_t

第 $t$t 代个体的拉格朗日函数为：

> $\displaystyle \mathcal L = \log c^1_t + ε \log ( θχ^σ_t z^{1−σ}_ t) + ρ \log c^2_{t +1} + λ_t \left[ \displaystyle (1 − τ )w(1 − z_t) - c^1_t - \frac{c^2_{t +1} − β_{t+1}}{r} - \chi_t \right]$\displaystyle \mathcal L = \log c^1_t + ε \log (θχ^σ_t z^{1−σ}_ t) + ρ \log c^2_{t +1} + λ_t \left[ \displaystyle (1 − τ )w(1 − z_t) - c^1_t - \frac{c^2_{t +1} − β_{t+1}}{r} - \chi_t \right]

一阶条件为：

> $\displaystyle \frac{\partial \mathcal L}{\partial c_t^1}=0 \Rightarrow \frac{1}{c_t^1} = \lambda_t$\displaystyle \frac{\partial \mathcal L}{\partial c_t^1}=0 \Rightarrow \frac{1}{c_t^1} = \lambda_t  
> $\displaystyle \frac{\partial \mathcal L}{\partial c_{t+1}^2}=0 \Rightarrow \frac{ρ}{c_{t+1}^2} = \frac{\lambda_t}{r}$\displaystyle \frac{\partial \mathcal L}{\partial c_{t+1}^2}=0 \Rightarrow \frac{ρ}{c_{t+1}^2} = \frac{\lambda_t}{r}  
> $\displaystyle \frac{\partial \mathcal L}{\partial χ_t}=0 \Rightarrow \frac{εσ}{χ_t} = λ_t$\displaystyle \frac{\partial \mathcal L}{\partial χ_t}=0 \Rightarrow \frac{εσ}{χ_t} = λ_t  
> $\displaystyle \frac{\partial \mathcal L}{\partial z_t}=0 \Rightarrow \frac{ε(1 -σ)}{z_t} = w(1-\tau)λ_t$\displaystyle \frac{\partial \mathcal L}{\partial z_t}=0 \Rightarrow \frac{ε(1 -σ)}{z_t} = w(1-\tau)λ_t

（2）**利用一阶条件，推导出稳态托儿时间** **$z^*$z^*** **的表达式**。

**解**：联立得最优储蓄、托儿服务时间和养娃时间为：

> $\displaystyle s_t = \frac{ρ}{1 + ρ + ε} w(1 − τ ) − \frac{1 + ε}{1 + ρ + ε} \frac{β_{t+1}}{r}$\displaystyle s_t = \frac{ρ}{1 + ρ + ε} w(1 − τ ) − \frac{1 + ε}{1 + ρ + ε} \frac{β_{t+1}}{r}  
> $\displaystyle \chi_t = \frac{εσ}{1 + ρ + ε} \left[ (1-\tau)w +\frac{β_{t+1}}{r} \right]$\displaystyle \chi_t = \frac{εσ}{1 + ρ + ε} \left[ (1-\tau)w +\frac{β_{t+1}}{r} \right]  
> $\displaystyle z_t = \frac{ε(1-σ)}{w(1-\tau)(1 + ρ + ε)} \left[ (1-\tau)w +\frac{β_{t+1}}{r} \right]$\displaystyle z_t = \frac{ε(1-σ)}{w(1-\tau)(1 + ρ + ε)} \left[ (1-\tau)w +\frac{β_{t+1}}{r} \right]

代入 $n_t = θχ^σ_t z^{1−σ}_ t$n_t = θχ^σ_t z^{1−σ}_ t 得到最优生育计划：

> $\displaystyle n_t = \frac{θεσ^σ(1 - σ)^{1-σ}}{(1 + ρ + ε)[w(1-\tau)]^{1-\sigma}} \left[ (1-\tau)w +\frac{β_{t+1}}{r} \right]$\displaystyle n_t = \frac{θεσ^σ(1 - σ)^{1-σ}}{(1 + ρ + ε)[w(1-\tau)]^{1-\sigma}} \left[ (1-\tau)w +\frac{β_{t+1}}{r} \right]

将 $z_t$ z_t 和 $n_t$ n_t 的表达式联立得：

> $n_t = [(1-\tau)w]^{\sigma} \theta [\sigma / (1-\sigma)] ^\sigma z_t$n_t = [(1-\tau)w]^{\sigma} \theta [\sigma / (1-\sigma)] ^\sigma z_t

代回 $z_t$ z_t 的表达式得：

> $\displaystyle \frac{1+\rho+\varepsilon}{\varepsilon(1-\sigma)}= \frac{1}{z_t}+\frac{ (1-z_{t+1})\tau}{r(1-\tau)} [(1-\tau)w]^\sigma \theta [\sigma / (1-\sigma)] ^\sigma$\displaystyle \frac{1+\rho+\varepsilon}{\varepsilon(1-\sigma)}= \frac{1}{z_t}+\frac{ (1-z_{t+1})\tau}{r(1-\tau)} [(1-\tau)w]^\sigma \theta [\sigma / (1-\sigma)] ^\sigma

这个方程给出了系统的动力学。当 $σ= 0$σ= 0 且 $θ = 1$θ = 1 时，就变为无托儿服务均衡下的方程：

> $\displaystyle z_t = \frac{ε}{1 + ρ + ε} \left[ 1 +\frac{τ (1 − z_{t+1})z_t}{r(1 − τ )z} \right]$\displaystyle z_t = \frac{ε}{1 + ρ + ε} \left[ 1 +\frac{τ (1 − z_{t+1})z_t}{r(1 − τ )z} \right]

设存在一个稳态 $z^*$z^* ，得到：

> $\displaystyle \frac{dz_{t+1}}{dz_t} = - \frac{1/z^{*2}}{(w/r)\tau\theta [w(1-\tau)^{\sigma-1}[\sigma/(1-\sigma)]^\sigma} < 0$\displaystyle \frac{dz_{t+1}}{dz_t} = - \frac{1/z^{*2}}{(w/r)\tau\theta [w(1-\tau)^{\sigma-1}[\sigma/(1-\sigma)]^\sigma} < 0

稳定性条件为：

> $z^{*2 }> \{ (w/r) \tau \theta [(1-\tau)w]^{\sigma-1} [\sigma / (1-\sigma )^\sigma ] \}^{-1}$z^{*2}> \{ (w/r) \tau \theta [(1-\tau)w]^{\sigma-1} [\sigma / (1-\sigma )^\sigma ] \}^{-1}

假设满足稳定条件，则有稳态育儿时间 $z^*$z^* 满足：

> $\displaystyle \frac{1+\rho+\varepsilon}{\varepsilon(1-\sigma)}= \frac{1}{z^*}+\frac{ (1-z^*)\tau}{r(1-\tau)} [(1-\tau)w]^\sigma \theta [\sigma / (1-\sigma)] ^\sigma$\displaystyle \frac{1+\rho+\varepsilon}{\varepsilon(1-\sigma)}= \frac{1}{z^*}+\frac{ (1-z^*)\tau}{r(1-\tau)} [(1-\tau)w]^\sigma \theta [\sigma / (1-\sigma)] ^\sigma

（3）**稳态下抚养孩子的时间是否在税率** **$\tau$\tau** **上严格增加**？

**解**：是的。上式对 $\tau$\tau 求导得：

> $\displaystyle \frac{dz}{d\tau} = \frac{(1-z)w^2 \theta [\sigma / (1-\sigma)]^\sigma [(1-\tau)w]^{\sigma-2}(1-\sigma \tau)}{(r/z^2)+w\tau \theta [(1-\tau )w]^{\sigma-1} [\sigma / (1-\sigma)]^\sigma}>0$\displaystyle \frac{dz}{d\tau} = \frac{(1-z)w^2 \theta [\sigma / (1-\sigma)]^\sigma [(1-\tau)w]^{\sigma-2}(1-\sigma \tau)}{(r/z^2)+w\tau \theta [(1-\tau )w]^{\sigma-1} [\sigma / (1-\sigma)]^\sigma}>0

也就是说，工资税率的提高增加了父母抚养孩子的时间。

（4）**稳态的生育率是否在税率** **$\tau$\tau** **上严格增加**？

**解**：不是。由 $z_t$ z_t 和 $n_t$ n_t 的联立式：

> $\displaystyle \frac{d n}{d \tau} = \theta [\sigma / (1-\sigma)]^\sigma (z/\tau ) [ (1-\tau)w]^\sigma \left( \eta - \sigma \frac{\tau}{1-\tau} \right)$ \displaystyle \frac{d n}{d \tau} = \theta [\sigma / (1-\sigma)]^\sigma (z/\tau ) [ (1-\tau)w]^\sigma \left( \eta - \sigma \frac{\tau}{1-\tau} \right)

其中， $\displaystyle\frac{d n}{d \tau}$ \displaystyle\frac{d n}{d \tau} 的正负由 $\displaystyle \left( \eta - \sigma \frac{\tau}{1-\tau} \right)$ \displaystyle \left(\eta - \sigma \frac{\tau}{1-\tau} \right) 决定。

（5）**提供一个直观的解释，说明为什么对（4）的回答与没有市场托儿服务的模型中的结果不同**。

**解**：有市场托儿服务的模型和没有市场托儿服务的模型中一样，稳态下抚养孩子的时间在税率上是增加的。如果稳态的生育率随税率而下降，则必然是稳态的儿童服务市场 $\chi^*$\chi^* 对税率的反应是不确定的。这是因为：

*   一方面，较高的育儿时间增加了市场托儿服务的边际产品，因此其需求更高；
*   另一方面，与市场上固定为 1 的托儿服务相比，上调税金会降低工资率，这导致市场对托儿服务的需求降低。

$dn^* / d \tau$dn^* / d \tau 的符号取决于这两种相互冲突的影响中哪一种占主导地位。

### R2.2

**R2.2**。设想只存活两期的经济个体具有效用函数

> $\displaystyle U = \frac{C_1^{1-\theta}}{1-\theta} + \frac{1}{1+\rho} \frac{C_2^{1-\theta}}{1-\theta}$\displaystyle U = \frac{C_1^{1-\theta}}{1-\theta} + \frac{1}{1+\rho} \frac{C_2^{1-\theta}}{1-\theta} （1）

*   $P_1,P_2$P_1,P_2 分别表示消费品的两期价格
*   $W$W 表示终生收入的价值
*   预算约束是 $P_1C_1+P_2C_2=W$P_1C_1+P_2C_2=W （2）

问：

*   ① 在已知 $P_1,P_2,W$P_1,P_2,W 时，求效用最大化的 $C_1,C_2$C_1,C_2 。
*   ② 求证： $C_1,C_2$C_1,C_2 之间的替代弹性为 $1/\theta$1/\theta 。其中两期消费之间的替代弹性可定义成：

> $\displaystyle - \frac{\partial \ln (C_1/C_2)}{\partial \ln (P_1/P_2)} = -\frac{(P_1/P_2)/(C_1/C_2)}{\partial (C_1/C_2)/\partial (P_1/P_2)}$\displaystyle - \frac{\partial \ln (C_1/C_2)}{\partial \ln (P_1/P_2)} = -\frac{(P_1/P_2)/(C_1/C_2)}{\partial (C_1/C_2)/\partial (P_1/P_2)}

**解**：① 预算约束移项可得

> $\displaystyle C_2 = \frac{W}{P_2} - \frac{C_1P_1}{P_2}$\displaystyle C_2 = \frac{W}{P_2} - \frac{C_1P_1}{P_2} （3）

将方程（3）代入方程（1），可得：

> $\displaystyle U = \frac{C_1^{1-\theta}}{1-\theta} + \frac{1}{1+\rho } \frac{(W/P_2 - C_1 P_1 / P_2)^{1-\theta}}{1-\theta}$\displaystyle U = \frac{C_1^{1-\theta}}{1-\theta} + \frac{1}{1+\rho } \frac{(W/P_2 - C_1 P_1 / P_2)^{1-\theta}}{1-\theta} （4）

根据方程（4）的无约束效用最大化问题求解第 1 期消费 $C_1$C_1，相应的一阶条件为：

> $\displaystyle \frac{\partial U}{\partial C_1} = C_1 ^{-\theta} + \left( \frac{1}{1+\rho} \right)C_2^{-\theta} \left( -\frac{P_1}{P_2} \right)= 0$\displaystyle \frac{\partial U}{\partial C_1} = C_1 ^{-\theta} + \left( \frac{1}{1+\rho} \right)C_2^{-\theta} \left( -\frac{P_1}{P_2} \right)= 0 （5）

求解 $C_1$ C_1 可得：

> $\displaystyle C_1 ^{-\theta} = \left( \frac{1}{1+\rho}\right) \left(\frac{P_1}{P_2} \right) C_2 ^{-\theta}$\displaystyle C_1 ^{-\theta} = \left( \frac{1}{1+\rho}\right) \left(\frac{P_1}{P_2} \right) C_2 ^{-\theta} （6）

简化上式得到：

> $\displaystyle C_1 = (1+\rho )^{1/\theta} \left( \frac{P_1}{P_2} \right) C_2^{-\theta}$ \displaystyle C_1 = (1+\rho)^{1/\theta} \left( \frac{P_1}{P_2} \right) C_2^{-\theta} （7）

为求解 $C_2$C_2 ，将方程（7）代入方程（3）导出：

> $\displaystyle C_2 = \frac{W}{P_2} - (1+\rho )^{1/\theta} \left( \frac{P_2}{P_1} \right)^{1/\theta} C_2 \left( \frac{P_1}{P_2} \right)$ \displaystyle C_2 = \frac{W}{P_2} - (1+\rho )^{1/\theta} \left( \frac{P_2}{P_1} \right)^{1/\theta} C_2 \left( \frac{P_1}{P_2} \right) （8）

简化上式可得：

> $\displaystyle C_2 \left[ 1+ (1+\rho)^{1/\theta} \left( \frac{P_2}{P_1} \right)^{(1-\theta)/\theta} \right] = \frac{W}{P_2}$\displaystyle C_2 \left[1+ (1+\rho)^{1/\theta} \left( \frac{P_2}{P_1} \right)^{(1-\theta)/\theta} \right] = \frac{W}{P_2} （9）  
> $\displaystyle C_2 = \frac{W/P_2}{1+(1+\rho)^{1/\theta}(P_2/P_1)^{(1-\theta)/\theta}}$\displaystyle C_2 = \frac{W/P_2}{1+(1+\rho)^{1/\theta}(P_2/P_1)^{(1-\theta)/\theta}} （10）

最终求解$C_1$C_1 的最优选择，将方程（10）代入方程（7）可得

> $\displaystyle C_1 = \frac{(1+\rho)^{1/\theta}(P_2/P_1)^{1/\theta} (W/P_2)}{1+(1+\rho)^{1/\theta}(P_2/P_1)^{(1-\theta)/\theta}}$\displaystyle C_1 = \frac{(1+\rho)^{1/\theta}(P_2/P_1)^{1/\theta} (W/P_2)}{1+(1+\rho)^{1/\theta}(P_2/P_1)^{(1-\theta)/\theta}} （11）

② 根据方程（7），第 1 期消费对第 2 期消费的最优比率为：

> $\displaystyle \frac{C_1}{C_2} = (1+ \rho ) ^{1/\theta} \left( \frac{P_2}{P_1} \right) ^{1/\theta}$ \displaystyle \frac{C_1}{C_2} = (1+ \rho ) ^{1/\theta} \left( \frac{P_2}{P_1} \right) ^{1/\theta} （12）

对方程（12）的两端取自然对数，得到：

> $\displaystyle \ln\left( \frac{C_1}{C_2} \right) = \left( \frac{1}{\theta} \right) \ln (1+\rho) + \left( \frac{1}{\theta} \right) \ln \left( \frac{P_2}{P_1} \right)$ \displaystyle \ln\left(\frac{C_1}{C_2} \right) = \left( \frac{1}{\theta} \right) \ln (1+\rho) + \left( \frac{1}{\theta} \right) \ln \left( \frac{P_2}{P_1} \right) （13）

约定弹性取值为正，使用方程（13）的导数定义$C_1 ,C_2$ C_1 ,C_2 的替代弹性：

> $\displaystyle \frac{\partial \ln (C_1/C_2)}{\partial \ln (P_2/P_1)} = \frac{1}{\theta}$\displaystyle \frac{\partial \ln (C_1/C_2)}{\partial \ln (P_2/P_1)} = \frac{1}{\theta} （14）

因此，较高的 $\theta$\theta 值意味着个人相对不愿意替换不同时期间的消费。

### R2.14

**R2.14**。假设对数效用函数和道格拉斯生产函数的 OLG 模型。说明下列变化如何影响 $k_{t+1} (k_t)$k_{t+1} (k_t) 函数。

*   （1） $n$n 增加
*   （2）生产函数向下移动（将 $f(k)$f(k) 表示成 $Bk^\alpha$Bk^\alpha 的形式， $B$B 下降）
*   （3） $\alpha$\alpha 增加

注：罗默教材中，对数效用函数和道格拉斯生产函数的特殊情形要求的 $k_{t+1} (k_t)$k_{t+1} (k_t) 表达式为：

> $\displaystyle k_{t+1 } = \frac{1}{(1+n) (1+g) } \frac{1}{2+\rho } (1-\alpha) k_t^\alpha$\displaystyle k_{t+1} = \frac{1}{(1+n) (1+g) } \frac{1}{2+\rho } (1-\alpha) k_t^\alpha （2.61）

**解**：（1）（2）的图象均为实际投资曲线末端向下移动，使得与直线的交点 $k^*$k^* 左移。（见附录）

（3）需要分类讨论：根据方程（2.61）导出

> $\displaystyle \frac{\partial k_{t+1}}{\partial \alpha} = \frac{1}{(1+n)(1+g)}\frac{1}{2+\rho} \left[ -k_t^\alpha + (1-\alpha) \frac{\partial k_t^\alpha}{\partial \alpha} \right]$\displaystyle \frac{\partial k_{t+1}}{\partial \alpha} = \frac{1}{(1+n)(1+g)}\frac{1}{2+\rho} \left[ -k_t^\alpha + (1-\alpha) \frac{\partial k_t^\alpha}{\partial \alpha} \right] （*）

设 $f(\alpha )\equiv k_t^\alpha$ f(\alpha)\equiv k_t^\alpha ，则 $\ln f(\alpha) = \alpha \ln k_t$\ln f(\alpha) = \alpha \ln k_t ，得到：

> $\displaystyle \frac{\partial \ln f(\alpha )}{\partial \alpha } = \ln k_t$\displaystyle \frac{\partial \ln f(\alpha)}{\partial \alpha } = \ln k_t

可以导出：

> $\displaystyle \frac{\partial f(\alpha)}{\partial \alpha} = \frac{\partial f(\alpha)}{\partial \ln f(\alpha)} \frac{\partial \ln f(\alpha)}{\partial \alpha} = \frac{1}{\partial \ln f(\alpha)/ \partial f(\alpha) } \frac{\partial \ln f(\alpha)}{\partial \alpha}$\displaystyle \frac{\partial f(\alpha)}{\partial \alpha} = \frac{\partial f(\alpha)}{\partial \ln f(\alpha)} \frac{\partial \ln f(\alpha)}{\partial \alpha} = \frac{1}{\partial \ln f(\alpha)/ \partial f(\alpha) } \frac{\partial \ln f(\alpha)}{\partial \alpha}

根据上式导出：

> $\displaystyle \frac{\partial f(\alpha)}{\partial \alpha} = f(\alpha ) \ln k_t$\displaystyle \frac{\partial f(\alpha)}{\partial \alpha} = f(\alpha ) \ln k_t

因此得到：

> $\displaystyle \frac{\partial k_t^\alpha }{\partial \alpha } = k_t^\alpha \ln k_t$\displaystyle \frac{\partial k_t^\alpha}{\partial \alpha } = k_t^\alpha \ln k_t

将此结果代入方程（*），得到：

> $\displaystyle \frac{\partial k_{t+1}}{\partial \alpha} = \frac{1}{(1+n)(1+g)}\frac{1}{2+\rho} \left[ -k_t^\alpha + (1-\alpha) k_t^\alpha \ln k_t\right]$\displaystyle \frac{\partial k_{t+1}}{\partial \alpha} = \frac{1}{(1+n)(1+g)}\frac{1}{2+\rho} \left[ -k_t^\alpha + (1-\alpha) k_t^\alpha \ln k_t\right]

简化上式得到：

> $\displaystyle \frac{\partial k_{t+1}}{\partial \alpha} = \frac{1}{(1+n)(1+g)}\frac{1}{2+\rho} \left\{ k_t^\alpha [ (1-\alpha) \ln k_t-1]\right\}$\displaystyle \frac{\partial k_{t+1}}{\partial \alpha} = \frac{1}{(1+n)(1+g)}\frac{1}{2+\rho} \left\{ k_t^\alpha [ (1-\alpha) \ln k_t-1]\right\}

*   $\ln k_t > 1/1(1-\alpha)$\ln k_t > 1/1(1-\alpha) ： $\alpha$\alpha 增加意味着既定 $k_t$k_t 对应的 $k_{t+1}$k_{t+1} 增加， $k_{t+1}$k_{t+1} 函数上移
*   $\ln k_t < 1/1(1-\alpha)$\ln k_t <1/1(1-\alpha) ： $\alpha$\alpha 增加意味着既定 $k_t$k_t 对应的 $k_{t+1}$k_{t+1} 增加， $k_{t+1}$k_{t+1} 函数下移
*   $\ln k_t = 1/1(1-\alpha)$\ln k_t = 1/1(1-\alpha) ：新的 $k_{t+1}$k_{t+1} 函数正好与原函数相交

参考文献
----

1.  罗默. 高级宏观经济学.
2.  焼田党. 人口老龄化、生育与社会保障.

相关研究
----

1.  [Two-child policy, gender income and fertility choice in China - ScienceDirect](https://link.zhihu.com/?target=https%3A//www.sciencedirect.com/science/article/pii/S1059056018310694%23sec3)

预习笔记
----

[Mr Figurant：高级宏观笔记 02：2 - 世代交叠 - Diamond](https://zhuanlan.zhihu.com/p/546574314)

继续学习
----

[Mr Figurant：高级宏观 12：最优税收模型](https://zhuanlan.zhihu.com/p/671467017)

参考
--

1.  [^](#ref_1_0)Samuelson, P. A. (1958). An Exact Consumption-Loan Model of Interest with or without the Social Contrivance of Money. Journal of Political Economy, 66(6), 467–482. [https://sci-hub.se/10.1086/258100](https://sci-hub.se/10.1086/258100)
2.  [^](#ref_2_0)Yakita, Akira. 2017. Population Aging, Fertility and Social Security. Springer. [https://sci-hub.se/10.1007/978-3-319-47644-5_4](https://sci-hub.se/10.1007/978-3-319-47644-5_4)
3.  [^](#ref_3_0)Apps, P., & Rees, R. (2004). Fertility, Taxation and Family Policy*. Scandinavian Journal of Economics, 106(4), 745–763. [https://sci-hub.se/10.1111/j.0347-0520.2004.00386.x](https://sci-hub.se/10.1111/j.0347-0520.2004.00386.x)