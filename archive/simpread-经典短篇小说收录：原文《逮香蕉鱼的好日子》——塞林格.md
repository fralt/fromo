>本文由[简悦SimpRead](http://ksria.com/simpread/)转码，原文地址[zhuanlan.zhihu.com](https://zhuanlan.zhihu.com/p/83086051)

旅馆里住了九十七位纽约来的广告业务员，他们简直把长途电话线全给霸占了。507号房的女孩为了一通长途一直从中午等到快两点半。不过她倒也没闲着。她看了小开本妇女杂志上登的一篇文章，标题是《性是欢愉还是受罪》。她洗了她的梳子和头发刷子。她把那身米色套装裙子上的一处污渍刮掉，又把她在萨克斯买的那件衬衫上的纽扣挪了挪位置，而且还用镊子把她一颗痣上新冒出来的两根毛拔掉。在接线生终于拨响她房间的电话时，她正坐在窗前座位上染指甲，左手上的已经快染完了。

她是那种女孩，绝不会听到电话响便把手里任何东西胡乱一扔的。瞧她那副架势，仿佛是自打进入青春期起，电话就一直在响似的。

电话零零地响着，她继续用小刷子涂抹小手指指甲，刻意描绘着那个月牙形的边缘。接着，她把盖子放回到指甲油瓶上，站起身，把她的左手——那只湿的——在空中前后甩动。她用那只干手把烟灰缸从窗台拿到床头柜上，电话就是放在这里的。她在两张铺叠整齐的单人床中的一张上坐下，捏起话筒，此时，铃声已经响了五六遍了。

“喂，”她说，左手五指叉开着，伸出去，离她那身白丝绸睡衣尽可能远些，这睡衣是此刻她身上唯一穿着的东西，另外就只有一双拖鞋了——那几只戒指她都留在洗澡间里了。

“您要的纽约长途电话接通了，格拉斯太太，”接线生说。

“谢谢你，”女孩说，一边在床头柜上给烟灰缸腾出个地方。

电话里传来一个妇人的声音。“穆里尔吗？是你吗？”

女孩把听筒从耳边稍稍移开一些。“是的，妈妈。你好吗？”她说。

“你可让我担心死了。你干嘛不来电话？你没事吧？”

“我昨儿晚上、前天晚上都一遍遍给你拔电话来着。这儿的电话可——”

“你没事吧?穆里尔？”

女孩把话筒从耳边再多支开去一些。“我挺好的。就是觉得热。这么多年来，佛罗里达还没有这么热过——”

“你怎么不给我打电话呢？我真为你担心——”

“妈妈，亲爱的，别冲着我叫。你的声音我听得很清楚,”那女孩说。“昨儿晚上我给你打了两次。一次就在刚刚——”

“这不，我就跟你爸爸说过没准你昨儿晚上打过电话。可是，没用，他非说——你没事吧，穆里尔？要跟我说实话呀。”

“我挺好的，别再问这个了，求求你了。”

“你们什么时候到的？”

“我也不知道。星期三上午吧，挺早的。”

“谁开的车？”

“他呀，”女孩说。“你别激动嘛。他开得非常棒。我都没想到。”

“真的是他开的？穆里尔，你要向我保——”

“妈妈，”女孩打断了话头，“我不是跟你说了吗？他开得非常棒。一路上时速都没超过五十，我是实话实说。”

“他没冲着树什么的玩什么花招吧？”

“我说了他开得非常棒，妈妈。行了，求求你了。我跟他说了要紧挨白线，该说的都说了，他明白我的意思，也照着做了。他甚至都没打算对树看上一眼——这是明摆着的。哦，对了，老爸把车子拾掇好了吗？”

“还没呢。人家要四百块钱，光就——”

“妈妈，西摩跟爸爸说过这钱由他来出。没有理由让——”

“好吧，以后再说。他行为怎么样——在汽车里和别的地方？”

“挺好的呀，”那女孩说。

“他还是没完没了地叫你那难听的——”

“不了。他现在又起了新的了。”

“是什么？”

“哦，这又有什么两样呢，妈妈？”

“穆里尔，我想知道。你爸爸——”

“好吧，好吧。他管我叫‘1948年度精神流浪小姐’，”女孩说着发出了格格的笑声。

“这没什么好笑的，穆里尔。这根本就一点也不好笑。简直是让人作呕。实际上，是让人感到悲哀。我一想到怎么——”

“妈妈，”女孩打断了话头，“听我说，你记得他从德国给我寄来的那本书吗？你知道吧——那本德国诗集。我把它怎么的啦？我想得脑袋生疼——”

“书你没丢。”

“你敢肯定？”女孩说。

“当然啦。也就是说，我没有丢。就在弗雷迪房间里呢。你把它丢在我这儿了，我没地方放——怎么啦？他又要啦？”

“不。他只是问起这事，在我们开车来的路上。他想知道我读了没有。”

“那可是德文的！”

“是啊，亲爱的。这没什么区别，”女孩说，交叉起了双腿。“他说那些诗正是本世纪独一无二的伟大诗人写的。他说我该去买一本译本什么的来。要不就学会这种语言，如果我愿意的话。”

“可怕。可怕。简直是可悲。的确是的。你爸爸昨儿晚上说——”

“等会儿，妈妈，”女孩说。她走到窗台前取来香烟，点上一支，又回到床边坐下。“妈妈？”她说，吐出了一口烟。

“穆里尔。好，现在你听我说。”

“我听着呢。”

“你爸爸跟西威茨基大夫谈过了。”

“是吗？”女孩说。

“他跟他谈了所有的情况。至少，他说他这样做了——你是了解你爸爸的。那些树的事。窗户的事儿。他对奶奶说的关于她故世的打算那些可怕的事情。他怎样对待百慕大带来的所有的漂亮图片的事情——一切的一切。”

“怎么样？”女孩说。

“哼。头一条，医生说部队把他从医院里放出来简直是在犯罪——我说的全是实话。他非常明确地告诉你父亲很有可能——非常大的可能，他说——西摩会完完全全失去对自己的控制。我说的全是实话。”

“这儿旅馆里就有一位精神病专家，”女孩说。

“谁？他听什么名字？”

“我不清楚。像是叫里塞尔什么的。听说他非常出色。”

“从没听说过他嘛。”

“嗯，反正大家都认为他很了不起。”

“穆里尔，别那么幼稚，好不好。我们太替你担心了。你爸爸昨儿晚上直想打电报让你回来，老实说——”

“我这会儿不想回家，妈妈。你别紧张嘛。”

“穆里尔，我一点儿没瞎说。西威茨基大夫说西摩很可能会完全失去控——”

“我刚到这儿，妈妈。这是多年来我头一次休假，我可不想把什么都胡乱往箱包里一塞就回家，”女孩说。“再说我现在也走不了哇。我皮肤晒坏了，简直没法动。”

“你晒得很厉害吗？我在你包里放了那瓶布朗兹防晒油，你没有抹吗？我就放在——”

“我抹了，可还是挨晒了。”

“太糟糕了。你哪个部位晒坏了？”

“全身上下，好妈妈，哪儿哪儿都是。”

“那真糟糕。”

“我死不了的。”

“告诉我，你跟这位精神病专家谈过啦？”

“唉，也算是谈了吧，”那女孩说。

“他说什么来着？你跟医生谈的时候西摩在哪儿？”

“在大洋厅里，弹钢琴呢。我们来到这儿接连两晚他都弹钢琴了。”

“呣，那医生说什么了？”

“哦，也没几句话。是他先跟我搭话的。昨晚玩Bingo(注：一种带赌博性质的抽彩游戏。)时我坐在他旁边，他问我在那个房间里弹钢琴的是不是我的先生。我说是的，话就是这么说起来的，接着他问我西摩是不是有病或是有什么别的事儿。我就告诉他——”

“他怎么会问起这个来的？”

“我哪里知道，妈妈。我琢磨是因为他脸色不好这样的事吧，”女孩说。“反正，Bingo散局后他和他太太问我愿不愿跟他们一起喝上一杯。我就去了。他太太真让人受不了。你还记得咱们那回在邦维特百货公司橱窗里见到的那件难看的晚礼服吗？就是那件，你说穿的人得有一个非常小，非常小——”

“那件绿的？”

“她正穿着呢。就只看见两片屁股了。她不断地问我西摩是不是跟在麦迪逊大街开一家店——是女帽店——的苏珊妮•格拉斯有亲戚关系。”

“那他到底说了什么？那医生。”

“哦。唉，其实也没说几句话。我的意思是我们在酒吧里呆着，喝了点酒。那里吵得要命。”

“是的，但你可曾——可曾告诉医生他想把奶奶的椅子怎么样吗？”

“没有，妈妈。我可没谈得那么细，”那女孩说。“我可能有机会跟他再谈一次。他一整天都泡在酒吧里。”

“他有没有说他认为西摩有可能变得——你明白吧——反常什么的？也许会对你做出什么来！”

“倒没这样说，”那女孩说。“他得掌握更多的情况呀，妈妈。他们得从你小时候的情况知道起——一切有关的情况。我方才跟你说了，我们简直没法谈话，那里吵得什么似的。”

“对了。你那件蓝色的外衣怎么样了？”

“没问题。我把里面的衬垫取了些出来。”

“今年的时装有什么新情况？”

“太可怕了。不过倒是真漂亮。满眼都是闪光装饰片——真是应有尽有，”女孩说。

“你们的房间怎么样？”

“还行。也就是还行吧。战前我们住过的那间这次没弄到，”女孩说。“今年来的人档次太低了。你真该瞧瞧在餐厅里坐在我们身边的是些什么人。在我们旁边那一桌的。简直像是一路挤在一辆大卡车里来的。”

“唉，现在哪儿哪儿都是这样。你的软底低跟便鞋怎么样？”

“太长了。我早就对你说那鞋太长了。”

“穆里尔，我就再一次问你一句——你真的没事儿吗？”

“是的，妈妈，”女孩说。“都跟你说了快一百遍了。”

“那么你真不想回家？”

“不想，妈妈。”

“你爸爸昨天晚上说，要是你愿意一个人独自到某个地方去把事情好好掂量掂量，他非常愿意支付费用。你满可以做一次惬意的海上航行的。我们俩都认为——”

“不，谢谢了，”女孩说，把叉着的腿放平了。“妈妈，这长途电话很贵——”

“我一想到你在整个战争中怎样一直等着那小子——我的意思是当你想到所有那些中了魔法似的年轻妻子，她们——”

“妈妈，”女孩说，“咱们还是挂上电话吧。西摩不定什么时候都会进来的。”

“他在哪儿？”

“在海滩上。”

“在海滩上？就他自己一个人？他在海滩上表现得好吧？”

“妈妈，”女孩说，“你这么说他就好像他是个乱叫乱嚷的疯子似的——”

“这样的话我可一个字也没说呀，穆里尔。”

“哼，你话里就有这个意思。我是说他光是躺在沙滩上。他连浴袍都不肯脱。”

“他不肯脱睡袍？为什么不肯？”

“我不知道。我猜他觉得自己太苍白了吧。”

“我的天，他正需要晒太阳呢。你就不能让他听你的？”

“你是知道西摩的脾气的，”女孩说，又一次把腿交叉起来。“他说他不想让一堆傻瓜盯看他身上的纹身。”

“他身上没刺任何纹身呀！他在部队里纹身啦？”

“没有，妈妈。没有，亲爱的，”女孩说着又站起了身子。“听我说，没准明天我再给你去电话。”

“穆里尔。等一下，你先听我说。”

“好吧，妈妈，”女孩说，把身体重心全移到右腿上。

“只要他行动，哪怕说话上有一点点古怪的迹象，马上给我打电话——你明白我的意思吧。你听见了吗？”

“妈妈，我又不怕西摩。”

“穆里尔，我要你答应我。”

“好吧，我一定做到就是了。再见了，妈妈，”那女孩说，“跟爸说我爱他。”她挂上了电话。

“又看见更多玻璃(注：这里小女孩是在玩弄语言游戏。原文中“Seemoreglass”与此篇人物名字西摩•格拉斯(SeymourGlass)谐音。)了，”西比尔•卡彭特说，她跟她母亲也住在这座旅馆里。“你见到更多玻璃了吗？”

“坏小妞，不许再那样说。妈妈简直要给你逼疯了。别乱动，求求你了。”

卡彭特太太正往西比尔双肩上抹防晒油，往下涂匀在她背上那两片细嫩的、翅膀般的肩胛骨上。西比尔摇摇晃晃地坐在一只充了气的海滩大皮球上，面对着大海。她穿着一套嫩黄色两件套的游泳衣，其中一件即使再过十年八年也未必对她有用。

“那其实只是一条普普通通的丝巾——你靠近了就能看清了，”坐在卡彭特太太旁边一张躺椅里的那个女人说。“我真想知道她是怎么系的。那真招人喜欢。”

“听起来也招人喜欢，”卡彭特太太应了一句。“西比尔，别动，淘气包。”

“你见到更多玻璃了吗？”西比尔说。

卡彭特太太叹了口气，“算了，”她说。她把防晒油瓶子的盖子拧上。“好了，你走开去玩吧，小淘气。妈咪要回旅馆去和哈贝尔太太喝杯马提尼酒。我一会儿给你带橄榄来。”

西比尔得到解脱，马上就奔过一段平坦的海滩，开始朝渔人亭的方向走去。她仅仅停下了一次，为的是把脚往一个海水泡透、坍塌的沙堡狠狠地踩下去，很快，她就走出了旅馆为游客划定的海滨浴场。

她走了大约四分之一英里，突然斜着朝海滩的一个松软部分冲上去。最后，在一个仰面躺着的年轻人的跟前猛地收住脚步。

“你打算下水吗，见到更多玻璃？”她说。

年轻人吃了一惊，他的右手伸上去捏住毛巾浴袍的翻领。他翻过身趴着睡，任凭一条卷起来盖住眼睛的毛巾掉落下来，接着他眯起眼睛仰望着西比尔。

“嘿。你好，西比尔。”

“你想下水吗？”

“我在等你呢，”年轻人说。“有什么新鲜事？”

“什么？”西比尔说。

“有什么新鲜事？今天有什么节目？”

“我爸爸明天要坐一架奈里飞机来，”西比尔说，一面踢着沙子。

“别往我脸上踢呀，宝贝儿，”年轻人说，把手按在西比尔的脚踝上。“我说，他也该来了，你爸爸。我每时每刻都在等他来。每时每刻呢。”

“那位女士在哪儿？”西比尔说。

“那位女士？”年轻人掸出些他稀疏头发里的沙子。“那可难说了，西比尔。那么多地方谁知道她在哪里。没准在美发厅。把她的头发染成貂皮颜色。要不就在她房间里，给穷苦孩子缝布娃娃。”年轻人此刻采取了平卧的姿势，他捏起两只拳头，把一只摞在另一只上，又把下巴搁在上面的那只拳头上。“问我点儿别的什么，西比尔，”他说。“你穿的游泳衣挺不错的。要说我喜欢什么，那就是一件蓝游泳衣了。”

西比尔盯着他看，接着又低下头看看自己鼓嘟嘟的肚皮。“这件可是黄的，”她说。“这件是黄的。”

“是吗？你走过来一点。”

西比尔往前跨了一步。

“你完全正确。瞧我有多傻。”

“那你准备下水吗？”西比尔说。

“我正在严肃考虑这个问题呢。我正反过来复过去地想呢，西比尔，你一定会很想知道的。”

西比尔捅了捅年轻人有时用来作枕头的那只橡皮气床。“这得打气了，”她说。

“你说得不错。它需要的气比我认为够了的多。”他移开两只拳头，让下巴落在沙子上。“西比尔，”他说，“你看上去气色不错。见到你真好。给我说说你自己的事儿。”他伸出胳膊把西比尔两只脚腕都捏在手里。“我是山羊座的，”他说。“你是什么座的？”

“沙伦•利普舒兹说，你让她跟你一块儿坐在钢琴凳上，”西比尔说。

“沙伦•利普舒兹这么说了吗？”

西比尔使劲儿点了点头。

他松开她的脚腕，收回双手，把一边儿的脸靠在他的右前臂上。“哦，”他说，“你也知道那样的事儿怎么来的，西比尔。我坐在那里弹琴。没见到你的人影。而沙伦•利普舒兹走过来挨着我坐下。我总不能把她推下去吧，是不是？”

“能的。”

“哦，不，不行的。这样的事儿我做不出来，”年轻人说。“不过我可以告诉你我当时是怎么做的。”

“怎么做的？”

“我假设她就是你。”

西比尔立刻弯下腰去，开始在沙滩上挖掘起来。“咱们下水吧，”她说。

“好吧，”年轻人说。“我寻思我也能抽空去泡一会儿的。”

“下一回，得把她推开，”西比尔说。

“把谁推开？”

“沙伦•利普舒兹呀。”

“哦，沙伦•利普舒兹，”那年轻人说。“这名字怎么起的。里面混合着回忆与欲望。”他猛地站起身子。他朝大海看出。“西比尔，”他说，“我告诉你咱们干什么好。咱们要看看能不能逮到一条香蕉鱼。”

“一条什么？”

“一条香蕉鱼呀，”他说，同时解开了他浴衣的腰带，脱掉浴衣。他的肩膀又白又窄，他那条游泳裤是宝蓝色的。他折好他的浴袍，先是竖着对折，然后横里折成三叠。他把盖眼睛的毛巾展开，铺在沙滩上，然后把叠好的浴袍放在上面。他弯下身子，捡起气床，把它挟在右胳肢窝底下。接着又伸出左手拉住西比尔的手。

这两个人开始朝海里走去。

“我猜你这么大准见过不少香蕉鱼吧，”年轻人说。

西比尔摇了摇她的头。

“你没见到过？你是住在什么地方的，那么说？”

“我不知道。”

“你肯定知道。你必然知道。沙伦•利普舒兹知道她住在什么地方而她只有三岁半。”

西比尔站住脚，猛地挣开被他拉住的手。她拾起一只普普通通的海滩上的贝壳，仔仔细细地察看着。她把贝壳扔掉，“是康涅狄格州的惠利森林，”她说，恢复了她的行走，小肚皮挺出在最前面。

“康涅狄格州的惠利森林，”年轻人说。“这么说，你的家正好是在离康涅狄格州惠利森林不远的某个地方？”

西比尔看着他。“那正是我住的地方，”她不耐烦地说。“我就住在康涅狄格州惠利森林。”她跑了几步，把他甩在后面，左手吊住左脚，单腿跳了两三步。

“你不知道这一来事情就变得非常清楚了，”年轻人说。

西比尔放下她的脚。“你看过《小黑人萨姆博》吗？”她说。

“你问我这个太有意思了，”他说。“巧得很，我昨天晚上刚看完。”他弯下身去再次捏住西比尔的手。“你觉得这书怎么样？”他问小女孩。

“那些老虎全绕着那棵树跑吗？”

“我认为它们从来没停下过。我从来没有见到过那么多老虎。”

“一共只有六只呀，”西比尔说。

“只有六只！”年轻人说。“你还说只有？”

“你喜欢蜡吗？”西比尔问道。

“我喜欢什么？”年轻人问。

“蜡。”

“非常喜欢。你不喜欢吗？”

西比尔点点头。“你喜欢橄榄吗？”她问。

“橄榄——喜欢的，橄榄和蜡。我不管什么时候走到哪里都要带上它们的。”

“你喜欢沙伦•利普舒兹吗？”

“是的。是的，我喜欢，”年轻人说。“我特别喜欢的是她从不欺侮旅馆大厅里的小小狗。就拿那位加拿大太太的那只小型大头狗来说吧。你也许不会相信，但是有些小女孩就喜欢用气球杆去戳弄它。沙伦不这么干。她从来不那么歹毒，那么不存好心。这就是我那么喜欢她的原因。”

西比尔不吱声了。

“我喜欢嚼蜡烛。”最后她说。

“又有谁不喜欢呢？”年轻人说，把脚泡湿了。“唷！好冷呀。”他把橡皮气床平扔到水里。“不，先等等，西比尔。咱们再走出去一点点。”

他们趟着水往海里走，直到水没到西比尔的腰。接着年轻人把她抱起，让她面朝下平躺在气床上。

“你从来也不戴游泳帽什么的吗？”他问。

“别撒手，”西比尔命令道。“你抓住我呀，喂。”

“卡彭特小姐。行了。我是懂行的，”那年轻人说。“你就只管睁大眼睛看有没有香蕉鱼好了，今天可是逮香蕉鱼的最佳日子呀。”

“我没见到有鱼嘛，”西比尔说。

“那是很自然的。它们的习性非常特别。”他继续推着气床。水还没有没到他胸口。“它们过着一种非常悲惨的生活，”他说。“你知道它们干什么吗，西比尔？”

小女孩摇了摇头。

“嗯，它们游到一个洞里去，那儿有许多香蕉。它们游进去时还是样子很普通的鱼。可是它们一进了洞，就馋得跟猪一样了。嘿，我就知道有那么一些香蕉鱼，它们游进一个香蕉洞，居然吃了足足有七十八根香蕉。”他推着气床和上面的乘客又往海平面前进了一英尺。“自然，它们吃得太胖了，就再也没法从洞里出来了。连挤都挤不出洞口了。”

“别离岸太远了，”西比尔说。“后来它们怎么样了？”

“后来谁怎么样了？”

“那些香蕉鱼呀。”

“哦，你是说吃了那么多香蕉出不了香蕉洞的那些鱼后来怎样吗？”

“是啊，”西比尔说。

“唉，我真不忍心告诉你，西比尔。它们死了。”

“为什么呢？”西比尔问。

“哦，它们得了香蕉热。那是一种可怕的病。”

“有个浪头冲过来了，”西比尔紧张地说。

“咱们不理它。咱们瞧不起它，”那年轻人说。“两个自以为了不起的人。”他双手捏住西比尔的两只脚腕，往下压也往前推。气床头一跷盖过了浪头。海水让西比尔的金发湿了个透，不过她的尖叫声里充满了欢乐。

气床重新平稳后，她用手把遮住双眼的一绺扁平的湿发撩开，报告说：“我刚才见到了一条。”“见到什么啦，我的宝贝儿？”

“一条香蕉鱼呀。”

“我的天哪，真的吗？”那年轻人说。“嘴里有香蕉吗？”

“有啊，”西比尔说。“六根呢。”

年轻人突然抓起西比尔垂在气床外缘的一只湿漉漉的脚，亲了亲弓起的脚心。

“嗨！”脚的主人转过身子来说。

“嗨什么嗨！咱们该回去了。你玩够了吗？”

“还没呢！”

“对不起了。”他说，把气床朝岸边推去一直到西比尔从上面爬下来。剩下的路他把气床抱在手里。

“再见，”西比尔说，毫无遗憾地朝旅馆的方向跑去。

年轻人穿上浴袍，把翻领捏捏紧，把他的毛巾使劲塞进了口袋。他捡起湿滑沉重的气床，挟在胳膊底下。他独自踩着沉重的步子，穿过柔软、灼热的沙滩朝旅馆走去。

在旅馆专门让洗海水澡的人走的地下大厅里，一个鼻子上涂了含锌软膏的女人和年轻人一起进了电梯。

“我看到你是在瞧我的脚，”电梯开动后他对那女的说。

“对不起，你说什么？”那女的说。

“我说我看到你在瞧我的脚。”

“对不起。方才我是在看地板。”那女的说，把脸转向电梯门。

“要是你想看我的脚，就直说好了，”年轻人说。“别他妈的这么鬼鬼祟祟。”

“请让我出去，”那女的急忙对开电梯的女孩说。

电梯门开了，那女的头也不回地走了出去。

“我两只脚挺正常，没他妈的一丁点儿值得别人盯着的，”年轻人说。“五楼，劳驾。”他从浴袍口袋里掏出钥匙。

他在五楼走出电梯，穿过走廊，进了507号。房间里一股新小牛皮箱子和洗甲水的气味。

他朝在一张单人床上睡着的女孩瞥了一眼。然后他走到一件行李前，打开它，从一叠短裤、内衣底下抽出一把7.65口径的奥其斯自动手枪。他退出弹夹，检查了一下，又重新塞回去。他扳上击铁。接着他走过去在空着的那张单人床上坐下，看看女孩，把枪对准，开了一枪。子弹穿过了他的右侧太阳穴。

[经典短篇小说收录：原文《逮香蕉鱼的好日子》——塞林格](https://link.zhihu.com/?target=https%3A//mp.weixin.qq.com/s%3F__biz%3DMzIxNzUxODY3NQ%3D%3D%26mid%3D2247483769%26idx%3D1%26sn%3D20ad48697d37b4735541d2925d1a5863%26chksm%3D97f9c645a08e4f5365788a600431bbd36fdfe481a61aa0ce54f4ce05d49206dd72491d2f4ee8%26token%3D817502560%26lang%3Dzh_CN%23rd)

关注微信公众号【火星生活空间站】，回复后台“合集”每月领经典小说文包。