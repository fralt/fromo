> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [www.cnblogs.com](https://www.cnblogs.com/danzhang/p/14906331.html)

### 1. Azure DevOps Server 免费吗？

前几天，有个客户说，发现一个新奇的现象 “不需要输入任何产品序列号就可以完成 Azure DevOps Server 产品的安装和配置；在 30 天之试用期到期后，除了系统提示到期之外，也没有发现任何功能异常，服务器可以正常运行，所有功能模块也可以正常使用。这是怎么回事，难道 Azure DevOps Server 免费吗？  
这也是一个被多次问到的问题，我在这篇博客中对 Azure DevOps Server 的授权做一个简单介绍。

### 2. 什么是 Azure DevOps Server？

Azure DevOps Server（简称 ADO Server，之前名叫 TFS）是微软研发管理平台产品，是微软公司对于应用软件生命周期管理的解决方案，它涵盖了软件研发过程中的需求管理、敏捷开发、版本管理、持续集成、持续发布、自动测试等全流程的工具支持。可以从微软的官方网站]([https://docs.microsoft.com/zh-cn/azure/devops/?WT.mc_id=DT-MVP-5001330](https://docs.microsoft.com/zh-cn/azure/devops/?WT.mc_id=DT-MVP-5001330)) 查询详细的介绍。

### 3. 不免费！

对于这个问题，简单的答案是，Express 版本免费，正式版本不免费！  
但是，在这个简单答案的后面，对于 TFS 版本的授权模式和技术限制，还有许多需要注意的内容，下面我们来具体说明一下。

*   首先，来说一下 Azure DevOps Server Express 这个版本
    
    *   Express 版本在功能使用方面，与正式版本几乎没有区别；除了报表和实验室功能之外，它具备了正式版本的所有功能模块，并且还能部署在客户端操作系统（例如 Windows 10/8/7）上；不像正式版本只能部署在服务器操作系统上。
        
    *   在授权模式方面，Express 版本是完全免费的，用户在使用这个版本的过程中，不需要任何付费；但是，Express 版本限制了最多用户数为 5，超过了 5 人的研发团队，使用起来就十分不方便。
        
    *   在 Express 版本的系统部署过程中，你会发现几乎不需要太多的配置，系统自动将所有功能部署在一台计算机中，甚至自动部署了 SQL Server 的 Express 版本；
        
    *   从上面两个特征来看，我们可以了解到这个产品的定位是评估（试用）用途，我们不推荐在企业的正式环境中部署这个版本。当然，如果企业在采用 Azure DevOps Server 的过程中，在评估期间，Express 版本中积累了大量的数据，也可以使用升级的方式，将 Express 版本升级到正式版本，升级方案可以参考这里（[https://docs.microsoft.com/en-us/azure/devops/server/upgrade/express?WT.mc_id=DT-MVP-5001330](https://docs.microsoft.com/en-us/azure/devops/server/upgrade/express?WT.mc_id=DT-MVP-5001330)）。
        
*   然后，我们来重点看看正式版本的授权情况
    
    *   首先，正式版本不免费、也不开源，这是非常明确的；使用 ADO Server 之前，必须获得服务器和客户端的授权；
        
    *   其次，在授权模式上，正式版本的授权内容分为两个部分：
        
        *   服务器端：企业必须拥有 ADO Server 的服务器端授权；简单来说，你安装一台 ADO Server，你必须采购了 ADO Server 的服务器端授权；由于 ADO Server 是运行在 Windows Server 和 SQL Server 之上的，你还必须具有这个两个产品的授权；你可以同步采购这两个产品的授权，也可以使用企业之前采购过的授权。、
            
        *   客户端：客户端的授权是按照用户数（或设备数）来计算的，需要为用户采购客户端授权；如果企业已经采购了 MSDN 订阅，每个订阅已经包含了一个客户端授权；用户也可以通过云端的采购来获取客户端授权，具体可以查看这里的官方说明（[https://azure.microsoft.com/en-us/pricing/details/devops/azure-devops-services/](https://azure.microsoft.com/en-us/pricing/details/devops/azure-devops-services/)）
            
    *   最后，我们来重点看看在具体使用 ADO Server 的过程中需要注意的内容
        
        *   TFS 2015.2 之前：  
            在安装和使用 TFS 2015.2 之前的产品，用户必须从 MSDN 或者销售商处获取到 TFS 的产品序列号，才能完成软件的安装配置工作；如果是从 MSDN 或批量授权渠道获取到安装介质，安装包中已经预设了一个产品序列号；如果是从官网上获取的试用版，当 60 天（可以扩展到 90 天）的试用期结束后，服务器会停止运行，用户不能使用任何功能。因此，使用 2015.2 之前的版本，用户不会问是否免费的问题，因为答案已经很明确了，不免费！
            
        *   TFS 2015.2 之后：  
            在微软发布了 TFS 2015.2 之后（包括当前最新版本的 Azure DevOps Server），研发产品组对授权限制相关的代码做了重大调整。但是要注意，这样的调整只是技术层面的，对产品授权模式不会有任何影响，就是说该买还得买。此后，在部署安装、使用过程中，我们需要注意以下几个方面的问题：
            
            *   安装介质：用户可以从微软 Visual Studio 官网 ([https://visualstudio.microsoft.com/downloads/](https://visualstudio.microsoft.com/downloads/)) 获取到使用版本的安装介质，也可以从批量授权、MSDN 订阅或其他渠道获取到正式版本的安装介质。
            *   上述两种渠道获取到的安装介质，在功能使用方面是完全一样的，具备 ADO Server 的全部完整的功能模块；在安装过程中，用户不需要输入产品序列号，就能完成整个部署过程。二者唯一的区别是，试用版在使用结束后，系统会出现 “版本已经过期” 的字样，但是系统功能不会停止，用户完全可以正常使用，系统不会因为版本过期而停止运行；正式版本在安装过程中、安装完成后，不会出现任何授权的相关提示。

![](https://img-blog.csdnimg.cn/20210424094926397.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2RldjY2,size_16,color_FFFFFF,t_70)  
如果需要，你还可以从微软 Azure DevOps Server 的[在线文档](https://docs.microsoft.com/zh-cn/azure/devops/?WT.mc_id=DT-MVP-5001330)，查询更多的权威资料，也欢迎通过下面的联系方式与我沟通，相互学习

[https://www.cnblogs.com/danzhang](https://www.cnblogs.com/danzhang "https://www.cnblogs.com/danzhang")   
DevOps MVP 张洪君  
![](https://img-blog.csdnimg.cn/20210424095438553.png)