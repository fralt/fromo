## git分支
> git 所有分支之间彼此互不干扰，各自完成各自的工作和内容。可以在分支使用完后**合并到总分支 (原分支)** 上，安全、便捷、不影响其他分支工作

#### 查看当前工作在那个分支

```
git branch
## 返回
## * master

```

可以看到当前的分支叫 `"master"`

#### master 分支

从项目创建之初，有且唯一的分支就是主分支。如果之后再创建分支，就是一个一个的从分支，主分支被叫做`master`

> Git 的 master 分支并不是一个特殊分支。 它就跟其它分支完全没有区别。 之所以几乎每一个仓库都有 master 分支，是因为 git init 命令默认创建它，并且大多数人都懒得去改动它。

#### HEAD

> 这里提个问题。对一个项目提交了很多个分支，如果有两个分支指向相同提交历史，Git 又是怎么知道当前在哪一个分支上呢？

很简单，它有一个名为 HEAD 的特殊指针。 请注意它和许多其它版本控制系统（如 Subversion 或 CVS）里的 HEAD 概念完全不同。 在 Git 中，它是一个指针，指向当前所在的本地分支（译注：将 HEAD 想象为当前分支的别名）。

而 HEAD 所指向的直接关系是当前分支，再找到分支的版本。如下图：

![](https://img-blog.csdnimg.cn/20201221165234243.JPG?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3NpbGVuY2VfcGlub3Q=,size_16,color_FFFFFF,t_70#pic_center)

#### 创建新分支

###### 概念解释

git 创建新分支。即在当前位置创建一个指针，比如起名为 `从分支dev`，然后将 HEAD 指向 dev。如下图：

1.分支创建好后的提交都是在`dev`分支上提交，而之前的总提交`master`分支的提交位置停留在创建从分支`dev`的位置。而`HEAD`会跟随新创建的分支，跟随每一次提交不停的先前移动。保持`HEAD指针`的在提交的最前沿。
2.在`master`上新创建的`dev分支`会继承`master分支`的所有提交，通过 `git log` 可以看出来。

![](https://img-blog.csdnimg.cn/20201221165251386.JPG?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3NpbGVuY2VfcGlub3Q=,size_16,color_FFFFFF,t_70#pic_center)
![](https://img-blog.csdnimg.cn/20201221165314252.JPG?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3NpbGVuY2VfcGlub3Q=,size_16,color_FFFFFF,t_70#pic_center)

###### 实地操作

```
## 创建并切换到dev分支
git checkout -b dev

```

此时就会有两个分支，并且指向 `dev分支`

![](https://img-blog.csdnimg.cn/20201221165341428.JPG#pic_center)

#### 提交分支

当`dev分支`工作完成，需要合并到`master分支`的时候，也只是**将 master 指针指向当前 dev 的位置**，并将 HEAD 指向 master，这时 dev 分支可以直接删除，也可以不删除，删除的话也只是移除了 dev 指针，只留下一个 master 指针，对工作区没有任何的影响，也就是曾经做的所有提交操作都不会有影响。
![](https://img-blog.csdnimg.cn/20201221165354318.JPG?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3NpbGVuY2VfcGlub3Q=,size_16,color_FFFFFF,t_70#pic_center)

#### 切换回主分支

```
## 分支切换回主分支master
git checkout master

```

#### 合并分支

当分支切换回主分支的时候，可以将 dev 的修改提交合并到 master 分支上，使用：

```
## 合并dev到master
git merge dev

```

_**重点：**_

> 这一次的合并称之为快速合并 `fast-forward`。只是将 master 的指针指向了 dev 最后一次提交的位置。

当分支切换回主分支 master 的时候，就可以删除 dev 分支使用：

```
## 删除dev分支
git branch -d dev

```

#### 小结：

查看分支：`git branch`

创建分支：`git branch <name>`

切换分支：`git checkout <name>`

创建 + 切换分支：`git checkout -b <name>`

合并某分支到当前分支：`git merge <name>`

删除分支：`git breach -d <name>`

冲突的发生和解决
--------

当同一个文件被两个分支都修改过，想要合并两个分支就会产生冲突，不能快速将 dev 合并到 master 上。并且 git 会提醒 “合并过程中产生了冲突，请修正后再提交”。

![](https://img-blog.csdnimg.cn/20201221165409121.JPG?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3NpbGVuY2VfcGlub3Q=,size_16,color_FFFFFF,t_70#pic_center)

##### **修正的过程：**

1.将两个分支的文件，进行对比修改，满足两个分支的提交。
2.使用 git add 和 git commit 进行一次新的提交。(此时提交的是 master 分支)
3.再次合并
![](https://img-blog.csdnimg.cn/20201221165423717.JPG?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3NpbGVuY2VfcGlub3Q=,size_16,color_FFFFFF,t_70#pic_center)

#### 查看带有冲突解决的日志

```
git log --graph -- pretty=oneline

```

#### 分支管理策略

> 通常，合并分支时，如果没有冲突，并且分支是单向一条线路继承下来的，git 会使用 fast forword 模式，但是有些快速合并不能成功，但是又没有冲突时，就会触发分支管理策略，git 会自动做一次新的提交。

当两个分支对工作区都进行了修改，但是修改的并不是同一个文件，而是两个不同的文件，也就是不会产生冲突。此时合并的时候，不能使用 **“快速合并”**，就会弹框需要你输入一段合并说明。使用快捷键 **ctrl+x** 退出

#### 合并时禁止快速合并模式

```
## 合并dev到master，禁止快速合并模式，同时添加说明
git merge --no-ff -m '' dev

```

bug 分支
------

##### 描述和说明

使用场景：当在某个分支上正在工作，突然有一个紧急的 bug 需要修复，此时可以使用 `stash`功能，将当前正在工作的`现场存储起来`，等 bug 修复之后，在返回继续工作。

操作顺序：

1.将当前的工作现场临时存储

```
## 对当前现场进行存储
git stash

```

2.切换到 bug 出现的分支上，比如 bug 出现在 `master`分支。如果 bug 就是在当前分支，可以操作此步骤

```
git checkout master

```

3.新添加一个 bug 临时分支

```
git checkout -b bug001

```

4.对代码进行修复。

5.切换回 master 分支

```
git checkout master

```

6.合并 bug 分支到主 master 上

```
git merge --no-ff -m '合并bug分支到master' bug001

```

7.删除 bug001 分支

```
git branch -d bug001

```

8.回到之前的工作现场所在的分支

```
git checkout dev

```

9.查看当前分支保存那些工作现场 (之前封冻存储的工作现场)

```
git stash list

```

10.恢复存储的现场

```
git stash pop

```


##### 小结：

修复 bug 时，通过创建新的 bug 分支进行修复，然后合并，最后删除。

当手头工作没有做完时，先把工作现场 git stash 一下，然后去修复 bug，修复后，再 git stash pop，恢复工作现场。

## gitflow


### 1. Git Flow 原理介绍
在使用 Git 的过程中如果没有清晰流程和规划，否则, 每个人都提交一堆杂乱无章的 commit, 项目很快就会变得难以协调和维护。

Git 版本管理同样需要一个清晰的流程和规范，Vincent Driessen 为了解决这个问题提出了 ASuccessful Git Branching Model

以下是基于 Vincent Driessen 提出的 Git Flow 流程图：

![](https://img2020.cnblogs.com/blog/1666887/202101/1666887-20210126130718746-1037691755.png)

### 2. Git 的常用分支介绍


#### 2.1 Production 分支

也就是我们经常使用的 Master 分支，这个分支最近发布到生产环境的代码，最近发布的Release， 这个分支只能从其他分支合并，不能在这个分支直接修改。

#### 2.2 Develop 分支

这个分支是我们是我们的主开发分支，包含所有要发布到下一个 Release 的代码，这个主要合并与其他分支，比如 Feature 分支。

#### 2.3 Feature 分支

这个分支主要是用来开发一个新的功能，一旦开发完成，我们合并回 Develop 分支进入下一个Release。

#### 2.4 Release 分支

当你需要一个发布一个新 Release 的时候，我们基于 Develop 分支创建一个 Release 分支，完成 Release 后，我们合并到 Master 和 Develop 分支。

#### 2.5 Hotfix 分支

当我们在 Production 发现新的 Bug 时候，我们需要创建一个 Hotfix, 完成 Hotfix 后，我们合并回 Master 和 Develop 分支，所以 Hotfix 的改动会进入下一个 Release。

### 3. Git Flow 各分支操作原理示意

#### 3.1 Master/Develop 分支

所有在 Master 分支上的 Commit 应该打上 Tag，一般情况下 Master 不存在 Commit，Develop 分支基于 Master 分支创建。

![](https://img2020.cnblogs.com/blog/1666887/202101/1666887-20210126130846925-637426621.png)

#### 3.2 Feature 分支

Feature 分支做完后，必须合并回 Develop 分支, 合并完分支后一般会删点这个 Feature 分支，毕竟保留下来意义也不大。

![](https://img2020.cnblogs.com/blog/1666887/202101/1666887-20210126130906885-1228135513.png)

#### 3.3 Release 分支

Release 分支基于 Develop 分支创建，打完 Release 分支之后，我们可以在这个 Release 分支上测试，修改 Bug 等。同时，其它开发人员可以基于 Develop 分支新建 Feature (记住：一旦打了Release 分支之后不要从 Develop 分支上合并新的改动到 Release 分支) 发布 Release 分支时，合并Release 到 Master 和 Develop， 同时在 Master 分支上打个 Tag 记住 Release 版本号，然后可以删
除 Release 分支了。

![](https://img2020.cnblogs.com/blog/1666887/202101/1666887-20210126130922795-1071048922.png)

#### 3.4 Hotfix 分支

hotfix 分支基于 Master 分支创建，开发完后需要合并回 Master 和 Develop 分支，同时在Master 上打一个 tag。

![](https://img2020.cnblogs.com/blog/1666887/202101/1666887-20210126130945348-1035830446.png)

### 4. Git Flow 命令示例

#### 4.1 Develop 相关

* 创建 develop
git branch develop
git push -u origin develop

#### 4.2 Feature 相关

* 开始 Feature
通过 develop 新建 feaeure 分支
git checkout -b feature develop
或者, 推送至远程服务器:
git push -u origin feature
修改 md 文件
git status
git add .
git commit
* 完成 Feature
git pull origin develop
git checkout develop
--no-ff：不使用 fast-forward 方式合并，保留分支的 commit 历史
--squash：使用 squash 方式合并，把多次分支 commit 历史压缩为一次
git merge --no-ff feature
git push origin develop
git branch -d some-feature
如果需要删除远程 feature 分支:
git push origin --delete feature

#### 4.3 Release 相关

* 开始 Feature
git checkout -b release-0.1.0 develop
* 完成 Feature
git checkout master
git merge --no-ff release-0.1.0
git push
git checkout develop
git merge --no-ff release-0.1.0
git push
git branch -d release-0.1.0
git push origin --delete release-0.1.0
合并 master/develop 分支之后，打上 tag
git tag -a v0.1.0 master
git push --tags

#### 4.4 Hotfix 相关

* 开始 Hotfix
git checkout -b hotfix-0.1.1 master
* 完成 Hotfix
git checkout master
git merge --no-ff hotfix-0.1.1
git push
git checkout develop
git merge --no-ff hotfix-0.1.1
git push
git branch -d hotfix-0.1.1
git push origin --delete hotfix-0.1.1
git tag -a v0.1.1 master
git push --tags
## 参考
[git 的分支管理（详细版）_git 分支管理 - CSDN 博客](/archive/git%20的分支管理（详细版）_git%20分支管理%20-%20CSDN%20博客)
[大厂 git 分支管理规范：gitflow 规范指南](/archive/大厂%20git%20分支管理规范：gitflow%20规范指南)