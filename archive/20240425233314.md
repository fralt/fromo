---
date: 2024-04-25 23:33
status: false
summary: 
aliases:
  - zk笔记
---
[home](../home)>[稍后读](稍后读)> 本文由 [简悦 SimpRead](http://ksria.com/simpread/)

我也用了 zk 一段时间了，觉得挺科学，应该是我近半年学习到最有价值的东西吧，分享一些我的理解和整理的笔记吧，以下观点来自对他人观点的整理和总结：

*   不用纠结 Zettelkasten 这个单词是啥，不是高深的玄学，就是德语的 “卡片盒”。
*   你无需像卢曼那样靠 zk 在 30 年里出版 60 本书，200 篇论文，但是 zk 是你的 “第二大脑”，通过修炼你的第二大脑，可以助你形成更深刻的洞见，并更高效率的进行输出。
*   想了解 zk 的背景介绍，可以看这篇文章：[卢曼：与卡片盒交流](https://link.zhihu.com/?target=https%3A//mp.weixin.qq.com/s%3F__biz%3DMzA5MzUzODA1OA%3D%3D%26mid%3D2247483947%26idx%3D1%26sn%3D2cccb76ee58ddd11541d91c4b283b594%26chksm%3D905d104ea72a9958d71c70313d5a4799641aa903e62f32ac1038ec5fb8c903fc7db36c261d26%26scene%3D178%26cur_album_id%3D1477367917246726144%23rd) 。

以下内容可能初读有点烧脑，每句话都是一个观点，都能展开成一段文字，但我挤干了所有水分，留下这么几百字，真的理解了，能持续受益，当然，在真正有价值的东西上花点时间很合算。

整个 zk 系统包含三个核心概念：

**第一：知识结构的流动性**

*   自底向上的归纳总结法，让知识的结构（或者说脉络）自动呈现出来

*   大部分笔记系统是 “**自顶向下**” 的，你一开始就要设定好分类，在往下创作

*   这是反直觉的，违背人思考方式的，它要求你一开始就让清楚整个世界是怎样的。
*   人类认识世界的过程是自底向上的，先看到土，思考自然怎么回事，又看到水和火，再往上抽象，思考自然是怎么回事，同之前的观点做融合修正。
*   自顶向下的思考过程有个前提，你一开始就能大致认清楚世界怎么回事情，后期发现前面考虑的不对会很痛苦。
*   自顶向下做熟悉的事情很方便，但对于探索和创新类思考很笨拙，违背大脑认识新事物的过程。

*   而 zk 笔记系统是 “**自底向上**” 的，你从一个个原子想法（Atomic Idea）开始构建，让他们相互连接，当你不断的在笔记系统中添加想法时，它**内在结构 / 脉络就会逐步自动呈现出来**。
*   你会看到一小群想法聚成一个类别，他们都有一个共同的中心，和周边大量相关的想法，那么**这个自然汇聚出来的中心就会变得很重要**。
*   随着时间推移，该系统将越来越有结构（structured），你不需要去纠结它的主干在哪里，或者你应该按照什么主干或者主题来组织你的知识。
*   就让信息本身慢慢的将自己的结构和主干自动呈现出来，这就是当前这个时间点最优的结构。

*   所谓的 “知识结构（或者说脉络）” 是动态变化的：

*   你的 zk 系统是 “**有生命**” 的，会呼吸的，它的结构会随着你不段加入的想法**持续进化**。
*   新的聚类会自己自动浮现出来，表明这个概念很重要。
*   其他不重要的想法或者子类可以合并或者让他慢慢变得不重要。
*   结构或者说脉络会**随着内容不断变化**。
*   在任何一个时间点，你可以认为，当前呈现出来的脉络或者结构即是你的 “第二大脑” 当前的最优结构。

*   知识结构是非常个性化的：

*   为想法添加互联的方式是非常个人的行为，每个人做的都不一样，今天的我和昨天的我可能做的也不一样。
*   你的卡片盒完美的反应了你当前思考的路径。
*   同时它又反过来帮助你更好的思考。

**第二：想法间的相互连接**

*   传统笔记系统的问题：

*   并不鼓励你笔记之间做内链。
*   让你很容易沉浸在某个特定的主题下而忽略了同其他概念的联系。
*   容易忽略他们背后更大的 “big picture”。

*   zk 系统的设计准则：

*   鼓励你通过不断的浏览，持续发现并建立各种之前你没想到的连接。
*   当你忽然发现之前看似无关的概念和事件之间似乎存在实际的连接时，你将会形成更具有洞察力的见解，同时帮助你归纳出更深刻的理论。

*   zk 系统并不是单纯的 “笔记集合”，而是笔记间的互联关系，这才是更有价值的地方。

**第三：让你的 zk 系统发挥出它的效果**

*   你的 zk 系统会强迫你不断的将脑袋里的想法纸面化：

*   你是否有这样的经历，觉得自己非常了解一件事情了，但当你想向朋友们解释时，往往话来到嘴边就是不知道怎么准确完整描述。
*   大脑的不稳定性：听到的东西留在浅层记忆，并不稳定，随着时间会快速消退漂移，用自己的话复述是一个固化和加深的过程。
*   你认为你完全理解这个事情的，但这其实仅仅只是你的感觉，大脑欺骗了你。
*   直到你真的可以向朋友流畅阐述或者写道纸上，否则你是无法真的确认你理解某个东西的。

*   费曼（不是卡片盒发明人卢曼）如何确定自己完全理解一件事情？

*   他在每次听讲座的时候，都会准备好纸和笔。
*   假装自己正在给一群中学生讲课，把他想要理解的东西阐述给同学们听。
*   用他自己的话，将主要内容向学生描述一遍。

*   重新连接一遍你的各种想法，搞清楚大脑正在发生的事情

*   这会让你的大脑按自然的方式检测到之前没考虑到的 “盲点”。
*   可以帮助你将之前很散乱的想法，凝结（Crystallize）成更加坚固的东西。
*   复述一件事情同样能够帮助你的大脑内的神经元建立更多联系并不断进化。

**后 话**

不管你是学生，科研人员，还是还是公司职员，但凡平时需要写点东西的人，最常见的问题就是对着一张白纸，准备开始写作，你会发现你很难开始，不管你左思又想，[头脑风暴](https://www.zhihu.com/search?q=%E5%A4%B4%E8%84%91%E9%A3%8E%E6%9A%B4&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A2713962647%7D)也好，翻阅各种参考资料也罢，都很难有看起来不错的内容从你的大脑里蹦出来。因为优秀的想法，都不容易在一两天内产生，更别说一两个小时。

而 zk 的价值在于将面向 dead line 最后几个小时的写作方法，拆分成始终贯穿你平时日常生活的写作习惯，你再也不会有面对一张白纸从头开始的尴尬，应为你平时所有的想法都已经摆在你面前了，包括他们各种相互之间的联系。

你要做的只是在这些想法的网络里，挑选出同你今天写作主题相关的内容，然后重新调整好顺序，补充一些必要信息，一会功夫，你就得到一篇初稿了，就是这么简单。

--

参考：

*   [卢曼：与卡片盒交流](https://link.zhihu.com/?target=https%3A//mp.weixin.qq.com/s%3F__biz%3DMzA5MzUzODA1OA%3D%3D%26mid%3D2247483947%26idx%3D1%26sn%3D2cccb76ee58ddd11541d91c4b283b594%26chksm%3D905d104ea72a9958d71c70313d5a4799641aa903e62f32ac1038ec5fb8c903fc7db36c261d26%26scene%3D178%26cur_album_id%3D1477367917246726144%23rd)
*   [卢曼：学习如何阅读](https://link.zhihu.com/?target=https%3A//mp.weixin.qq.com/s%3F__biz%3DMzA5MzUzODA1OA%3D%3D%26mid%3D2247483824%26idx%3D1%26sn%3D1414f5af0d635b5900a0888c11cff1e9%26chksm%3D905d13d5a72a9ac3397472a482db71ecd16820d3c506a1e9d2adaa6ef30eb28589ed2f257321%26scene%3D21%23wechat_redirect)
*   [《会记笔记就会写作》完全版 - 上](https://link.zhihu.com/?target=https%3A//mp.weixin.qq.com/s%3F__biz%3DMzI1NTA4Nzk5Mw%3D%3D%26mid%3D2247483737%26idx%3D1%26sn%3D39b37468fd4bdb3f20589489ecf63118%26chksm%3Dea3a054fdd4d8c59e0625583d5b5b21e1b0f5beed9aece9424d80b4de86e79a2d1a1e31c8b8f%26scene%3D178%26cur_album_id%3D1464601583634939905%23rd)
*   [《会记笔记就会写作》完全版 - 中](https://link.zhihu.com/?target=https%3A//mp.weixin.qq.com/s%3F__biz%3DMzI1NTA4Nzk5Mw%3D%3D%26mid%3D2247483814%26idx%3D1%26sn%3Dd2371a3016337a6bd66cef2cd80653e9%26chksm%3Dea3a05b0dd4d8ca62a7948c4da3e821c95e3a28a7f8479e42944967ba071ff3a79ee25b75e8e%26cur_album_id%3D1464601583634939905%26scene%3D189%23wechat_redirect)
*   [《会记笔记就会写作》完全版 - 下（1）](https://link.zhihu.com/?target=https%3A//mp.weixin.qq.com/s%3F__biz%3DMzI1NTA4Nzk5Mw%3D%3D%26mid%3D2247483897%26idx%3D1%26sn%3D940a4e180424397d0743cb0c91b36b32%26chksm%3Dea3a05efdd4d8cf95a8c8c720155460410f3ae7a3c04fd8020cb9af433f704806930a5c7425b%26cur_album_id%3D1464601583634939905%26scene%3D189%23wechat_redirect)
*   [《会记笔记就会写作》完全版 - 下（2）](https://link.zhihu.com/?target=https%3A//mp.weixin.qq.com/s%3F__biz%3DMzI1NTA4Nzk5Mw%3D%3D%26mid%3D2247483977%26idx%3D1%26sn%3Def1ece822b4478638ce62b3821de2e79%26chksm%3Dea3a065fdd4d8f499acf275b49e5c791f5094918614567c44d9a44cad0fda9fcbb8ad9920eeb%26cur_album_id%3D1464601583634939905%26scene%3D189%23wechat_redirect)
*   [《会记笔记就会写作》完全版 - 下（完结篇）](https://link.zhihu.com/?target=https%3A//mp.weixin.qq.com/s%3F__biz%3DMzI1NTA4Nzk5Mw%3D%3D%26mid%3D2247484101%26idx%3D1%26sn%3D29886e6070b7168a29a13893ba3a21c4%26chksm%3Dea3a06d3dd4d8fc573b682c69a576cf99ad7a25e8109eb132ed8a31f8d3ecf81cd1dfa5d822c%26cur_album_id%3D1464601583634939905%26scene%3D189%23wechat_redirect)
*   [Youtube - Rethinking my PKM part 5: How do you organize your notes?](https://link.zhihu.com/?target=https%3A//www.youtube.com/watch%3Fv%3DAtdAAD47aQY%26t%3D724s)
*   [Youtube - Understanding note-taking](https://link.zhihu.com/?target=https%3A//www.youtube.com/watch%3Fv%3D-r6fnC5lVfE%26t%3D609s)
*   [Youtube - Building a Second Brain Book on a Page](https://link.zhihu.com/?target=https%3A//www.youtube.com/watch%3Fv%3D3i4CiImIYYA)

（完）

--

用 Obsidian 搭建 zk 工作流可以看这个视频:

[【中文字幕】Zettelkasten 笔记系统的搭建 | 手把手教学 | Zettelkasten in Obsidian_哔哩哔哩_bilibili](https://link.zhihu.com/?target=https%3A//www.bilibili.com/video/BV1Di4y1y7df/%3Fspm_id_from%3D333.337.search-card.all.click%26vd_source%3D03fea8b80d1864616f56adac92aa2617)

--

![](https://picx.zhimg.com/v2-6fcdd5f27ff15993fd6a9702a64cbc1d_l.jpg?source=1def8aca) 土嗨英语

更新一下：回答中提到的书，**英文样章可以在[作者官方网站](https://link.zhihu.com/?target=http%3A//takesmartnotes.com/download/725/)下载。样章由我翻译成中文，可以[在这里查看](https://link.zhihu.com/?target=https%3A//xiangji.ml/categories/How-to-Take-Smart-Notes/)。翻译得略仓促，如有错误欢迎指出。**

*   [聪明人怎么做笔记 1/4](https://zhuanlan.zhihu.com/p/136427760) 序言
*   [聪明人怎么做笔记 2/4](https://zhuanlan.zhihu.com/p/136909250) 第一章第一节
*   [聪明人怎么做笔记 3/4](https://zhuanlan.zhihu.com/p/136656539) 第一章第二节
*   [聪明人怎么做笔记 4/4](https://zhuanlan.zhihu.com/p/136909882) 第一章第三、四节

**5 月 20 日**再更新：很多人帮忙提了问题，所以稿子改了几次，但是知乎上一篇篇改太麻烦了，需要完整样章及翻译 PDF 的可以关注公众号之后发送 “**notes**” 获得下载链接。

“土嗨英语 “是我个人在业余时间做的一个关于英语学习的公众号，主要会发一些跟英语学习相关的文章，包括外刊文章翻译、时事新闻、美剧英语、听力等等。

![](https://picx.zhimg.com/v2-d2ad2b4a4d16b5a447b3339b742f8f7e_r.jpg?source=1def8aca)

* * *

`Zettelkasten` 是德语，英语是 `Slip-box`，翻译过来就是纸条盒、便签盒、卡片盒子、[卡片箱](https://www.zhihu.com/search?q=%E5%8D%A1%E7%89%87%E7%AE%B1&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A1120682799%7D)；简称卡片盒笔记法，或者**卡盒笔记法**。

这个笔记方法，是经过德国作家 Sönke Ahrens 的书 **[How to take smart notes](https://link.zhihu.com/?target=https%3A//book.douban.com/subject/30216624/)** 被广为人知的。这本书详细介绍了**[卡片盒笔记法](https://www.zhihu.com/search?q=%E5%8D%A1%E7%89%87%E7%9B%92%E7%AC%94%E8%AE%B0%E6%B3%95&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A1120682799%7D)**的前因后果和详细指南。作者**[个人网站](https://link.zhihu.com/?target=https%3A//takesmartnotes.com/)**上有英文样章，感兴趣可以前往下载阅读。样章的中文版我已经翻译完成，发给了作者，等确认授权之后分享出来。

### **总的来说**

总的来说，**卡片盒笔记法**不是一种做笔记的**技巧**，而是一个**流程**。而这个流程所做的事情就是：**把你感兴趣或者觉得自己将来会用到的笔记收集起来，然后用一种标准化的方式去处理这些笔记，建立笔记之间的联系，供你使用。**

卡片盒所遵循的几个原则：

1.  所有的思考都是在写的过程中发生的。写是唯一重要的事情。
2.  没有人的写作是从零开始的，你的笔记就是你做的准备。
3.  不要从上到下给你的笔记分类，而是从下往上（bottom-up）慢慢归纳出你的主题分类。
4.  工具不会改变你的流程，工具只是帮你把你的流程更顺利地运转下去。

### **用卡片盒方法做笔记写论文的流程**

1.  **做临时笔记**。把你大脑中的想法记下来，无论用什么方法都可以，这些笔记的目的就是让你想起你的那个想法。
2.  **做[文献笔记](https://www.zhihu.com/search?q=%E6%96%87%E7%8C%AE%E7%AC%94%E8%AE%B0&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A1120682799%7D)**。看书或者看到任何信息，觉得自己会用到或者和自己的思考相关时，笔记要剪短，要用自己的语言，不要复制粘贴。把这些笔记连同它的[参考文献](https://www.zhihu.com/search?q=%E5%8F%82%E8%80%83%E6%96%87%E7%8C%AE&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A1120682799%7D)放在一个地方（文献管理系统）。
3.  **做永久笔记**。浏览你在第一步和第二步所做的笔记，找出他们之间的联系，把这些笔记整合成新的永久的笔记。永久笔记才是你要放在你的**卡片盒**里的东西。这里要注意的点是：

每个笔记只包含一个想法，记笔记的时候要像写给别人看的一样：**用整句话，解释来源，参考了什么，用词尽量准确、简单、清晰**。做完之后，就可以把第一步的瞬时笔记扔掉，把第二步的文献笔记放进你的文献管理系统。你现在可以把它们忘掉了。所有重要的东西已经在你的卡片箱里了。  

1.  **把永久笔记放到你的卡片盒**。注意点：1）把新笔记放在相关笔记的旁边；2）添加相关笔记的链接；3）要确保你可以重新找到这条笔记（可以做一个[索引卡](https://www.zhihu.com/search?q=%E7%B4%A2%E5%BC%95%E5%8D%A1&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A1120682799%7D)，上面列出所有相关的笔记）。
2.  **构建主题**。从你的笔记中地构建出你写作的主题、研究的问题等等。
3.  **撰写草稿**。把你的笔记转写成一个文章的草稿。
4.  **修改润色**。重读草稿，润色。重复。

这个流程是根据书中第二章 `2.1 Writing a Paper Step by Step`概括。详细过程见原文。

### **遵循卡片盒笔记原则的软件**

1.  **[Roam Research](https://link.zhihu.com/?target=https%3A//roamresearch.com/)**，强烈推荐。2019 年年底推出的一款革命性的软件，国外好多人推荐。国内速度比较慢，科学上网可以快一些。现在还在公测阶段，过段时间可能会收费。
2.  **[Zettlr](https://link.zhihu.com/?target=https%3A//www.zettlr.com/download)**，全平台，免费。
3.  其他插件类应用，包括 `VS Code` , `Vim`, `Atom`, `SublimeText` 都有相关插件，当然是免费的。

### **参考文章**

*   **[Zettelkasten 官网文章集合](https://link.zhihu.com/?target=https%3A//zettelkasten.de/posts/overview/)**
*   **[Wtf is Zettelkasten](https://link.zhihu.com/?target=https%3A//www.zettlr.com/post/what-is-a-zettelkasten)**
*   **[如何高效实践卡片式写作？](https://link.zhihu.com/?target=https%3A//sspai.com/post/59109)**
*   **[What is Zettelkasten](https://link.zhihu.com/?target=https%3A//www.reddit.com/r/Zettelkasten/comments/b566a4/what_is_a_zettelkasten/)** 科学上网

版权所有，转载请注明出处。

![](https://pic1.zhimg.com/cebb9f6e2846e17b9928eca98ef77d64_l.jpg?source=1def8aca)日行一善

用了快 2 年的[卡片法](https://www.zhihu.com/search?q=%E5%8D%A1%E7%89%87%E6%B3%95&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A3288691316%7D) + 双链接，看到很多人写的好复杂哦，实践起来这玩意其实没有这么难，而且也没有这么神奇。

介绍
--

说到卡片盒就会说到[卢曼](https://www.zhihu.com/search?q=%E5%8D%A2%E6%9B%BC&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A3288691316%7D)，出了一大堆书的德国人，然后就是巴拉巴拉。其实他的这套方法已经过时了，因为当时资料过于匮乏，他所有的资料加起来就是几十本书这样子，然后写文章呢，这么多书引用起来又记不住，所以就要拆开。

也就是大家上学时期读书把知识转化为笔记这样，但这么多书记录完的笔记 + 感想又是一大堆内容，等于重新写了几本书这样。

卢曼关注的很多社会和法律的知识，知识与知识之间互相关联，急需一种新的方法，当时又没有电脑，所以卢曼就用卡片法来模拟现代电脑上的笔记软件。

例如下图就是一张卡片，以日期为编号，然后对这个卡片的大致介绍，其后是详细内容，如果牵扯到别的卡片了，就写下卡片的编号，最后是参考。

![](https://pic1.zhimg.com/v2-b0850e30ce5044afea1ca972fee635fc_r.jpg?source=1def8aca)

如今来看这套卡片法真的是很折磨人，需要人工去根据编号到处找卡片，如果你没有一个任劳任怨的妻子千万不要尝试。

这里说明：

*   这套方法是无奈之举，效率不如现在任何一款带搜索的[笔记软件](https://www.zhihu.com/search?q=%E7%AC%94%E8%AE%B0%E8%BD%AF%E4%BB%B6&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A3288691316%7D)
*   他有妻子全天候的整理这些卡片
*   卢曼的卡片全是社会相关内容，非常的集中

现代卡片法
-----

卡片法的核心就是对内容进行拆分，内容之间互相关联，那在笔记上的应用就是使用双链接进行互相关联。直接以名称命名即可，方便后续搜索。

![](https://picx.zhimg.com/v2-244deb64c9e12b54c9ce17ca74f7d118_r.jpg?source=1def8aca)

这里也不需要什么标签、目录文件夹结构等等，直接有需要搜索即可，搜索法律，那法律中关联了《公证》，直接顺着全都可以搜到了。卡片通过双链接就构建成了一个动态的[目录结构](https://www.zhihu.com/search?q=%E7%9B%AE%E5%BD%95%E7%BB%93%E6%9E%84&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A3288691316%7D)，也是有顺序的。

![](https://pic1.zhimg.com/v2-4af7a5180f2a71b4628f45c76269331a_r.jpg?source=1def8aca)

其实传统的目录结构对人的精力是一种束缚！因为有目录结构，就总会想看看自己之前写的文档，会漫无目的的[填充维护](https://www.zhihu.com/search?q=%E5%A1%AB%E5%85%85%E7%BB%B4%E6%8A%A4&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A3288691316%7D)。而在新的卡片法里除非你需要用到笔记了，才会进行搜索，并不会漫无目的的到处乱逛。

我现在用笔记软件的时间比之前少了很多，但产出和记录多了很多。11 月份刚过去 14 天，就已经写了快 60 篇了，当然在[卡片时代](https://www.zhihu.com/search?q=%E5%8D%A1%E7%89%87%E6%97%B6%E4%BB%A3&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A3288691316%7D)每一篇内容都很少，但确实知识的输入输出很丝滑。

![](https://picx.zhimg.com/v2-9b62b27567d2eccd53ac27ba61f826e2_r.jpg?source=1def8aca)

我的笔记很多涉及隐私，所以就一直在用本地笔记，尝试了 OB、logseq、思源等等后最终选择了【[思源笔记](https://www.zhihu.com/search?q=%E6%80%9D%E6%BA%90%E7%AC%94%E8%AE%B0&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A3288691316%7D)】，毕竟我想把精力花在进步上，而不是折腾[插件](https://www.zhihu.com/search?q=%E6%8F%92%E4%BB%B6&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A3288691316%7D)上面。

如果对双链接感兴趣，可以看看大佬写的无压笔记法，目前我也是用了一年，可以说是[终极笔记](https://www.zhihu.com/search?q=%E7%BB%88%E6%9E%81%E7%AC%94%E8%AE%B0&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A3288691316%7D)法了，是卡片法的进一步诠释。

[https://www.yuque.com/deerain/gannbs/ffqk2e](https://link.zhihu.com/?target=https%3A//www.yuque.com/deerain/gannbs/ffqk2e)

![](https://pic1.zhimg.com/v2-1968207e8c106b452041b831d5dabd22_l.jpg?source=1def8aca)flomo 浮墨笔记在 2022 年初的计划中，我们提到了[深度年（Depth Year）](https://link.zhihu.com/?target=http%3A//mp.weixin.qq.com/s%3F__biz%3DMzI0MDA3MjQ2Mg%3D%3D%26mid%3D2247485664%26idx%3D1%26sn%3D7b9298974f58c87a52e33ae2ba1881a7%26chksm%3De9212a81de56a397dc668faaab98bcbf717ea806389876132430643c40cd0cd16e97ecf1b2c1%26scene%3D21%23wechat_redirect)的概念，希望能用一年时间把一些已有的东西深入打磨。在 Android 完成离线，iOS 完成编辑器重构后，[flomo 101](https://www.zhihu.com/search?q=flomo%20101&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A2500356443%7D)（也就是 help.flomoapp.com[1] ）作为 flomo 产品的重要组成部分，也开始了其深度修订计划。  
希望利用这次修订计划，弥补之前内容中的缺陷和遗憾。预计每篇修订幅度在 20% ~ 60% 左右，即使曾经看过的同学，再看一遍相信也会有收获。  
你可以在文末的二维码看到修订的大纲[计划表](https://www.zhihu.com/search?q=%E8%AE%A1%E5%88%92%E8%A1%A8&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A2500356443%7D)，如果对于某些话题有兴趣，也可以点击阅读全文加群交流。![](https://picx.zhimg.com/v2-b4527f44a110935fb17a398126d6b827_r.jpg?source=1def8aca)

在 flomo 101 中，我们推荐的「卡片笔记」法，最初是由德国社会学家[尼克拉斯 · 卢曼](https://www.zhihu.com/search?q=%E5%B0%BC%E5%85%8B%E6%8B%89%E6%96%AF%20%C2%B7%20%E5%8D%A2%E6%9B%BC&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A2500356443%7D)发明，他称其为「Zettelkasten」。

卢曼曾经是一位普通公务员，但他凭借这套方法在不到一年的时间里，就完成了博士论文和定职论文，成为了[比勒菲尔德大学](https://www.zhihu.com/search?q=%E6%AF%94%E5%8B%92%E8%8F%B2%E5%B0%94%E5%BE%B7%E5%A4%A7%E5%AD%A6&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A2500356443%7D)的社会学教授。在这个新职位上他填写过一份关于他研究内容的问卷，回答是这样的：「项目：社会学理论。期限：30 年。成本：零。」

![](https://pic1.zhimg.com/v2-c6c40de55f8d85be88c6eb8008a18ae2_r.jpg?source=1def8aca)

从本质上讲，卡片笔记法不是一种「技巧」，而是一个「流程」，一种**存储和组织知识、扩展记忆以及生成新连接和想法的方法**。简单来说就是，把你感兴趣或者觉得自己未来会用到的知识收集起来；然后用一种标准化的方法处理这些笔记，确保[颗粒度](https://www.zhihu.com/search?q=%E9%A2%97%E7%B2%92%E5%BA%A6&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A2500356443%7D)和标准统一；最后建立笔记之间的联系，供日后有需要的时候检索使用。

![](https://pic1.zhimg.com/v2-a5bdfb0608428d9a963901dce90a8ee3_r.jpg?source=1def8aca)

这里关键的思想是，**为未来的你做笔记**。所以重要的不是文采，而是线索，所以记录翔实、用词明确、索引清晰最重要。

卢曼使用这套方法，30 年间积累了 90000 个知识卡片，出版了 58 本著作和数百篇文章（不包括译本），许多都成为各领域的经典之作。

**[卡片笔记](https://www.zhihu.com/search?q=%E5%8D%A1%E7%89%87%E7%AC%94%E8%AE%B0&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A2500356443%7D)背后的原理**
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

为何卢曼要把一篇篇记录拆成一堆小卡片呢？**这和我们大脑的思考方式有关。**

「块 」是心理学中的一个专业术语，意思是指任何已经熟悉并在记忆的索引中占有一席之地的知识单位。既然在索引中占有一席之地，那么「块 」就是你在专业领域中能认识到的任何东西。

说汉语的人都是汉语专家 —— 因为我们已经储存了超过 10 万个熟悉的「块」，这些块被称为词语。当我们在文章中看到它们时，我们会认出它们并从记忆中检索它们的含义。

所以我们的学习，并不是一上来就掌握了整个系统，**而是不断地积累相关的知识点，然后将其之间建立链接而达成的。**

![](https://pic1.zhimg.com/v2-2273ae1fc21129786d08804a59cff31e_r.jpg?source=1def8aca)

诺奖得主[西蒙 · 赫伯特](https://www.zhihu.com/search?q=%E8%A5%BF%E8%92%99%20%C2%B7%20%E8%B5%AB%E4%BC%AF%E7%89%B9&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A2500356443%7D)（Herbert A. Simon）认为，专家和普通人之间的差异，就在于其所在领域掌握的这些块的数量，专家至少拥有大约 5 万到 10 万个所在领域中 「块（Chunk） 」组成的[知识网](https://www.zhihu.com/search?q=%E7%9F%A5%E8%AF%86%E7%BD%91&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A2500356443%7D)。

卢曼的「[卡片笔记法](https://www.zhihu.com/search?q=%E5%8D%A1%E7%89%87%E7%AC%94%E8%AE%B0%E6%B3%95&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A2500356443%7D)」和西蒙赫伯特的「块」理论，在这里产生了交集。

所以虽然一张卡片看起来过于孱弱，但却是知识的最小单位，需要长期积累。而这世界上所有复杂的结构，都是源自于最小单位之间关系的演变。蚂蚁如此，蜂群如此，城市如此，知识的宫殿亦是如此。

**卡片如何构成**
----------

在原始的 Zettelkasten 方法中，由卡片盒和卡片两部分组成。盒子用来装卡片，而每张卡片上都有一条信息，包括一个索引和信息的来源。

在大多数情况下，这些信息都很简短，而卡片的大小有助于保持它的简洁。当然图片和其他形式的信息也可以使用。

![](https://picx.zhimg.com/v2-50fbae6c1e3823a266a3b31602e56105_r.jpg?source=1def8aca)

我们再来看看卢曼的卡片和 flomo 卡片的相同之处：

**1. [唯一识别符](https://www.zhihu.com/search?q=%E5%94%AF%E4%B8%80%E8%AF%86%E5%88%AB%E7%AC%A6&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A2500356443%7D)（unique identifier）。** 这给了你的卡片一个唯一的编号。

**2. 卡片的正文（body）。** 这是你写下你想要获取的东西的地方，即知识的片段或自己的思考。

**3. [参考文献](https://www.zhihu.com/search?q=%E5%8F%82%E8%80%83%E6%96%87%E7%8C%AE&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A2500356443%7D)（References）。** 在每个卡片的底部，放上引用知识的来源。

![](https://picx.zhimg.com/v2-5dffe1ae0235797e4dbc144ee8d76a90_r.jpg?source=1def8aca)

**卡片正文是最重要的部分，请务必使用你自己的语言来描述你想记录的东西，而不仅仅是复制黏贴** —— 这样只是知识的搬运工，你没有在这个过程中为知识增值。

也不必担心理解的不够透彻，因为还要可以在卡片上标注出处，方便将来查找源信息。

另外需要提到的是，**卡片笔记是一套流程，不要试图在 flomo 中完成所有的工作**，就像卢曼不会直接把卡片贴在论文纸上就发表一样。flomo 能完成的是卡片的记录、分类和连接，而更多的创造，则需要联合其他工具，以及个人的思考一起来使用。

关于如何分类，后续会单独开一期讲，此处先不展开。

**写卡片的原则**
----------

在《卡片笔记写作法》一书中，提到有下面这么几种笔记需要记录：**流水笔记、文献笔记、永久性笔记、项目笔记。**

详细的介绍可以在[微信读书](https://www.zhihu.com/search?q=%E5%BE%AE%E4%BF%A1%E8%AF%BB%E4%B9%A6&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A2500356443%7D)、得到 App 找到《[卡片笔记写作法](https://www.zhihu.com/search?q=%E5%8D%A1%E7%89%87%E7%AC%94%E8%AE%B0%E5%86%99%E4%BD%9C%E6%B3%95&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A2500356443%7D)》这本书查看。

![](https://picx.zhimg.com/v2-897e53aad4cb909f9feba234edd340cc_r.jpg?source=1def8aca)

但这里想提供另一个视角：**不去照搬卢曼的概念，而是理解他的笔记原则。**因为我们和他所处的时代、环境不同，尽信书不如无书。

所以我们可以试着将卢曼的一些笔记原则提取出来：

**1. 每张卡片只记一件事。**用清晰、简洁的方式写作，不求文采出众，但求记录翔实、索引明确。这样能有效地了解到事情的全部，也方便和其他内容、主题建立联系。太宽泛很难聚焦，太零碎会让链接模糊。

**2. 不要复制信息。**你必须用你自己的话写，这个刻意的过程能让我们更好地思考。没有任何创造是一蹴而就的，都是经历了大量的积累和缓慢的思考。

**3. 用概念来分类。**不要用作者、书、事件、项目、主题来分类。这是为了让旧概念和新概念交融，如果每个卡片都被割裂到一本本书上、视频上，将来查找就会困难，而且和其他笔记的链接也会减弱。

**4. 不要考虑分享。**写卡片是为自己积累，没必要为了向别人炫耀而增加太多无用的修辞和背景描述，而炫耀之心也会扭曲你本来的学习目的。

**5. 控制记录数量。**积累知识是一场长跑，关键的不是第一刻跑得多快，而是在于能以怎样的速度持久，所以每天能在 7 - 9 张有价值的卡片即可。

不要纠结是否和书中一样。因为在卢曼之前，并没有这种方法；而在卢曼之后，也不一定非得只有这种方法。重要的是在记录的过程中，自己认真地思考过。

![](https://picx.zhimg.com/v2-c60166bfcfe9867fcd5697c92b9f0459_r.jpg?source=1def8aca)

至于在卡片上记录什么，这个问题只有你自己能回答，因为没有任何人能代替你来思考这个问题。（关于这一点，可以点击查看上期内容：**[如何寻找自己的领域](https://link.zhihu.com/?target=http%3A//mp.weixin.qq.com/s%3F__biz%3DMzI0MDA3MjQ2Mg%3D%3D%26mid%3D2247485941%26idx%3D1%26sn%3D28e92a5f06fb1883e95d6dc14db63d5d%26chksm%3De9212b94de56a2824cd58114927e2011d71e89b85358bf973c3ba50449ca0eb398b4fa254d55%26scene%3D21%23wechat_redirect)**）

**持续写卡片的益处**
------------

很多时候，当我们面对一张白纸，或者一个闪烁着光标的空白编辑器，试图从脑中迸发出一个完全全新的东西，在僵持几分钟后，会发现这是徒劳的。随着时间的流逝我们会慢慢告诉自己：自己似乎没有那么多的创造力 。

**而事实上，没有人真正从零开始创作。**任何人想出来的任何东西，都必须来自于之前的经验、研究或其他洞察。

![](https://pic1.zhimg.com/v2-e417afa624a658cee0d92206309d29c6_r.jpg?source=1def8aca)

对所有的知识工作者来说，在我们决定要写下来某些东西之前，你必须下功夫研究并积累。换句话说，**你应该在创作之前就进行研究，这样你在创作之前，就有数周、甚至数月的积累供将来参考**。

这也是我们设计 flomo 的初衷。

许多人并没有养成记录卡片的习惯，当他们需要创作的时候，往往无法追根溯源，所以他们的创作失败率很高。他们要么总是把一切归零重新开始 —— 这很容易失败；要么总是回溯自己最熟悉的部分 —— 这很无聊。

**小结**
------

最后用卢曼的一句话作为结束，希望你也在坚持一段时间能有这样的共鸣：

**当你发现从没有足够的东西可写，变成有太多东西可写时，你就会发现这些习惯带来的变化。**

持续不断记录，意义自然浮现。

![](https://pic1.zhimg.com/v2-58fe1465a818ad629659a2c5eb32cd23_r.jpg?source=1def8aca)

**查看更多 flomo 101 的内容**
----------------------

### **References**

`[1]` Gapingvoid.com: _[https://www.gapingvoid.com/blog/2019/03/05/want-to-know-how-to-turn-change-into-a-movement/](https://link.zhihu.com/?target=https%3A//www.gapingvoid.com/blog/2019/03/05/want-to-know-how-to-turn-change-into-a-movement/)_

`[2]` _[https://mp.weixin.qq.com/s/d3ZiKQplFBiUf5KIC22lRg](https://link.zhihu.com/?target=https%3A//mp.weixin.qq.com/s/d3ZiKQplFBiUf5KIC22lRg)_

![](https://picx.zhimg.com/v2-bf62ec87bc4c662c4a96638d2fb16c98_l.jpg?source=1def8aca) 组件世界 (NotionPet)​

[知识管理方法论](https://www.zhihu.com/search?q=%E7%9F%A5%E8%AF%86%E7%AE%A1%E7%90%86%E6%96%B9%E6%B3%95%E8%AE%BA&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A3306659480%7D) · 合辑 ⬇️
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

• [当我们在说「个人知识体系」时，究竟在说什么？](https://zhuanlan.zhihu.com/p/667586048)

• [知识管理和生产力工具的「安慰剂」？](https://zhuanlan.zhihu.com/p/667613576)

• [「信息囤积症」的速救指南](https://zhuanlan.zhihu.com/p/667585307)

• [高效工作与生活，学会和「碎片化」妥协](https://zhuanlan.zhihu.com/p/667583117)

• [个人推荐以及不推荐的「思维导图」使用方法](https://zhuanlan.zhihu.com/p/667585115)

• [极简笔记法——如何通过 「实体笔记本规划」 让我们重获人生的控制权](https://zhuanlan.zhihu.com/p/664963076)

• [什么是 Zettelkasten 笔记法？传说中「卡片盒子法」？](https://zhuanlan.zhihu.com/p/667612588)

• [如何做一个行之有效「待办事项」清单](https://zhuanlan.zhihu.com/p/667615356)

• [在过去 12 年里，我的生产力 App 就是一个简单的 txt 文件](https://zhuanlan.zhihu.com/p/667610992)

• [关于「多层日历」工具的思考](https://zhuanlan.zhihu.com/p/668735305)

• [是时候抛弃对「个人项目管理」的盲目崇拜了](https://zhuanlan.zhihu.com/p/667614035)

[第二大脑方法论 - 从入门到精通：如何打造高效易用、提升自我的终极笔记管理系统？](https://zhuanlan.zhihu.com/p/671768275)![](https://pic1.zhimg.com/v2-e5e1647a4f6caba6f3f6a8a9473c384f_r.jpg?source=1def8aca)

[知识管理系统](https://www.zhihu.com/search?q=%E7%9F%A5%E8%AF%86%E7%AE%A1%E7%90%86%E7%B3%BB%E7%BB%9F&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A3306659480%7D)：什么是 Zettelkasten 笔记法？传说中「卡片盒子法」？【译】⬇️
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

转载声明
----

**转载来源**：效率火箭 博客

**原作者**：[火箭君](https://www.zhihu.com/search?q=%E7%81%AB%E7%AE%AD%E5%90%9B&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A3306659480%7D)。

**原文链接**：[效率火箭](https://link.zhihu.com/?target=https%3A//xlrocket.blog/)

**原文标题**：【译】什么是 Zettelkasten 笔记法？传说中「卡片盒子法」？

**博客介绍**：高效搞定事情，尽情享受生活。长期分享各种**前沿生产力工具的专业评测**和**效率管理的深度思考**。

本文内容摘译自 Reddit r/zettelkasten 频道 以及 [http://zettelkasten.de](https://link.zhihu.com/?target=http%3A//zettelkasten.de) 网站。

封面来自：Bild mit Genehmigung des Autors

在开始正文之前，我先要说明一下：

（1）任何方法的效果因人而异，每个人应该寻求符合自己的方法组合，而不是照搬某一个所谓 张三李四或[卢曼](https://www.zhihu.com/search?q=%E5%8D%A2%E6%9B%BC&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A3306659480%7D) 大神的方法。方法要为目的服务，我们不是方法的奴隶。

（2）Zettelkasten 可以是具体的方法，也可以是抽象的体系，可以举一反三运用在更多领域，但还是那句话，不要迷信，要独立思考。

（3）Zettelkasten 的精髓，可以几句话讲完的，没有必要出一本书，也不适合成为一种营销「噱头」，更不应该成为某种「鄙视链」的一部分。

![data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' width='302' height='187'></svg>](data:image/svg+xml;utf8,<svg%20xmlns='http://www.w3.org/2000/svg'%20width='302'%20height='187'></svg>)

以下正文：

What is Zettelkasten？
---------------------

Zettelkasten 是德语，意为 「活动抽屉」 或「[卡片索引盒](https://www.zhihu.com/search?q=%E5%8D%A1%E7%89%87%E7%B4%A2%E5%BC%95%E7%9B%92&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A3306659480%7D)」。

（火箭君注：其实就是我们以前的提到的 传统图书馆分类法 中的 分类抽屉。这套体系早就被运用在信息管理的领域中）

从本质上讲，Zettelkasten 是一种记录和整理的方法，用来：

*   存储和组织信息
*   扩展我们的记忆
*   产生新的联系和想法

要实现这几点， 我们可以通过 纸笔也可以通过 App，或者任何其它记录手段。

![](https://pic1.zhimg.com/v2-57067f2d116b9a61c807659569692f93_r.jpg?source=1def8aca)

一般 **Zettelkasten 方法**包括：

*   编号：首先，需要编写一个带有唯一 ID 名称 / 编号的 「容器」（可以是 纸质卡片，[云笔记](https://www.zhihu.com/search?q=%E4%BA%91%E7%AC%94%E8%AE%B0&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A3306659480%7D)，电子文件）。一般建议使用时间戳作为 ID。时间戳不仅唯一，而且以后能看到信息的创建时间。
*   原子性：尽量遵守「原子性」的理念，一个 「容器」 就存放一个想法，不要杂糅。如果有多个想法，尽量分别录入到多个「容器」里（例如：多张纸质卡片）
*   链接：现在我们有了很多「原子级别」的「容器」，每当我们添加新的内容时，可以考虑和已有的哪些「容器」进行关联。在回顾旧的内容时，也可以补充这种[链接](https://www.zhihu.com/search?q=%E9%93%BE%E6%8E%A5&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A3306659480%7D)。链接帮助我们在不同内容间跳转，串起一系列上下文和场景。链接也能启发我们找到新的思路，产生更多的关联。（类似电子邮件里面的 [threads](https://www.zhihu.com/search?q=threads&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A3306659480%7D)）
*   跳转：其实就是 Wiki 那样的 超链接。大多是 App 和 [电子文件](https://www.zhihu.com/search?q=%E7%94%B5%E5%AD%90%E6%96%87%E4%BB%B6&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A3306659480%7D) 都以不同形式支持跳转，例如有些笔记软件 用 #/@/[[ 作为跳转链接开始的标志， 后面会填写一个标识，比如我们开头说的 ID。应用程序可以更方便的实现跳转，而纸笔版本会比较吃力，需要自己去翻出关联的「容器」；比如：从另一个抽屉盒子里，翻出关联的[纸质卡片](https://www.zhihu.com/search?q=%E7%BA%B8%E8%B4%A8%E5%8D%A1%E7%89%87&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A3306659480%7D)。
*   结构： 这种方法的一个关键思想是，当我们添加内容时，让类别或主题自然会有机地出现。如果预先设置了结构、类别或层次，可能反而会限制我们在看似不相关的想法之间建立联系的能力。
*   [元内容](https://www.zhihu.com/search?q=%E5%85%83%E5%86%85%E5%AE%B9&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A3306659480%7D)：当我们发现更多类似主题时，可以创建引用相关内容的所有 id 形成「元内容」（关于内容的内容，例如：索引，目录，分类列表）。
*   永久笔记：为未来的自己写下「永久笔记」，就像写信给别人一样。尽量用清晰、简洁的方式撰写，可以把它想象成一篇维基文章。「永久笔记」的目的是让将来的自己能够理解现在的想法。
*   让自己理解：不要只复制粘贴信息。必须用自己的语言，自己能理解的方式来写。每次你阅读原始文本时，必须以自己理解的方式处理这些信息。所以，最好首先写下自己对这些信息的理解，将它保存在「容器」里。
*   引用出处：虽然鼓励用自己的文字记录，但也鼓励在注释中引用原文。对于那些进行学术写作的人来说，特别必要。因为行业要求必须列出引用来源。（火箭君：一般来说有高度引用要求的话，会使用一个单独的引用管理器，Endnote，Note Express 之类，而像 Zettlr 这样的 App 甚至可以让你与 Zotero 集成。）

最后
--

火箭君觉得：Zettelkasten 并不是万灵药，但是可以很有效的对抗信息「无序化」以及「[记忆流失](https://www.zhihu.com/search?q=%E8%AE%B0%E5%BF%86%E6%B5%81%E5%A4%B1&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A3306659480%7D)」，通过链接还可能（只是可能）产生一些新的想法和见解。最后，通过多次回顾，自然会呈现出一些 类别，频道，目录， 这些不是预设的而是动态形成的。我们也可以据此进行归类，将一些「容器」（卡片）归到某些更大的容器（抽屉盒子）里。我们可以想象：图书馆的分类抽屉就是这样来的。这种方式源自古老的逻辑技巧：归纳法。

最后，为什么不能就把笔记文字图片放在一个电脑文件夹（或物理的活页夹）里面呢？当然可以，很多组织单位都是这样保存归档资料的。可能会丧失一些 Zettelkasten 带来的好处，但也有其它好处，因为那些文件夹几乎从来没有人会去翻阅或回顾，不会有人基于这些材料形成新想法，如果不是审计需要，大家很想烧掉这些文件。那么，自然也就没有必要折腾各种链接和索引了。这就是手段为目的服务。

![](https://picx.zhimg.com/v2-845bb4f74d967e1fa002a8d0ff037bc8_r.jpg?source=1def8aca)

顺便一说 [http://zettelkasten.de](https://link.zhihu.com/?target=http%3A//zettelkasten.de) 是一个不错的海外介绍 Zettelkasten 的网站，收集了很多衍生的用法，有兴趣可以一看（不用番羽哦）。

注释：以上是作者全文。

工具箱

除了**待办、日历**等工具，最常见的**效率工具**可能便是可以作为**[知识库](https://www.zhihu.com/search?q=%E7%9F%A5%E8%AF%86%E5%BA%93&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A3306659480%7D)**的**编辑器 · 文档 · 笔记**软件。

已有选择很多，如何选择合适的工具？

[常见笔记软件、文档编辑器的数据导入和导出策略](https://zhuanlan.zhihu.com/p/602551207)

目前，离线[本地文档](https://www.zhihu.com/search?q=%E6%9C%AC%E5%9C%B0%E6%96%87%E6%A1%A3&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A3306659480%7D)、在线文档的主要代表便是 Obsidian、Notion.

Obsidian
--------

**访问**：➡️ [Obsidian](https://link.zhihu.com/?target=https%3A//obsidian.md/)

![](https://picx.zhimg.com/v2-e57a9a286013476d7c58e6a94ce951ee_r.jpg?source=1def8aca)[Obsidian 使用教程：如何高效使用 Obsidian ？快来看看专家用户基于 Obsidian 的开箱即用库](https://zhuanlan.zhihu.com/p/619967023)[笔记软件 Obsidian 使用教程 & 学习资源汇总](https://zhuanlan.zhihu.com/p/619960525)

Notion
------

**访问**：➡️ [Notion](https://link.zhihu.com/?target=https%3A//www.notion.so/)

![](https://picx.zhimg.com/v2-72ba705a493aa8fb5a2f34cbb1caf74c_r.jpg?source=1def8aca)

具体参见

[Notion 使用教程 & 资源：从入门到精通](https://zhuanlan.zhihu.com/p/614078488)![](https://picx.zhimg.com/v2-4f92305228ac62fe4f9437e668cb4687_r.jpg?source=1def8aca)![](https://picx.zhimg.com/v2-e662666b120d50493f9189a3805cb883_r.jpg?source=1def8aca)

### Notion 免费开源替代

这类工具理念很好，不过多数完成度不是很高。建议测试，看是否能满足自己的需求。

[支持本地化部署方案、免费开源的 Notion 类软件](https://zhuanlan.zhihu.com/p/567674087)![](https://picx.zhimg.com/v2-7c5230211cbb4a60e9416826e6b93ca1_r.jpg?source=1def8aca)

### 国产替代 FlowUs

核心功能：**块编辑器 + 多维表格。**

其他功能 **⬇️**

![](https://picx.zhimg.com/v2-4b3d56fbfa2039f7b18d6c90df340fec_r.jpg?source=1def8aca)![](https://picx.zhimg.com/v2-f5467d52ce631df27c7085bdb71ae69b_r.jpg?source=1def8aca)

**➡️ 访问**：[FlowUs 息流 - 新一代生产力工具](https://link.zhihu.com/?target=https%3A//flowus.cn/product) ⬅️

具体介绍如下 [FlowUs 使用教程合辑：从入门到精通](https://zhuanlan.zhihu.com/p/599727106)

最后，关于组件世界 ⬇️

组件世界 Widget Store
-----------------

一个服务于 **Notion 类**产品、**双链笔记类**产品等文本编辑器的可嵌入式小组件库。

![](https://pic1.zhimg.com/v2-3e9ac1ca237f3097d4fc20a43133a09a_r.jpg?source=1def8aca)![](https://pic1.zhimg.com/v2-c9d312f967269125af4c0862b225efa3_r.jpg?source=1def8aca)

### 访问 ⬇️

[组件世界 / WidgetStore](https://link.zhihu.com/?target=https%3A//cn.widgetstore.net/%23/home)