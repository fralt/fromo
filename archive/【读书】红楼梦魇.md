

## 介绍
《红楼梦魇》是[张爱玲](/archive/张爱玲)的一部重要作品。 1966年张爱玲定居美国，至1995年离世，期间以十年时间研究《红楼梦》，此书正是其晚年多年研究的结晶。书中共收入其七篇研究文章，包括《〈红楼梦〉未完》，《〈红楼梦〉插曲之一》，《初详〈红楼梦〉》，《二详〈红楼梦〉》，《三详〈红楼梦〉》，《四详〈红楼梦〉》，《五详〈红楼梦〉》。

## 笔记

一、创作前期的故事线。

张爱玲从线索上概括了此书还未称为红楼梦之前的，散乱的故事。

[甄士隐](https://www.zhihu.com/search?q=%E7%94%84%E5%A3%AB%E9%9A%90&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A1611895531%7D)的简短故事线，舒心生活——家庭变故——看透世间——出家而去，是作者早期创作的独立[短篇小说](https://www.zhihu.com/search?q=%E7%9F%AD%E7%AF%87%E5%B0%8F%E8%AF%B4&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A1611895531%7D)。

贾雨村的故事线，穷人做官——考取功名——官场判案——结交权贵——助纣为虐——祸连富家——遭受惩罚，是一篇独立的小说。

宝玉的故事，青梗峰下相会——投身贵族——贵族之间的事——一夜崩塌——男主出家——整书闭环的[中篇小说](https://www.zhihu.com/search?q=%E4%B8%AD%E7%AF%87%E5%B0%8F%E8%AF%B4&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A1611895531%7D)。

因这个故事框架利于糅合其他主线，故选取此线作为主线。

风月宝鉴的故事线，大户里表面的纯净——内部礼教的崩塌——各种的故事（秦氏、姐弟、王熙凤、[贾瑞](https://www.zhihu.com/search?q=%E8%B4%BE%E7%91%9E&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A1611895531%7D)、尤氏等）——沉迷而惨淡的结局，一篇主旨的小说。

林黛玉的故事线，父母双亡的贵族少女投奔母系的贵族家庭——被贵族的嫡子爱上——二人在礼教下的爱情——少女病死，同期家庭败亡——男主角履行诺言而出家，这样的爱情小说。

小红的故事线，一个穷丫头与主人的好感——被主人母亲逐而又被另一主赏识——与贵族家庭成员恋爱与私奔——贵族家庭的崩塌——[善助旧主](https://www.zhihu.com/search?q=%E5%96%84%E5%8A%A9%E6%97%A7%E4%B8%BB&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A1611895531%7D)，这样一个小说。

男主角仆人的故事线，袭人——早期照顾——情感交互多，至于性关系——花家赎人未得——母亲同意，暗中许配——后期家庭败落，花家赎人成功——嫁给男主角一位挚友（或随便什么人）——男主败落重建相逢（后面至少三种结局：现实式——袭人不理男主角；袭人与男主双双哭死；袭人暗自照顾宝玉至出家）

男主角仆人的故事线，金钏儿——房内丫头，性格活泼好爽——与主角日渐升温——有一次主角的冲动反礼行为被撞见——主角母亲反怪金钏儿下流——金钏儿投井自杀

这个故事线之后，因为主角身边性格活泼的丫头还需要一个，所以只好再创造出晴雯这个形象，再把[金钏儿](https://www.zhihu.com/search?q=%E9%87%91%E9%92%8F%E5%84%BF&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A1611895531%7D)放到王夫人家。然而晴雯的创造必然削弱宝玉仆人三角中另一个，也就是重要的麝月的形象，晴雯甚至与麝月分享乃至夺去了麝[月的故事](https://www.zhihu.com/search?q=%E6%9C%88%E7%9A%84%E6%95%85%E4%BA%8B&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A1611895531%7D)。[麝月](https://www.zhihu.com/search?q=%E9%BA%9D%E6%9C%88&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A1611895531%7D)的原型是雪芹故时的仆人做他的妾，跟随了他一辈子的，重要性不言而喻。但作为小说的创作，作者不可能写原本的 “历史”，而是要创造新的想象的结局。


秦氏自缢这段最早期的原型故事如何改变的时间轴：第四次增删，畸笏与脂主迫使作者必须更改前后章回。那么宝玉初入太虚幻境的第 25 回（他与凤姐被咒），就得挪到前面。那么[太虚幻境](https://www.zhihu.com/search?q=%E5%A4%AA%E8%99%9A%E5%B9%BB%E5%A2%83&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A1611895531%7D)与秦氏的关联，就必须用什么东西联系在一起，那就是乳名。连带的，故事中期的宝玉袭人的初次性体验，必须往前挪。那么后来黛玉的一句嫂子，就离原来较近的时间轴，差了很远。那为什么作者两位亲戚要让作者改秦氏呢？因为秦氏在改之前，对王熙凤说了一番警示之言。让这二人看出秦氏的美好。

但这是秦氏的话吗？不是，是作者安给秦氏的。作者将[元春之死](https://www.zhihu.com/search?q=%E5%85%83%E6%98%A5%E4%B9%8B%E6%AD%BB&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A1611895531%7D)托梦给王熙凤的对话，放在了秦氏这里。也就是说元春之死，是有的，也就是小说第 54、55 回。为什么之前把元春之死放在小说中，后面才要放在小说之后？因为，如果是放在中间，尔后[宁府](https://www.zhihu.com/search?q=%E5%AE%81%E5%BA%9C&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A1611895531%7D)被废封号，荣府连累败落。那就是皇帝不念旧情。文字狱的年代，这样的作品结构是不可能生存的。于是，写好的元春之死，必须改为另一王妃之死，挪到贾府败落之后，然后元春再因为自己父母守难，感应而病死。

作者第三次删改前——元春中期之死——宁府与贾雨村私挪甄家非法财物——[宁国公](https://www.zhihu.com/search?q=%E5%AE%81%E5%9B%BD%E5%85%AC&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A1611895531%7D)世袭被废，荣府连累衰败——贾环世袭，逼迫宝玉离去——（宝玉的数种结局）的大故事线，必然变动。

第三次删改：元春之死挪到了两府之后，但那段告诫之话必须留下，也就是挪到了秦氏之言。

第四次删改：秦氏淫丧天香楼被改为病死——那就必须改太虚幻境和宝玉袭人之事。此时其他的改动，导致原有的结局：荣府衰败已不可用，因为逻辑上讲，再衰败，也是[百足之虫](https://www.zhihu.com/search?q=%E7%99%BE%E8%B6%B3%E4%B9%8B%E8%99%AB&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A1611895531%7D)死而不僵。宝玉也就不可能出家（出家的结局已定）。

贾环继承，赶走宝玉是非常早期的强力的剧情。只要曹雪芹的笔下，荣府的世袭职位不废除，贾环就必然继承，而不是[贾蔷](https://www.zhihu.com/search?q=%E8%B4%BE%E8%94%B7&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A1611895531%7D)。再次佩服张爱玲，就从 79 回贾政贾赦喝酒一段，就告诉了我们作者的小心思。

第五次删改，作者甚至都没完成就去世了。


抄家线：贾雨村与贾赦事发——大观园梦终——黛玉病死，同时薛家[明哲保身](https://www.zhihu.com/search?q=%E6%98%8E%E5%93%B2%E4%BF%9D%E8%BA%AB&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A1611895531%7D)，早已离去——各女儿离散，贾母病死——宝玉出园，开始困厄与颠沛流离之日——最终被带到青梗峰下

未抄家线：[贾雨村](https://www.zhihu.com/search?q=%E8%B4%BE%E9%9B%A8%E6%9D%91&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A1611895531%7D)与贾赦事发——宁府爵位被削，荣府一落千丈——宝钗嫁宝玉——大观园梦终——黛玉病死 贾母病死 元春病死 [贾政](https://www.zhihu.com/search?q=%E8%B4%BE%E6%94%BF&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A1611895531%7D)病死——贾环上位——宝钗一年后难产而死——贾宝玉被赶走———宝玉出家———最终被带到青梗峰下

下面以宝玉的结局为例讲讲张爱玲是怎样分析的。

一：黛玉病死，宝钗嫁给宝玉，一年后难产而死（或者不到一年病死），宝玉贫困至极，做了看街兵，就是在街边木棚中居住，往来官员时任人驱使的仆役。湘云早寡，沿街乞食。直到晚年二人才相遇，在风雪夜相拥而泣，然后再婚，共同生活。这就是白头偕老。

二：黛玉病死当天宝钗嫁给宝玉，宝玉出家，随一僧一道而去，最后重归青埂峰下，那块玉也重新化为顽石。

三、黛玉病死，宝钗难产，宝玉不务正业，没有出息，袭人终于主动求去，多年后宝玉乞食至袭人家门口。以袭人的角度叙述，嫁给蒋玉菡后家道日盛，生活丰足，早已不再回忆救主。风雪之中二人隔门对望，忽然同时扑地立毙。

四、黛玉病死，宝玉宝钗生活困苦，袭人说服蒋玉菡将二人接到家中奉养（有版本是袭人多次接济宝玉，遭到蒋玉菡厌憎），后来宝玉出家，将宝钗托付给袭人。


不同的版本之中张爱玲的主要结论是：早期《红楼梦》本来没有打算写到抄家、破败，而是回顾烈火烹油的大家生活，和温馨细腻的闺阁情趣，自传成分很重，人物也相对较少，能和曹雪芹的经历相吻合。但是随着故事的展开，许多人物走上了符合逻辑的发展道路，而这个逻辑，再创作也脱不开大家族整体衰亡的事实，甚至于曹自身的思想是比较接近于 “败落乃是自作自受” 的意思，之前越轰轰烈烈，也就伏笔了之后忽然的败落。袭人作为宝玉原型心中一个重要的感情对象，最早拥有了完整的剧情，那就是负恩，离宝玉而去。如果没有这个选择，袭人不成其为袭人。那么为了袭人（以及其他许多人物）走向上的完整，抄家的剧情不可避免。事实上，可以说每一个红楼人物的命运都和最后的衰亡密切相关。这一个阶段的修改，是逐渐从为自身讳饰的消遣性创作走向严肃，揭示命运的必然与偶然，完善每个人物自身的逻辑。之后，随着书稿流散，读者增加，反馈成规模。曹雪芹也逐渐成长为一个职业的严肃的作者，就像张爱玲一样，他走向了对读者严肃的迎合，直到将人物套上神话的框子。张爱玲没有分析为什么这样做，单纯是为了淡化悲剧色彩么？整部红楼每一个字都是为了悲剧做铺垫，我个人的理解，神话是为了给予读者安慰，安慰并不是淡化，而是以因果报应来解释命运的不可知。其实，谁也不知道这仅仅是安慰呢？

## 参考

- [2024-04-24-如何评价张爱玲的《红楼梦魇》？](/archive/2024-04-24-如何评价张爱玲的《红楼梦魇》？)
- [【读书】红楼梦](/archive/【读书】红楼梦)
- [2024-04-24-《红楼梦魇》：张爱玲读红楼](/archive/2024-04-24-《红楼梦魇》：张爱玲读红楼)
- [2024-04-24-说说《红楼梦魇》（红楼梦魇）书评](/archive/2024-04-24-说说《红楼梦魇》（红楼梦魇）书评)