### 在相应文件夹下建立desktop.ini
文档desktop.ini
```
[.ShellClassInfo]
LocalizedResourceName=@%SystemRoot%\system32\shell32.dll,-21770
IconResource=%SystemRoot%\system32\imageres.dll,-112
IconFile=%SystemRoot%\system32\shell32.dll
IconIndex=-235
```
桌面desktop.ini
```
[.ShellClassInfo]
LocalizedResourceName=@%SystemRoot%\system32\shell32.dll,-21769
IconResource=%SystemRoot%\system32\imageres.dll,-183
```

### 设置文件属性
1. 进入上级目录执行
```
attrib +r Videos//相应的文件夹
```
3. 参考下正常 desktop.ini 的属性，cd 进目录执行命令 
```
attrib +a +s +h desktop.ini
```

### 无法识别attrib
添加环境变量`%SystemRoot%\system32`
## 参考
- [2024-04-23-桌面、文档、下载等文件夹移动后无法复原或 desktop.ini 不起作用的修复方法_移动桌面位置以后还原不了 - CSDN 博客](/archive/2024-04-23-桌面、文档、下载等文件夹移动后无法复原或%20desktop.ini%20不起作用的修复方法_移动桌面位置以后还原不了%20-%20CSDN%20博客)
- [2024-04-23-win10 资源管理器](/archive/2024-04-23-win10%20资源管理器)
