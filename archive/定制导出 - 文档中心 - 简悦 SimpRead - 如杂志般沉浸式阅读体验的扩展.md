> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [](http://ksria.com/simpread/docs/#/定制化导出?id=标题)

> 文档中心 | 简悦 SimpRead - 如杂志般沉浸式阅读体验的扩展

> 此功能最低要求 2.2.0 版本，如低于此版本，[请升级](https://simpread.pro/) 到最新版本。

* * *

[目录](#/定制化导出?id=目录)
---------------------------------------------------------------------------

*   [描述](#/定制化导出?id=描述)
    
*   [与普通导出的区别](#/定制化导出?id=与普通导出的区别)
    
*   [能做什么](#/定制化导出?id=能做什么)
    
*   [选项](#/定制化导出?id=选项)
    
*   [自定义导出标题](#/定制化导出?id=自定义导出标题)
    
*   [HTML 定制化](#/定制化导出?id=html)
    
*   [Markdown 定制化](#/定制化导出?id=markdown)
    
*   [自定义导出](#/定制化导出?id=自定义导出)
    
*   [Webhook](#/定制化导出?id=webhook)
    
*   [格式化](#/定制化导出?id=格式化)
    

[描述](#/定制化导出?id=描述)
---------------------------------------------------------------------------

> 定制化导出是 2.2.0 最重要的功能之一，此功能针对以下用户：
> 
> 1.  动手能力强
>     
> 2.  双链笔记使用者
>     

[与普通导出的区别](#/定制化导出?id=与普通导出的区别)
---------------------------------------------------------------------------------------------------------------------------------------

> 定制化导出是在简悦 [导出到本地服务](#/保存到本地) 的基础上，结合 [同步助手的增强导出](#/Sync?id=导出服务) 而形成的定制化操作。
> 
> 普通用户完全可以不用使用此功能。

[能做什么](#/定制化导出?id=能做什么)
-----------------------------------------------------------------------------------------------

1.  将当前标注 [以设定好格式](#/定制化导出?id=markdown) 导入到双链笔记中。
    
2.  结合 [同步助手 · 增强导出功能](#/Sync?id=导出服务) 可以将 **某个页面的全部标注导出为双链笔记直接使用** 的格式。
    
3.  结合 **定制化标题**，可以让稍后读读取本地缓存文件。
    

等等，定制化导出是简悦最重要的一个功能。

[选项](#/定制化导出?id=选项)
---------------------------------------------------------------------------

> 选项页 → 服务 → 定制导出

[自定义导出标题](#/定制化导出?id=自定义导出标题)
-----------------------------------------------------------------------------------------------------------------------------

Tip

此功能是 [读取本地快照](#/稍后读?id=读取本地快照) 的基础，也是大部分基于本地系统的功能，与 Obsidian、Logseq 联动等功能的基础。

此功能是一个复合型功能，详细配置请看 [一站式教程](https://www.yuque.com/kenshin/simpread/wkswh7)。

> 此功能需要配合 [**简悦 · 同步助手 1.0.1**](#/Sync?id=下载) 才能使用，支持如下关键字的定制化：

*   `{{id}}` → 稍后读 ID
    
*   `{{type}}` → 当前导出的类型 e.g. `markdown` `html` `offlinehtml` 等
    
*   `{{title}}` → 当前页面的标题
    
*   `{{un_title}}` → 稍后读的标题
    
*   `{{create}}` → 稍后读的日期
    
*   `{{timestamp}}` → 稍后读日期的时间戳
    
*   `{{date_format|yyyy-MM-dd hh.mm:ss}}` → 当前时间的格式化
    
    > 当设置为 `{{date_format|yyyy-MM-dd hh:mm:ss}}` 会生成 `2021-05-21 08:36:56` 这样的结构，这个时间是 **这条标注产生的时间**，而非当前时间。
    
*   `{{mode}}` → 模式 e.g. 当导出的内容为全文时 mode = ""；当导出为内容仅为标注时 mode="annote" 此标识用于区分导出内容（因稍后读增加了读取本地内容的功能） `important`
    

> 例如： 当选择 **全文 + 标注** 时的导出方案，如下图：

[![](https://z3.ax1x.com/2021/05/19/g5ep9J.png)](https://imgtu.com/i/g5ep9J)

```
{{id}}{{un_title}}{{mode}} → 1234-卡片盒笔记法详细介绍.html
{{id}}{{un_title}}{{mode}} → 1234-卡片盒笔记法详细介绍@annote.html → 当带有 @annote 时其内容为 标注

```

> 导出文件带有类型

```
{{id}}{{type}}{{un_title}}{{mode}} → 1234-markdown-卡片盒笔记法详细介绍.md

```

[HTML](#/定制化导出?id=html)
---------------------------------------------------------------

HTML 定制化包括以下几个功能：

1.  笔记模板
    
2.  导出文件包含目录
    
3.  图片代理
    

### [主题模板](#/定制化导出?id=主题模板)

> 新增加了 **笔记模板**，在视觉上更接近 **Zettelkasten 笔记法的卡片形态**，同时也可配合 [**反向链接**](#/双向链接)，更适合 Anki 类的轻量级回顾复习。

![](https://z3.ax1x.com/2021/05/18/gfgqsA.png)

### [目录](#/定制化导出?id=目录-1)

> 当导出为原文时，会加入跟阅读模式一样的目录功能。

![](https://z3.ax1x.com/2021/05/18/gf2wyd.png)

### [图片代理](#/定制化导出?id=图片代理)

> 如果你使用 [同步助手 · 增强导出功能](#/Sync?id=导出服务) 的 PDF 功能的话，务必开启此选项。

> 此功能需要配合 [**简悦 · 同步助手 1.0.1**](#/Sync) 才能使用。

[![](https://z3.ax1x.com/2021/05/19/g5ZiTS.png)](https://imgtu.com/i/g5ZiTS)

> 引申阅读 [利用此功能可方便在双链笔记中做永久的原文链接](https://github.com/Kenshin/simpread/discussions/2152)。

[Markdown](#/定制化导出?id=markdown)
-----------------------------------------------------------------------

Tip

使用 **Markdown 辅助增强插件** 具有更多模版定制化选项，详细 [请看这里](https://github.com/Kenshin/simpread/discussions/3725)。

[**特点**](#/定制化导出?id=特点)
-------------------------------------------------------------------------------

*   可完美的支持 Obsidian · Logseq 等任意双链笔记的语法规则，细节请看 [](https://github.com/Kenshin/simpread/discussions/3725#discussioncomment-2635600)。
    
*   内置 [](https://ejs.co/#docs) ，所以可以使用 EJS 引擎全部的功能。
    
*   可在模板中直接编写 Javascript 代码。
    
*   所见即所得，相比 1.0（下面的方案） 在模板中去掉空格或无法空格等问题，2.0 无此问题。
    
*   兼容 1.0 的 [语法规则](#/定制化导出?id=markdown)（占位符）。
    
*   兼容 1.0 [模板](https://github.com/Kenshin/simpread/discussions/2153)。
    

> Markdown 定制化主要针对 **方便导出到双链笔记** 的功能，包括：

1.  自定义模板
    
2.  单条标注的模板
    
3.  自定义标签 / 多级链接 / 反向链接
    
4.  内部链接
    
5.  外部链接
    

### [Markdown](#/定制化导出?id=markdown-1)

> 简悦稍后读的全部内容均可定制化，包括：

*   `{{host}}` → URL host
    
*   `{{title}}` → 标题
    
*   `{{create}}` → 时间
    
    *   `{{date_format|now|yyyy-MM-dd hh.mm:ss}}` → 稍后读建立时间的格式化，now 当前时间，`仅可在全局或标注中使用一次` **此标识后必须要加一个空格**

*   `{{desc}}` → 描述
    
*   `{{note}}` → 备注
    
*   `{{tags}}` → 标签
    
*   `{{url}}` → 原页面
    
*   `{{refs}}` → 引用来源
    
*   `{{backlinks}}` → 反向链接
    
*   标注
    
    *   `{{#each}}` → 标注循环开始标识
        
    *   `{{/each}}` → 标注循环结束标识
        
        > 下面的内容必须要放在 `{{#each}}` 与 `{{/each}}` 中才能生效。（可以看下方的例子）
        
    *   `{{an_id}}` → 标注 id
        
    *   `{{an_timestamp}}` → 标注 YYYYMMDDHHmm
        
    *   `{{an_create}}` → 标注 时间
        
    *   `{{date_format|<type>|yyyy-MM-dd hh.mm:ss}}` → 标注 时间的格式化，`<type>` 包含：id 标注时间，now 当前时间，`仅可在全局或标注中使用一次` **此标识后必须要加一个空格**
        
        > 当设置为 `{{date_format|id|yyyy-MM-dd hh:mm:ss}}` 会生成 `2021-05-18 14:27:56` 这样的结构，这个时间是 **这条标注产生的时间**，而非当前时间。
        
    *   `{{an_html}}` → 标注的富文本
        
    *   `{{an_short_text}}` → 标注的文本，仅截取前 20 个字符
        
    *   `{{an_text}}` → 标注的文本
        
    *   `{{an_note}}` → 标注的备注
        
    *   `{{an_tags}}` → 标注的标签
        
    *   `{{an_refs}}` → 标注的引用来源
        
    *   `{{an_backlinks}}` → 标注的反向链接
        
    *   `{{an_org_uri}}` → 标注原页面的位置，可通过锚点定位 e.g. `https://sspai.com/post/39491`
        
    *   `{{an_int_uri}}` → 标注页面内部链接的位置 e.g. `http://localhost:7026/reading/xxx`
        
    *   `{{an_ext_uri}}` → 标注页面外部链接的位置 e.g. `https://simpread.pro/@<id>/reading/xxx`
        
    *   `{{[ \S]+\|xxx}}` → 格式化某些结构，包括：`backlinks` `desc` `backlinks` `refs` `an_text` `an_html` `an_note` `an_refs` `an_backlinks`，举例说明：
        
        *   如果 `note = aaa`，设置为 `{{>|note}}`，最后生成的结果是：`>aaa`
            
        *   如果 `note = aaa`，设置为 `{{> |note}}`，最后生成的结果是：`> aaa` （留意当前的例子与上面的例子，`>` 之后有空格）
            

#### [一个例子](#/定制化导出?id=一个例子)

```
  > 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [{{host}}]({{url}})

  - 描述
  > {{desc}}

  - 备注
  {{>|note}}

  - 标签
  > {{tags}}

  > 建立时间：{{create}}

  > 关联阅读：
  >
  {{> |backlinks}}

  > 外部引用：
  >
  {{> |refs}}

  ***

  {{#each}}

  - 时间
    {{an_create}}

  - 标注
  {{  >|an_html}}

  - 备注
  {{  >|an_note}}

  - 标签
    {{an_tags}}

  - 关联
  {{  |an_backlinks}}

  - 外部引用
  {{  |an_refs}}

  - 链接
    - [原链接](<{{an_org_uri}}>)
    - [内部链接](<{{an_int_uri}}>)
    - [外部链接](<{{an_ext_uri}}>)

  - 引用来源
    - [{{an_text}}](<{{an_ext_uri}}>)

  ***

  {{/each}}

```

> 根据 [教程](https://github.com/Kenshin/simpread/discussions/2222) 将上面的内容粘贴到 Obsidian 就能看到结果了。

### [单条 Markdown](#/定制化导出?id=单条markdown)

> 当只想导出某个具体标注时，则可以使用此功能，同样也支持定制化。

#### [一个例子](#/定制化导出?id=一个例子-1)

```
{{#each}}

---

title: {{title}}
uid: {{an_timestamp}}}
tags: {{tags}}

---


- 来源
  > [{{title}}](<{{an_int_uri}}>)

- 标注
{{  >|an_html}}

- 时间
  > {{date_format|id|yyyy-MM-dd hh:mm:ss}} 

- 备注
{{  >|an_note}}

{{/each}}

```

### [官方模板](#/定制化导出?id=官方模板)

> 如果你是一个双链笔记使用者，可以直接 [在这里](https://github.com/Kenshin/simpread/discussions/2153) 找到官方定制的一些方案。

### [其它的一些定制](#/定制化导出?id=其它的一些定制)

> 为双链笔记使用者专门提供了一些常用的功能，如： **标签的定制化** **多级标签的形式** **反向链接的形式** 等细节。

![](https://s1.ax1x.com/2022/11/09/zSDVVs.png)

> 其中 **内部链接** 与 **外部链接** 是关于 **双向链接使用者方便摘录后的留存地址** 功能，稍晚时提供。如你已完全理解并配置好 **定制化导出**，但仍未给出此文档的话，请提 [Issues](https://github.com/kenshin/simpread/issues/new)
> 
> 除此之外介绍一些比较重要的说明。

#### [反向链接](#/定制化导出?id=反向链接)

因为简悦 2.2 本就支持反向链接，所以你可以使用其它双链笔记支持的方式定制化，包括如下内容：

*   `[{{id}}]({{id}})` → 稍后读 id
    
*   `[[{{un_title}}]([{{un_title}})]` → 稍后读标题
    
*   `[[{{an_org_uri}}]([{{an_org_uri}})]` → 稍后读原页面的位置，可通过锚点定位
    
*   `[[{{an_int_uri}}]([{{an_int_uri}})]` → 稍后读内部链接的位置
    
*   `[[{{an_ext_uri}}]([{{an_ext_uri}})]` → 稍后读外部链接的位置
    

![](https://s1.ax1x.com/2022/11/09/zSBHgO.png)

导出后就会得到类似下面的效果

![](https://z3.ax1x.com/2021/07/10/RzvPhR.png)

[自定义导出](#/定制化导出?id=自定义导出)
---------------------------------------------------------------------------------------------------------

> 得益于 Pandoc 的加持，2.2.0 版支持通过 [**简悦 · 同步助手 1.0.1**](#/Sync) 导出任意格式，如：`.docs` 等，以下是一个自定义导出的例子：

```
{"name":"Markdown 转换 docx", "in":"markdown", "out":"docx", "icon":"<i class=\"fas fa-file-word\"></i>"}
{"name":"HTML 转换 docx", "in":"html", "out":"docx"}

```

### [参数说明](#/定制化导出?id=参数说明)

*   `name` → 标识，设置后会出现在任意具有导出服务里面。
    
*   `in` → 转换前的格式，目前只支持 `html` `markdown`
    
*   `out` → 转换后的格式，只要是 `pandoc` 支持的均可转换，如：`docx` 等。
    
*   `icon` → [https://fontawesome.com/icons?d=gallery&m=free](https://fontawesome.com/icons?d=gallery&m=free) 提供的 icon
    

### [效果](#/定制化导出?id=效果)

> 当配置完毕后，只要是具有导出功能的地方均可使用，下图为 **阅读模式** 和 **弹出导出对话框** 下的效果

[![](https://z3.ax1x.com/2021/05/21/g7ZAl4.png)](https://imgtu.com/i/g7ZAl4)

[![](https://z3.ax1x.com/2021/05/21/g7ZnTx.png)](https://imgtu.com/i/g7ZnTx)

[Webhook](#/定制化导出?id=webhook)
---------------------------------------------------------------------

> 2.2.0 版简悦加入了自己的 Webhook 功能，如果有不支持的导出服务，可以通过 Webhooks 导出到任意服务。
> 
> 比如：利用 IFTTT 的 Webhooks 服务，**即便不使用简悦 API，也可以将简悦导入到任意支持 IFTTT 的服务**。
> 
> 与自定义导出一致：也可以在任意具有导出功能的地方使用它。

### [一个 Webhook 写法](#/定制化导出?id=一个-webhook-写法)

```
{"name":"Webhook2Pocket","url":"https://maker.ifttt.com/trigger/simpread_webhook/with/key/******","type":"POST","headers":{"Content-Type":"application/json"},"body":{"value1":"{{url}}"}}

```

### [参数说明](#/定制化导出?id=参数说明-1)

> 如果你是一名开发者的话，会很容易理解 Webhook 的参数，其实就是 `$.ajax` 或 `axios` 参数。

*   `name` → 标识，设置后会出现在任意具有导出服务里面。
    
*   `url` → 调用的服务地址
    
*   `type` → 调用方式，如：`POST` `GET` `PUT` 等
    
*   `headers` → 请求头信息
    
*   `fmt` → 仅包含 `md|html|ofhtml` 对应了下方的 `{{content}}` 得到的结果
    
    > 假设设置为 `md` ，那么 `{{content}}` 的内容就是 **阅读模式正文的 Markdown**
    
*   `body` → 请求体的内容，支持以下参数
    
    *   `{{url}}` → 原文地址
        
    *   `{{title}}` → 标题
        
    *   `{{desc}}` → 描述
        
    *   `{{content}}` → 内容
        
    *   `{{tags}}` → 标签，用 `,` 分割，仅在稍后读时有效
        
    *   `{{note}}` → 备注，仅在稍后读时有效
        

### [注意](#/定制化导出?id=注意)

> 因一个 Bug 导致 `fmt` 参数无法生效，因此 `{{content}}` 的内容仅只能是 Markdown。

[格式化](#/定制化导出?id=格式化)
-------------------------------------------------------------------------------------

> 简悦的导出格式支持下面这些细节的定制化

### [例子](#/定制化导出?id=例子)

```
{"headingStyle":"atx","hr":"---","table":"md"}

```

### [表格](#/定制化导出?id=表格)

> 表格这部分的定制化比较特殊，因为 HTML 的表格结构一旦很复杂后，再转换为 Markdown 后就会存在问题。
> 
> 并且很多现代化的 Markdown 编辑器（ e.g. Typora ）都支持预览 HTML Table，所以：**默认导出的表格为 HTML**。
> 
> 如果你需要使用 `md` 结构的表格，只需要设置： `"table":"md"`

![](https://z3.ax1x.com/2021/09/04/hginvF.png)

#### [一个例子](#/定制化导出?id=一个例子-2)

> 访问 [https://www.runoob.com/linux/linux-vim.html](https://www.runoob.com/linux/linux-vim.html) 默认使用 HTML 方式导出 `.md` 效果为

![](https://s1.ax1x.com/2022/11/09/zSDpPP.png)

> 加入 `"table":"md"` 后导出的效果为

![](https://s1.ax1x.com/2022/11/09/zSDkrQ.png)

### [定制化说明](#/定制化导出?id=定制化说明)

<table><thead><tr><th>Option</th><th>Valid values</th><th>Default</th></tr></thead><tbody><tr><td><code>headingStyle</code></td><td><code>setext</code> or <code>atx</code></td><td><code>setext</code></td></tr><tr><td><code>hr</code></td><td>Any <a href="http://spec.commonmark.org/0.27/#thematic-breaks" target="_blank" rel="noopener">Thematic break</a></td><td><code>* * *</code></td></tr><tr><td><code>bulletListMarker</code></td><td><code>-</code>, <code>+</code>, or <code>*</code></td><td><code>*</code></td></tr><tr><td><code>codeBlockStyle</code></td><td><code>indented</code> or <code>fenced</code></td><td><code>indented</code></td></tr><tr><td><code>fence</code></td><td><code>```</code> or <code>~~~</code></td><td><code>```</code></td></tr><tr><td><code>emDelimiter</code></td><td><code>_</code> or <code>*</code></td><td><code>_</code></td></tr><tr><td><code>strongDelimiter</code></td><td><code>**</code> or <code>__</code></td><td><code>**</code></td></tr><tr><td><code>linkStyle</code></td><td><code>inlined</code> or <code>referenced</code></td><td><code>inlined</code></td></tr><tr><td><code>linkReferenceStyle</code></td><td><code>full</code>, <code>collapsed</code>, or <code>shortcut</code></td><td><code>full</code></td></tr><tr><td><code>preformattedCode</code></td><td><code>false</code> or <a href="https://github.com/lucthev/collapse-whitespace/issues/16" target="_blank" rel="noopener"><code>true</code></a></td><td><code>false</code></td></tr></tbody></table>