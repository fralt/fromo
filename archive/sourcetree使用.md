Sourcetree 中的基本名词说明：
--------------------

克隆/新建(clone)：从远程仓库URL加载创建一个与远程仓库一样的本地仓库。

提交(commit)：将暂存区文件上传到本地代码仓库。

推送(push)：将本地仓库同步至远程仓库，一般推送（push）前先拉取（pull）一次，确保一致（十分注意：这样你才能达到和别人最新代码同步的状态，同时也能够规避很多不必要的问题）。

拉取(pull)：从远程仓库获取信息并同步至本地仓库，并且自动执行合并（merge）操作（git pull=git fetch+git merge）。

获取(fetch)：从远程仓库获取信息并同步至本地仓库。

分支(branch)：创建/修改/删除分枝。

合并(merge)：将多个同名文件合并为一个文件，该文件包含多个同名文件的所有内容，相同内容抵消。

贮藏(git stash)：保存工作现场。

丢弃(Discard)：丢弃更改,恢复文件改动/重置所有改动,即将已暂存的文件丢回未暂存的文件。

标签(tag)：给项目增添标签。

工作流(Git Flow)：团队工作时，每个人创建属于自己的分枝（branch），确定无误后提交到master分支。

终端(terminal)：可以输入git命令行。

每次拉取和推送的时候不用每次输入密码的命令行：git config credential.helper osxkeychain sourcetree。

检出(checkout)：切换不同分支。

添加（add）：添加文件到缓存区。

移除（remove）：移除文件至缓存区。

重置(reset)：回到最近添加(add)/提交(commit)状态。



连接 Gitee or GitHub，获取代码:
------------------------

**注意：这里介绍的是使用 SSH 协议获取关联远程仓库的代码，大家也可以直接使用过 HTTPS 协议的方式直接输入账号密码获取关联代码！**

### 全面概述 Gitee 和 GitHub 生成 / 添加 SSH 公钥：

> [https://www.cnblogs.com/Can-daydayup/p/13063280.html](https://www.cnblogs.com/Can-daydayup/p/13063280.html)

### 在 SourceTree 中添加 SSH 密钥：

**工具 => 选择：**

 ![](https://img2020.cnblogs.com/blog/1336199/202006/1336199-20200622191720038-469713811.png)

 

**添加 SSH 密钥位置：C:\Users\xxxxx\.ssh\id_rsa.pub：**

![](https://img2020.cnblogs.com/blog/1336199/202006/1336199-20200622192020108-1504684036.png)

**SSH 客户端选择 OpenSSH：**

 ![](https://img2020.cnblogs.com/blog/1336199/202006/1336199-20200622192157993-1839762494.png)

### Clone 对应托管平台仓库（以 Gitee 为例）：

**打开码云，找到自己需要 Clone 的仓库！**

![](https://img2020.cnblogs.com/blog/1336199/202006/1336199-20200622192403083-401690651.png)

 ![](https://img2020.cnblogs.com/blog/1336199/202006/1336199-20200622192429232-2018866249.png)

![](https://img2020.cnblogs.com/blog/1336199/202006/1336199-20200622192434571-1329451685.png)

SourceTree 设置默认工作目录：
--------------------

由上面我们可以发现每次 Clone 克隆项目的时候，克隆下来的项目默认存储位置都是在 C 盘，因此每次都需要我们去选择项目存放的路径，作为一个喜欢偷懒的人而言当然不喜欢这种方式啦，因此我们可以设置一个默认的项目存储位置。

### 设置 SourceTree 默认项目目录：

**点击工具 => 选项 => 一般 => 找到项目目录设置 Clone 项目默认存储的位置：**

 ![](https://img2020.cnblogs.com/blog/1336199/202006/1336199-20200622193155954-1782496013.png)

SourceTree 代码提交：
----------------

### 1. 首先切换到需要修改功能代码所在的分支：

 ![](https://img2020.cnblogs.com/blog/1336199/202006/1336199-20200622193239308-1099089483.png)

 ![](https://img2020.cnblogs.com/blog/1336199/202006/1336199-20200622193252528-397347765.png)

### 2. 将修改的代码提交到暂存区：

![](https://img2020.cnblogs.com/blog/1336199/202006/1336199-20200622193327759-1898808631.png)

### 3. 将暂存区中的代码提交到本地代码仓库：

**注意：多人同时开发项目的时候，不推荐默认选中立即推送变更到 origin/develop，避免一些不必要的麻烦！**

![](https://img2020.cnblogs.com/blog/1336199/202006/1336199-20200622193427447-6854062.png)

###4. 代码拉取更新本地代码库，并将代码推送到远程仓库：

![](https://img2020.cnblogs.com/blog/1336199/202006/1336199-20200622193448706-1897752328.png)

 

 **勾选需要推送的分支，点击推送到远程分支：**

![](https://img2020.cnblogs.com/blog/1336199/202006/1336199-20200623132528142-1349180450.png)

 

**代码成功推送到远程代码库：**

![](https://img2020.cnblogs.com/blog/1336199/202006/1336199-20200622193919813-2013529741.png)

### 5. 在 Gitee 中查看推送结果：

![](https://img2020.cnblogs.com/blog/1336199/202006/1336199-20200623004546609-1003391480.png)

SourceTree 分支切换，新建，合并：
----------------------

### 1. 分支切换：

**双击切换：**

![](https://img2020.cnblogs.com/blog/1336199/202006/1336199-20200623004639876-954717941.png)

 

**单击鼠标右键切换：**

![](https://img2020.cnblogs.com/blog/1336199/202006/1336199-20200623004740334-196731669.png)

### 2. 新建分支：

**注意：在新建分支时，我们需要在哪个主分支的基础上新建分支必须先要切换到对应的主分支才能到该主分支上创建分支，如下我们要在 master 分支上创建一个 feature-0613 分支：**

 ![](https://img2020.cnblogs.com/blog/1336199/202006/1336199-20200623004820177-502988921.png)

![](https://img2020.cnblogs.com/blog/1336199/202006/1336199-20200623004930000-228279236.png)

### 3. 合并分支:

**注意：在合并代码之前我们都需要将需要合并的分支拉取到最新状态（** 避免覆盖别人的代码，或者丢失一些重要文件）!!!!!**

 

在 master 分支上点击右键，选择合并 feature-0613 至当前分支即可进行合并:

![](https://img2020.cnblogs.com/blog/1336199/202006/1336199-20200623005043926-285373876.png)

 

分支合并成功：

![](https://img2020.cnblogs.com/blog/1336199/202006/1336199-20200623005109462-548799471.png)

SourceTree 代码冲突解决：
------------------

### 首先我们需要制造一个提交文件遇到冲突的情景：

在 SoureceTree 中在 Clone 一个新项目，命名为 pingrixuexilianxi2，如下图所示：

![](https://img2020.cnblogs.com/blog/1336199/202006/1336199-20200623005413727-71003140.png)

我们以项目中的【代码合并冲突测试. txt】文件为例：

![](https://img2020.cnblogs.com/blog/1336199/202006/1336199-20200623005432930-48043549.png)

 

在 pingrixuexilianxi2 中添加内容，并提交到远程代码库，添加的内容如下：

![](https://img2020.cnblogs.com/blog/1336199/202006/1336199-20200623013805749-1130371050.png)

 

在 pingrixuexilianxi 中添加内容，提交代码（不选择立即推送变更到 origin/master），拉取代码即会遇到冲突：

 ![](https://img2020.cnblogs.com/blog/1336199/202006/1336199-20200623005654555-453287987.png)

![](https://img2020.cnblogs.com/blog/1336199/202006/1336199-20200623005706004-874248216.png)

 ![](https://img2020.cnblogs.com/blog/1336199/202006/1336199-20200623005726572-1651182655.png)

 

冲突文件中的内容：

 ![](https://img2020.cnblogs.com/blog/1336199/202006/1336199-20200623005746702-666269418.png)

### 直接打开冲突文件手动解决冲突：

由下面的冲突文件中的冲突内容我们了解到：

```
<<<<<<< HEAD
6月19日 pingrixuexilianxi添加了内容
=======
6月18日 pingrixuexilianxi2修改了这个文件哦
>>>>>>> a8284fd41903c54212d1105a6feb6c57292e07b5

```

 

<<<<<<< HEAD 到 ======= 里面的【6 月 19 日 pingrixuexilianxi 添加了内容】是自己刚才的 Commit 提交的内容

======= 到 >>>>>>> a8284fd41903c54212d1105a6feb6c57292e07b5 里面的【6 月 18 日 pingrixuexilianxi2 修改了这个文件哦】是远程代码库更新的内容（即为 pingrixuexilianxi2 本地代码库推送修改内容）。

 

手动冲突解决方法：

>　根据项目需求删除不需要的代码就行了，假如都需要的话我们只需要把 <<<<<<<HEAD=======>>>>>>> a8284fd41903c54212d1105a6feb6c57292e07b5 都删掉冲突就解决了（注意，在项目中最后这些符号都不能存在，否则可能会报异常）。

 

最后将冲突文件标记为已解决，提交到远程仓库：

 ![](https://img2020.cnblogs.com/blog/1336199/202006/1336199-20200623010600495-2059103978.png)

### 采用外部文本文件对比工具 Beyond Compare 解决冲突:

#### **SourceTree 配置文本文件对比工具 Beyond Compare:**

工具 => 选项 => 比较：

 ![](https://img2020.cnblogs.com/blog/1336199/202006/1336199-20200623010832552-117689472.png)

![](https://img2020.cnblogs.com/blog/1336199/202006/1336199-20200623010846029-1287602466.png)

#### 使用 Beyond Compare 解决冲突：

Beyond Compare 使用技巧：

官方全面教程：[https://www.beyondcompare.cc/jiqiao/](https://www.beyondcompare.cc/jiqiao/)

 

SourceTree 打开外部和合并工具：

![](https://img2020.cnblogs.com/blog/1336199/202006/1336199-20200623011001843-1136765684.png)

**注意：第一次启动 Beynod Compare 软件需要一会时间，请耐心等待：**

******![](https://img2020.cnblogs.com/blog/1336199/202006/1336199-20200623011022849-1902126537.png)******

 

 

Beynod Compare 进行冲突合并：

![](https://img2020.cnblogs.com/blog/1336199/202006/1336199-20200623011036473-506121355.png)

 

点击保存文件后关闭 Beynod Compare 工具，SourceTree 中的冲突就解决了，在 SourceTree 中我们会发现多了一个 .orig 的文件。接着选中那个. orig 文件，单击右键 => 移除，最后我们推送到远程代码库即可：

 ![](https://img2020.cnblogs.com/blog/1336199/202006/1336199-20200623011106251-1595993209.png)


Git 分布式版本控制器常用命令和使用：
--------------------

当然作为一个有逼格的程序员， 一些常用的命令我们还是需要了解和掌握的，详情可参考我之前写过的文章：

> [https://www.cnblogs.com/Can-daydayup/p/10134733.html](https://www.cnblogs.com/Can-daydayup/p/10134733.html)

SourceTree 如何提交 PR(Pull Request)：
---------------------------------

Pull Request 提交相关操作参考该篇文章：

> [https://www.jianshu.com/p/b365c743ec8d](https://www.jianshu.com/p/b365c743ec8d)

### 1、fork 项目：

![](https://img2020.cnblogs.com/blog/1336199/202011/1336199-20201127002204564-932959538.png)

### 2、克隆本地

![](https://img2020.cnblogs.com/blog/1336199/202011/1336199-20201127002239255-499272074.png)

 打开 Git Bash 输入仓库克隆命令：

```
git clone https://github.com/liangtongzhuo/taro-ui.git

```

### 3、根据文档创建分支

拖进 SourceTree，基于 dev 创建分支如下图：

![](https://img2020.cnblogs.com/blog/1336199/202011/1336199-20201127003247988-782045808.png)

### 4、提交修改的代码到远程代码库

文章上面已经提到了使用 SourceTree 提交的相关操作，可参考：

[](https://www.cnblogs.com/Can-daydayup/p/13128633.html#_label5](https://www.cnblogs.com/Can-daydayup/p/13128633.html#_label5](https://www.cnblogs.com/Can-daydayup/p/13128633.html#_label5](https://www.cnblogs.com/Can-daydayup/p/13128633.html#_label5](https://www.cnblogs.com/Can-daydayup/p/13128633.html#_label5](https://www.cnblogs.com/Can-daydayup/p/13128633.html#_label5](https://www.cnblogs.com/Can-daydayup/p/13128633.html#_label5](https://www.cnblogs.com/Can-daydayup/p/13128633.html#_label5](https://www.cnblogs.com/Can-daydayup/p/13128633.html#_label5](https://www.cnblogs.com/Can-daydayup/p/13128633.html#_label5](https://www.cnblogs.com/Can-daydayup/p/13128633.html#_label5](https://www.cnblogs.com/Can-daydayup/p/13128633.html#_label5](https://www.cnblogs.com/Can-daydayup/p/13128633.html#_label5](https://www.cnblogs.com/Can-daydayup/p/13128633.html#_label5](https://www.cnblogs.com/Can-daydayup/p/13128633.html#_label5](https://www.cnblogs.com/Can-daydayup/p/13128633.html#_label5](https://www.cnblogs.com/Can-daydayup/p/13128633.html#_label5](https://www.cnblogs.com/Can-daydayup/p/13128633.html#_label5](https://www.cnblogs.com/Can-daydayup/p/13128633.html#_label5](https://www.cnblogs.com/Can-daydayup/p/13128633.html#_label5](https://www.cnblogs.com/Can-daydayup/p/13128633.html#_label5](https://www.cnblogs.com/Can-daydayup/p/13128633.html#_label5](https://www.cnblogs.com/Can-daydayup/p/13128633.html#_label5](https://www.cnblogs.com/Can-daydayup/p/13128633.html#_label5](https://www.cnblogs.com/Can-daydayup/p/13128633.html#_label5](https://www.cnblogs.com/Can-daydayup/p/13128633.html#_label5](https://www.cnblogs.com/Can-daydayup/p/13128633.html#_label5](https://www.cnblogs.com/Can-daydayup/p/13128633.html#_label5](https://www.cnblogs.com/Can-daydayup/p/13128633.html#_label5](https://www.cnblogs.com/Can-daydayup/p/13128633.html#_label5](https://www.cnblogs.com/Can-daydayup/p/13128633.html#_label5](https://www.cnblogs.com/Can-daydayup/p/13128633.html#_label5](https://www.cnblogs.com/Can-daydayup/p/13128633.html#_label5)（或者 Ctrl F：SourceTree 代码提交）

当然也可以使用 git 命令提交：

```
git add .--提交所有修改的文件到本地暂存区
git commit -m"fix(dos):修正文字 " --提交到本地代码库
git push--提交到github中的远程代码库

```

### 5、提交 Pull Request

第四步提交成功后，进入原来 fork 的仓库，点击 Compare

![](https://img2020.cnblogs.com/blog/1336199/202011/1336199-20201127003500843-1194814098.png)

 提交你的说明，选择合并的分支即可，剩下等待合并。

![](https://img2020.cnblogs.com/blog/1336199/202011/1336199-20201127003609496-1713228465.png)
## 参考
[【最全面】SourceTree 使用教程详解（连接远程仓库，克隆，拉取，提交，推送，新建 - 切换 - 合并分支，冲突解决，提交 PR）](/archive/【最全面】SourceTree%20使用教程详解（连接远程仓库，克隆，拉取，提交，推送，新建%20-%20切换%20-%20合并分支，冲突解决，提交%20PR）)