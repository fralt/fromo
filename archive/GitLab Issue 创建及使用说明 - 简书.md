> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [www.jianshu.com](https://www.jianshu.com/p/ab5897597d68)

> 现在部门里对需求版本的项目管理都有一套较为成熟的体系了。但是对于 bug 的修复管理统计，还缺乏一套行之有效的方案。对于开发而言，修复 bug 在每日的工作中也占了一定的比重。而这部分的工作量目前没有很好的进行一个量化和统计。同时也没有一个很好的方式去展现目前已经发现的 bug 和已经修复的 bug。使用 Issue 可以一定程度上解决上面所述的问题。本文章主要用于介绍在 GitLab 中如何创建和使用。同时如何为这个 Issue 创建分支和发起 Merge Request。

### Issue 是什么？

如果大家经常使用 Github 上的开源项目，相信对 Issue 一定不陌生，在使用开源项目时，特别是一些比较新的项目，许多问题的解决方案都是在使用者提出的 Issue 里的，甚至是还没解决的。这时你就可以向开源项目的作者提 Issue。让他注意到有这个问题，如果项目活跃，可能很快就会出新 release 修复你提的问题。

![](http://upload-images.jianshu.io/upload_images/813533-5416caa7e1fd9ece.png) GitHub 向 Dubbo 团队提 Issue

### Issue 的基本用法

###### 1. 创建 Issue

![](http://upload-images.jianshu.io/upload_images/813533-1ddd1adfdb83e03b.png) 创建 Issue - 1

![](http://upload-images.jianshu.io/upload_images/813533-54fe05f29e40b295.png) 创建 Issue - 2

一般情况下，如果是一个团队内部使用的 Issue，应该包含下面几项信息：  
1. 标题，描述。  
2. 分配给谁 (Assignee)。  
3. 标签 (Labels)。  
4. 对应的版本 (Milestone)，可选。  
5. 预计完成时间 (Due Date)，可选。

创建完 Issue 以后，可以在相关 Issue 下进行讨论和问题跟踪：

![](http://upload-images.jianshu.io/upload_images/813533-f54422872e644bc1.png) 支持对 Issue 评论

###### 2. 创建 Label

创建标签是为了给 Issue 和看板使用，一般来说，一个 Issue 应该有至少两种类型的 Label，即 Issue 的类型和 Issue 的状态。其中 Issue 的状态可以用来构建看板的栏目。团队也可以根据自己团队的需要创建对应的 Label。

![](http://upload-images.jianshu.io/upload_images/813533-6b05a75a35ba30bb.png) 创建 Label

###### 3. 创建 Board

在看板的页面，可以看到以 Label 为列汇总的 Issue 信息。团队也可以根据自己团队的需要创建对应的看板。

![](http://upload-images.jianshu.io/upload_images/813533-da47827e425e8b67.png) 看板页面

。

###### 4. 创建 MileStones

这里的 MileStones，和版本的概念差不多，可以把一个 Issue 划分在某个版本下。支持以版本的维度进行项目管理。

![](http://upload-images.jianshu.io/upload_images/813533-9a1671b35d509619.png) 1.1.0MileStone 视图

### 基于 Issue 拉取分支以及发起 Merge request

无论是 GitHub 还是 GitLab，都可以方便地在 Issue 上创建分支。在该分支上解决了 Issue 的问题以后，提交远程分支。就可以发起 Merge request

![](http://upload-images.jianshu.io/upload_images/813533-765e58898c834036.png) 为 Issue 创建分支

![](http://upload-images.jianshu.io/upload_images/813533-18f01c825ce25a61.png) 命令行 merge 操作

![](http://upload-images.jianshu.io/upload_images/813533-05243733ebead9bc.png) merge 完成后，该 Issue 的状态会自动变成 closed

![](http://upload-images.jianshu.io/upload_images/813533-3247056b3a366deb.png) merge 完成以后，分支状态就会变成 merged，可删除该分支