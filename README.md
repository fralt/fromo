## 某方的窝


[![GitHub](https://img.shields.io/badge/dynamic/json?logo=github&label=GitHub&labelColor=495867&color=495867&query=%24.data.totalSubs&url=https%3A%2F%2Fapi.spencerwoo.com%2Fsubstats%2F%3Fsource%3Dgithub%26queryKey%3Dhayschan&style=flat-square)](https://github.com/hayschan)
[![RSS](https://img.shields.io/badge/dynamic/json?logo=rss&logoColor=white&label=RSS&labelColor=95B8D1&color=95B8D1&query=%24.data.totalSubs&url=https%3A%2F%2Fapi.spencerwoo.com%2Fsubstats%2F%3Fsource%3Dfeedly%257Cinoreader%257CfeedsPub%26queryKey%3Dhttps://haysc.tech/feed.xml&style=flat-square)](https://haysc.tech/)

- 生活流水账：[闻子语录](https://gitlab.com/caifangwen/fromo/-/issues/1)
- 爱好：[某方看戏](https://gitlab.com/caifangwen/fromo/-/issues/19)、[棋类]()
- 笔记：[经济学](https://gitlab.com/caifangwen/fromo/-/issues/21)、[法学](https://gitlab.com/caifangwen/fromo/-/issues/22)、[计算机](https://gitlab.com/caifangwen/fromo/-/issues/23)、[写作](https://gitlab.com/caifangwen/fromo/-/issues/24)
- 知识库：[Gitlab Wiki](https://gitlab.com/caifangwen/fromo/-/wikis/home)

<h6>* 本仓库作为obsidian仓库的线上备份，issue用于自言自语，执行<a href="/">摸鱼计划</a>。</h6>