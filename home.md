


[![GitHub](https://img.shields.io/badge/dynamic/json?logo=github&label=GitHub&labelColor=495867&color=495867&query=%24.data.totalSubs&url=https%3A%2F%2Fapi.spencerwoo.com%2Fsubstats%2F%3Fsource%3Dgithub%26queryKey%3Dhayschan&style=flat-square)](https://github.com/hayschan)
[![RSS](https://img.shields.io/badge/dynamic/json?logo=rss&logoColor=white&label=RSS&labelColor=95B8D1&color=95B8D1&query=%24.data.totalSubs&url=https%3A%2F%2Fapi.spencerwoo.com%2Fsubstats%2F%3Fsource%3Dfeedly%257Cinoreader%257CfeedsPub%26queryKey%3Dhttps://haysc.tech/feed.xml&style=flat-square)](https://haysc.tech/)
<a target="_blank" href="http://mail.qq.com/cgi-bin/qm_share?t=qm_mailme&email=QSczIC01AScuOSwgKC1vIi4s" style="text-decoration:none;"><img src="http://rescdn.qqmail.com/zh_CN/htmledition/images/function/qm_open/ico_mailme_01.png"/></a>

- 生活流水账：[闻子语录](https://gitlab.com/caifangwen/fromo/-/issues/1)
- 爱好：[某方看戏](https://gitlab.com/caifangwen/fromo/-/issues/19)、[棋类]()
- 笔记：[经济学](https://gitlab.com/caifangwen/fromo/-/issues/21)、[法学](https://gitlab.com/caifangwen/fromo/-/issues/22)、[计算机](https://gitlab.com/caifangwen/fromo/-/issues/23)、[写作](https://gitlab.com/caifangwen/fromo/-/issues/24)
- 知识库：[Gitlab Wiki](https://gitlab.com/caifangwen/fromo/-/wikis/home)

## 近期
- [新政素材](/archive/新政素材)

## 分类

- [稍后读](/archive/稍后读)
- [unlinked files output](unlinked%20files%20output)

| 分类  | 细目                                                                                                         |     |
| --- | ---------------------------------------------------------------------------------------------------------- | --- |
| 知识  | - [技术](/archive/技术)<br>- [写作](/archive/写作)<br>- [读书笔记](/archive/读书笔记)                                      |     |
| 爱好  | - [MBTI](/archive/MBTI)<br>- [棋类](/archive/棋类)<br>- [越剧](/archive/越剧)<br>- [游戏](/archive/游戏)               |     |
| 收藏  | - [simpread](/archive/simpread)<br>- [效率](/archive/效率)<br>- [收藏夹](/archive/收藏夹)<br>- [Tips](/archive/Tips) |     |
